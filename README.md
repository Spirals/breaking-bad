# Research Artifact — Breaking Bad: Quantifying the Addiction of Web Elements to JavaScript
> This is the research artifact for the TOIT paper **"Breaking Bad: Quantifying the Addiction of Web Elements to JavaScript"**.

## Contents
`crawl_nojs` includes the source code for the crawl infrastructure and the browser extension for the crawl with JavaScript.

`crawl_req` includes the source code for the crawl infrastructure and the browser extension used to log HTTP requests.

The heuristic-based WebExtension detecting nojs breakage can be found in [`crawl_nojs/how-broken`](crawl_nojs/how-broken).

## License
Licensed under the Apache-2.0 license (see LICENSE).

Copyright (c) 2023 Inria
