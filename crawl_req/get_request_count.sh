#!/usr/bin/env bash

set -o errexit
set -o nounset

file="${1:?Specify a file}"
crawl_type="${2:?Specify a crawl type}"

get_url_from_filename() {
  grep -o '[^/]\+$' \
    | sed -e 's/^requestLog_//' \
      -e 's/_[a-z]\+_[[:digit:]]\+\.json$//' \
      -e 's/_/\//g'
}

url=$(echo "${file}" | get_url_from_filename)

page_json=$(sed -e 's/^\["/[/' -e 's/"\]$/]/' -e 's/","/,/g' -e 's/\\"/"/g' "${file}")

for party_class in firstParty thirdParty; do
  # First-party/third-party
  if [ "${party_class}" = 'thirdParty' ]; then
    thirdparty_value='true'
  else
    thirdparty_value='false'
  fi

  party_matching_req=$(echo "${page_json}" \
    | jq -c -M "map(select(.thirdParty == ${thirdparty_value}))")

  req_count=$(echo "${party_matching_req}" | jq 'length')

  # Images
  image_req_count=$(echo "${party_matching_req}" \
    | jq 'map(select(.type == "image" // .type == "imageset")) | length')

  # Stylesheets
  stylesheet_req_count=$(echo "${party_matching_req}" \
    | jq 'map(select(.type == "stylesheet")) | length')

  # Fonts
  font_req_count=$(echo "${party_matching_req}" \
    | jq 'map(select(.type == "font")) | length')

  # External script
  script_req_count=$(echo "${party_matching_req}" \
    | jq 'map(select(.type == "script")) | length')

  # XHR
  xhr_req_count=$(echo "${party_matching_req}" \
    | jq 'map(select(.type == "xmlhttprequest")) | length')

  # Tracking/non-tracking
  tracking_req_count=$(echo "${party_matching_req}" \
    | jq "[.[].urlClassification.${party_class} | length] | map(select(. > 0)) | length")

  nontracking_req_count=$(echo "${party_matching_req}" \
    | jq "[.[].urlClassification.${party_class} | length] | map(select(. == 0)) | length")

  echo "\"${url}\",${crawl_type},all,${party_class},${req_count}"
  echo "\"${url}\",${crawl_type},image,${party_class},${image_req_count}"
  echo "\"${url}\",${crawl_type},stylesheet,${party_class},${stylesheet_req_count}"
  echo "\"${url}\",${crawl_type},font,${party_class},${font_req_count}"
  echo "\"${url}\",${crawl_type},script,${party_class},${script_req_count}"
  echo "\"${url}\",${crawl_type},xhr,${party_class},${xhr_req_count}"
  echo "\"${url}\",${crawl_type},tracking,${party_class},${tracking_req_count}"
  echo "\"${url}\",${crawl_type},nontracking,${party_class},${nontracking_req_count}"
done
