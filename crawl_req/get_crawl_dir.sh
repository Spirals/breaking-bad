#!/usr/bin/env sh

set -o errexit
set -o nounset

container="${1?:Specify a container name or id}"
echo "$(docker inspect "${container}" | grep -oP '"UpperDir": "\K.*' | sed 's/",$//')"/home/crawl/
