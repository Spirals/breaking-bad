/*
 * WEB_EXT_SOURCE_DIR=./request-logger OTHER_EXTS=uBlock0 node index.js crawl.txt
 */

import path from "path";
import { fileURLToPath } from "url";
import fs from "fs";
import puppeteer from "puppeteer-core";
import webExt from "web-ext";

const PAGE_DOMCONTENTLOADED_TIMEOUT_S = 10;
const PAGE_LOAD_TIMEOUT_S = 30;
const EXTENSION_ANALYSIS_TIMEOUT_S = 5;

const REQUEST_LOG_SELECTOR = "#request-logger-log";

const HEADLESS = true;
const VIEWPORT = {
  width: 1280,
  height: 800,
};

const SECOND_TO_MS = 1000;

const FIREFOX_VERSION = "90.0a1";

const URL_DONE_INDEX_NAME = "URL_DONE_INDEX";

const __dirname = path.dirname(fileURLToPath(import.meta.url));

const args = process.argv.slice(2);
const urlListFilepath = args[0];
if (!urlListFilepath) {
  console.error("Could not read the URL list filepath.");
  process.exit(1);
}

let outputDir = args[1];
if (!outputDir) {
  outputDir = __dirname;
}

console.log(`Will use ${outputDir} as output directory.`);

const urlList = fs.readFileSync(urlListFilepath)
  .toString()
  .split("\n")
  .filter((url) => Boolean(url));

const crawlStartTime = Date.now();

const urlListName = path.parse(urlListFilepath).name;
const crawlOutputDir = path.resolve(
  outputDir,
  `crawl_${urlListName}_${crawlStartTime}`,
);
fs.mkdirSync(crawlOutputDir);

const logger = fs.createWriteStream(
  `${crawlOutputDir}/crawl_${urlListName}_${crawlStartTime}.log`,
  { flags: "a" },
);

const errorLogger = fs.createWriteStream(
  `${crawlOutputDir}/crawl_${urlListName}_${crawlStartTime}.error`,
  { flags: "a" },
);

const crawlLog = (message, crawlType) => {
  const time = new Date().toISOString();
  const msg = `${time}: [${crawlType}] ${message}`;
  logger.write(msg + "\n");
  console.log(msg);
};

const crawlLogError = (message, crawlType) => {
  const time = new Date().toISOString();
  const msg = `${time}: [${crawlType}] ERROR: ${message}`;
  errorLogger.write(msg + "\n");
  console.log(msg);
};

const START_INDEX_ENV_VAR = "START_INDEX";
const startIndexEnvVar = Number.parseInt(process.env[START_INDEX_ENV_VAR]);
if (process.env[START_INDEX_ENV_VAR] && Number.isNaN(startIndexEnvVar)) {
  console.error(`Could not parse the ${START_INDEX_ENV_VAR} environement \
variable.`);
  process.exit(1);
}
const startIndex = startIndexEnvVar ? startIndexEnvVar : 0;
crawlLog(`Will start at index ${startIndex} of the URL list.`, "");

const OTHER_EXTS_ENV_VAR = "OTHER_EXTS";
if (!process.env[OTHER_EXTS_ENV_VAR]
  || process.env[OTHER_EXTS_ENV_VAR].length === 0) {
  console.error(`Could not parse the ${OTHER_EXTS_ENV_VAR} environement \
variable.`);
  process.exit(1);
}
const SOURCE_DIRS = process.env.OTHER_EXTS.split(",");

const substituteURL = (url) => {
  return url.replace(/\//g, "_");
};

process.on("uncaughtException", (err) => {
  crawlLogError(`uncaught Exception at: ${err}`, "");
});

process.on("unhandledRejection", (reason, promise) => {
  crawlLogError(`unhandled Rejection at: ${promise} reason: ${reason}`, "");
});

// TODO: use page.waitForSelector instead when it works for nojs
const waitForSelector = async (page, selector, timeout) => {
  const POLL_WAITING_TIME_MS = 500;

  const errMsg
    = `timeout of ${timeout} ms in waitForSelector for selector ${selector}`;

  /* eslint-disable no-await-in-loop */
  for (let time = 0; time < timeout; time += POLL_WAITING_TIME_MS) {
    const elementHandle = await Promise.race([
      page.$(selector),
      new Promise((resolve, reject) => {
        setTimeout(() => reject(new Error(errMsg)), timeout);
      }),
    ]);
    if (elementHandle) {
      return elementHandle;
    }
    await page.waitForTimeout(POLL_WAITING_TIME_MS);
  }
  /* eslint-enable no-await-in-loop */

  return Promise.reject(new Error(errMsg));
};

const evaluateWithTimeout = (page, fn, otherArgs, timeout) => {
  const errMsg
    = `timeout of ${timeout} ms in evaluateWithTimeout`;

  return Promise.race([
    () => page.evaluate(fn, ...otherArgs),
    () => new Promise((resolve, reject) => {
      setTimeout(
        () => reject(new Error(errMsg)),
        timeout,
      );
    }),
  ]);
};

const saveJSONReport = async (page, filenameBase, crawlType) => {
  const requestLogJSON = await page.$eval(
    REQUEST_LOG_SELECTOR,
    (jsonScript) => jsonScript.textContent,
  );
  const jsonRequestLogFilename = `requestLog_${filenameBase}.json`;
  const jsonRequestLogPath
    = `${crawlOutputDir}/${crawlType}/${jsonRequestLogFilename}`;
  fs.promises.writeFile(jsonRequestLogPath, requestLogJSON);
  crawlLog("Request log JSON written.", crawlType);
  return jsonRequestLogPath;
};

const hrtimeToMs = (hrtime) => {
  const NS_PER_SEC = 1e9;
  const NS_TO_MS = 1e6;

  return Math.ceil((hrtime[0] * NS_PER_SEC + hrtime[1]) / NS_TO_MS);
};

const inspectURL = async (browser, url, crawlType) => {
  crawlLog(`Starting ${url}`, crawlType);

  const page = await browser.newPage();
  page.setDefaultNavigationTimeout(PAGE_LOAD_TIMEOUT_S * SECOND_TO_MS);

  const startTime = process.hrtime();

  // Use a small timeout for the DOMContentLoaded event, to fail quickly when
  // possible
  await page.goto(url, {
    waitUntil: "domcontentloaded",
    timeout: PAGE_DOMCONTENTLOADED_TIMEOUT_S * SECOND_TO_MS,
  });
  const domLoadingTime = hrtimeToMs(process.hrtime(startTime));
  const realPageURL = page.url();
  crawlLog(
    `Loaded DOM ${realPageURL} for ${url} in ${domLoadingTime} ms.`,
    crawlType,
  );

  await page.waitForNavigation({
    waitUntil: "load",
    timeout: PAGE_LOAD_TIMEOUT_S * SECOND_TO_MS,
  });
  const pageLoadingTime = hrtimeToMs(process.hrtime(startTime));
  crawlLog(
    `Loaded page ${realPageURL} for ${url} in ${pageLoadingTime} ms.`,
    crawlType,
  );

  await waitForSelector(
    page,
    REQUEST_LOG_SELECTOR,
    EXTENSION_ANALYSIS_TIMEOUT_S * SECOND_TO_MS,
  );
  crawlLog(
    `Found request log selector in ${realPageURL} for ${url}`,
    crawlType,
  );

  const filenameBase
    = `${substituteURL(page.url())}_${crawlType}_${Date.now()}`;

  const jsonReportPath = await saveJSONReport(page, filenameBase, crawlType);

  await page.close();

  return {
    crawlURL: url,
    realURL: realPageURL,
    jsonReportPath,
  };
};

const launchFirefox = async (
  revisionInfo,
  platform,
  cdpPort,
  pref,
  sourceDirs,
  headless,
  crawlType,
) => {
  const cliArgs = ["--remote-debugging-port", cdpPort];
  if (headless) {
    cliArgs.unshift("--headless");
  }

  // For now, the upstream web-ext tool does not support multiple sourceDir
  // paths, but it is easy to patch it with the following:
  // diff --git a/src/cmd/run.js b/src/cmd/run.js
  // index 5334554..93eac0b 100644
  // --- a/src/cmd/run.js
  // +++ b/src/cmd/run.js
  // @@ -130 +130,5 @@ export default async function run(
  // -  const manifestData = await getValidatedManifest(sourceDir);
  // +  const extensions = await Promise.all(sourceDir.split(',')
  // +    .map(async (sourceDir) => {
  // +      const manifestData = await getValidatedManifest(sourceDir);
  // +      return { sourceDir, manifestData };
  // +    }));
  // @@ -154 +158 @@ export default async function run(
  // -    extensions: [{sourceDir, manifestData}],
  // +    extensions,
  const sourceDir = [process.env.WEB_EXT_SOURCE_DIR, sourceDirs]
    .flat()
    .map((sourceDirPath) => path.resolve(__dirname, sourceDirPath))
    .join(",");
  crawlLog(`sourceDir: ${sourceDir}`, "");

  // Enable CDP logging
  pref["remote.log.level"] = "Trace";

  await webExt.cmd.run({
    firefox: revisionInfo.executablePath,
    sourceDir,
    noReload: true, // Disable file watching
    pref,
    args: cliArgs,
  }, {
    shouldExitProgram: false,
  });

  const browserVersion = `${revisionInfo.product} ${revisionInfo.revision}`;
  crawlLog(`Launched ${browserVersion} on ${platform}.`, crawlType);
};

const connectToFirefox = (cdpPort) => {
  return puppeteer.connect({
    browserURL: `http://localhost:${cdpPort}`,
    product: "firefox",
    defaultViewport: {
      width: VIEWPORT.width,
      height: VIEWPORT.height,
    },
  });
};

const getLastPage = async (browser) => {
  const pages = await browser.pages();
  return pages[pages.length - 1];
};

(async () => {
  const cdpPortPlain = 12345;
  const cdpPortNoJS = cdpPortPlain + 1;
  const cdpPortAdBlocker = cdpPortPlain + 2;

  const browserFetcher = puppeteer.createBrowserFetcher({ product: "firefox" });
  crawlLog(`Downloading Firefox ${FIREFOX_VERSION}`, "");
  const revisionInfo = await browserFetcher.download(FIREFOX_VERSION);
  const platform = browserFetcher.platform();
  crawlLog("Firefox downloaded", "");

  const CRAWL_TYPE_PLAIN = "plain";
  const CRAWL_TYPE_NO_JS = "nojs";
  const CRAWL_TYPE_AD_BLOCKER = "adblocker";

  await launchFirefox(
    revisionInfo,
    platform,
    cdpPortPlain,
    {},
    [],
    HEADLESS,
    CRAWL_TYPE_PLAIN,
  );
  await launchFirefox(
    revisionInfo,
    platform,
    cdpPortNoJS,
    { "javascript.enabled": false },
    [],
    HEADLESS,
    CRAWL_TYPE_NO_JS,
  );
  await launchFirefox(
    revisionInfo,
    platform,
    cdpPortAdBlocker,
    {},
    SOURCE_DIRS,
    HEADLESS,
    CRAWL_TYPE_AD_BLOCKER,
  );

  await new Promise((resolve) => setTimeout(resolve, 300));
  const browserPlain = await connectToFirefox(cdpPortPlain);

  await new Promise((resolve) => setTimeout(resolve, 500));
  const browserNoJS = await connectToFirefox(cdpPortNoJS);

  await new Promise((resolve) => setTimeout(resolve, 500));
  const browserAdBlocker = await connectToFirefox(cdpPortAdBlocker);

  const REPORTS_DIR = "reports";

  fs.mkdirSync(`${crawlOutputDir}/${REPORTS_DIR}`);
  fs.mkdirSync(`${crawlOutputDir}/${CRAWL_TYPE_PLAIN}`);
  fs.mkdirSync(`${crawlOutputDir}/${CRAWL_TYPE_NO_JS}`);
  fs.mkdirSync(`${crawlOutputDir}/${CRAWL_TYPE_AD_BLOCKER}`);

  crawlLog(`Data will be stored to ${crawlOutputDir}`, "all");

  /* eslint-disable no-await-in-loop */
  for (const [urlIndex, url] of urlList.slice(startIndex).entries()) {
    const crawlURLResults = await Promise.all([
      inspectURL(browserPlain, url, CRAWL_TYPE_PLAIN)
        .catch(async (err) => {
          const page = await getLastPage(browserPlain);
          crawlLogError(
            `${err} on ${page.url()} for ${url}`,
            CRAWL_TYPE_PLAIN,
          );
          page.close();
        }),
      inspectURL(browserNoJS, url, CRAWL_TYPE_NO_JS)
        .catch(async (err) => {
          const page = await getLastPage(browserNoJS);
          crawlLogError(
            `${err} on ${page.url()} for ${url}`,
            CRAWL_TYPE_NO_JS,
          );
          page.close();
        }),
      inspectURL(browserAdBlocker, url, CRAWL_TYPE_AD_BLOCKER)
        .catch(async (err) => {
          const page = await getLastPage(browserAdBlocker);
          crawlLogError(
            `${err} on ${page.url()} for ${url}`,
            CRAWL_TYPE_AD_BLOCKER,
          );
          page.close();
        }),
    ]);

    const crawlURLResultsJSON = JSON.stringify({
      [CRAWL_TYPE_PLAIN]: crawlURLResults[0],
      [CRAWL_TYPE_NO_JS]: crawlURLResults[1],
      [CRAWL_TYPE_AD_BLOCKER]: crawlURLResults[2],
    });

    // Write the report in a JSON file
    await fs.promises.writeFile(
      `${crawlOutputDir}/${REPORTS_DIR}/${substituteURL(url)}.json`,
      crawlURLResultsJSON,
    ).catch((err) => {
      crawlLogError(`${err} on ${url}`, "");
    });

    // Write the index of the last completed URL to a file, to be able to resume
    // at this index the crawl if needed
    await fs.promises.writeFile(
      `${crawlOutputDir}/${URL_DONE_INDEX_NAME}`,
      startIndex + urlIndex,
    ).catch((err) => {
      crawlLogError(`${err} on ${url}`, "");
    });

    crawlLog(`${startIndex + urlIndex + 1}/${urlList.length} done.`, "all");
  }
  /* eslint-enable no-await-in-loop */

  crawlLog("Done.", "");
})();
