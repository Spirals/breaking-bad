# Crawl requests

## Dependencies
The following programs and Python modules are required to run the various
processing scripts.

- `bash` (a `bash`-compatible shell is required for process substitution with
  `<()` and `bash` is required for its `echo` command for now)
- `python` 3.5+
  - `matplotlib`
  - `numpy`
  - `pandas`
  - `seaborn`
  - `tabulate`
  - `lxml`
- `jq`
- `pup`
- `awk`
- `tar`

All of these are available in Debian's repositories.

The following are required on the server to run the crawl itself.

- `docker`/`podman`
- `tar`

## Processing Workflow
![Processing workflow](./doc/usage_diagram/usage_diagram.png)

## Crawl instructions
Instructions to run the crawl itself, on a dedicated server, can be found in
`CRAWL_SETUP.md`.

## License
Licensed under the Apache-2.0 license (see LICENSE).

Copyright (c) 2023 Inria
