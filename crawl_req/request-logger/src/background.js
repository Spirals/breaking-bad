"use strict";

const tabRequests = {};

const filterObjectProps = (obj, propArray) => {
  return Object.fromEntries(Object.entries(obj).filter(
    ([key, _]) => propArray.includes(key)
  ));
};

const onBeforeRequest = (details) => {
  const reqDetails = filterObjectProps(details, [
    "documentUrl",
    "originUrl",
    "frameId",
    "requestId",
    "thirdParty",
    "timeStamp",
    "method",
    "type",
    "url",
    "urlClassification",
  ]);

  const requestJSON = JSON.stringify(reqDetails);

  const tabId = details.tabId;

  browser.tabs.sendMessage(
    tabId,
    requestJSON,
  );
};

browser.webRequest.onBeforeRequest.addListener(
  onBeforeRequest,
  { urls: ["<all_urls>"] },
);
