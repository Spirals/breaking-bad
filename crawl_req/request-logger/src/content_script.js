"use strict";

const IDLE_TIME = 3000;
const JSON_ID = "request-logger-log";

const requests = [];

browser.runtime.onMessage.addListener((message) => {
  requests.push(message)
});

window.addEventListener("load", () => {
  setTimeout(() => {
    const jsonScriptElement = document.createElement("script");
    jsonScriptElement.type = "application/json";
    jsonScriptElement.id = JSON_ID;
    jsonScriptElement.textContent = JSON.stringify(requests);

    (document.head || document.documentElement).appendChild(jsonScriptElement);
  }, IDLE_TIME);
});
