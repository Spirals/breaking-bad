#!/usr/bin/env sh

set -o errexit
set -o nounset

script_dir=$(dirname "$0")

echo "actual_url,crawl_type,request_type,party_class,req_count"

for crawl_type in nojs plain; do
  find . -type f -path "*/*/crawl_crawl_urls_*/${crawl_type}/requestLog_*.json" -print0 \
    | xargs -P0 -0 -I{} "${script_dir}/get_request_count.sh" {} "${crawl_type}"
done
