#!/usr/bin/env sh

set -o errexit
set -o nounset

url_file=${1:?Specify URL file}

WEB_EXT_SOURCE_DIR=request-logger OTHER_EXTS=other_ext node index.js "${url_file}"
