#!/usr/bin/env python3

import argparse
import pathlib

import pandas as pd
import seaborn as sns
import matplotlib.pyplot as plt

import IPython

def plot(
    csv_path: pathlib.Path,
    output_path: pathlib.Path,
):
    df = pd.read_csv(csv_path)
    df = df.drop_duplicates(subset=[
        "actual_url",
        "crawl_type",
        "request_type",
        "party_class",
    ])
    print(df)

    df_plain = df[df["crawl_type"] == "plain"]
    df_nojs = df[df["crawl_type"] == "nojs"]

    print(df_plain.groupby("party_class").mean())

    print("Max:")
    print(df_plain[
        (df_plain["request_type"] == "script")
        & (df_plain["party_class"] == "firstParty")
    ].max())
    print(df_plain[
        (df_plain["request_type"] == "script")
        & (df_plain["party_class"] == "thirdParty")
    ].max())

    df_count = pd.DataFrame(
        index=[
            "First party", "Third party", "All",
            "--- Non-tracking",
            "--- Tracking",
            "--- Image",
            "--- Image ",
            "--- Stylesheet",
            "--- Stylesheet ",
            "--- Font",
            "--- Font ",
            "--- Script",
            "--- Script ",
            "--- XHR",
            "--- XHR ",
        ],
        columns=pd.MultiIndex.from_product([["[plain]", "[nojs]"], ["M", "SD"]]),
        data={
            ("[plain]", "M"): (
                df_plain[df_plain["request_type"] == "all"]
                    .groupby("party_class").mean()["req_count"].to_list()
                + df_plain[df_plain["request_type"].isin([
                        "all", "nontracking", "tracking",
                    ])]
                    .groupby(["actual_url", "crawl_type", "request_type"])
                    .sum()
                    .groupby("request_type")
                    .mean()["req_count"].to_list()
                + df_plain[df_plain["request_type"] == "image"]
                    .groupby("party_class")
                    .mean()["req_count"].to_list()
                + df_plain[df_plain["request_type"] == "stylesheet"]
                    .groupby("party_class")
                    .mean()["req_count"].to_list()
                + df_plain[df_plain["request_type"] == "font"]
                    .groupby("party_class")
                    .mean()["req_count"].to_list()
                + df_plain[df_plain["request_type"] == "script"]
                    .groupby("party_class")
                    .mean()["req_count"].to_list()
                + df_plain[df_plain["request_type"] == "xhr"]
                    .groupby("party_class")
                    .mean()["req_count"].to_list()
            ),
            ("[plain]", "SD"): (
                df_plain[df_plain["request_type"] == "all"]
                    .groupby("party_class").std()["req_count"].to_list()
                + df_plain[df_plain["request_type"].isin([
                        "all", "nontracking", "tracking",
                    ])]
                    .groupby(["actual_url", "crawl_type", "request_type"])
                    .sum()
                    .groupby("request_type")
                    .std()["req_count"].to_list()
                + df_plain[df_plain["request_type"] == "image"]
                    .groupby("party_class")
                    .std()["req_count"].to_list()
                + df_plain[df_plain["request_type"] == "stylesheet"]
                    .groupby("party_class")
                    .std()["req_count"].to_list()
                + df_plain[df_plain["request_type"] == "font"]
                    .groupby("party_class")
                    .std()["req_count"].to_list()
                + df_plain[df_plain["request_type"] == "script"]
                    .groupby("party_class")
                    .std()["req_count"].to_list()
                + df_plain[df_plain["request_type"] == "xhr"]
                    .groupby("party_class")
                    .std()["req_count"].to_list()
            ),
            ("[nojs]", "M"): (

                df_nojs[df_nojs["request_type"] == "all"]
                    .groupby("party_class").mean()["req_count"].to_list()
                + df_nojs[df_nojs["request_type"].isin([
                        "all", "nontracking", "tracking",
                    ])]
                    .groupby(["actual_url", "crawl_type", "request_type"])
                    .sum()
                    .groupby("request_type")
                    .mean()["req_count"].to_list()
                + df_nojs[df_nojs["request_type"] == "image"]
                    .groupby("party_class")
                    .mean()["req_count"].to_list()
                + df_nojs[df_nojs["request_type"] == "stylesheet"]
                    .groupby("party_class")
                    .mean()["req_count"].to_list()
                + df_nojs[df_nojs["request_type"] == "font"]
                    .groupby("party_class")
                    .mean()["req_count"].to_list()
                + df_nojs[df_nojs["request_type"] == "script"]
                    .groupby("party_class")
                    .mean()["req_count"].to_list()
                + df_nojs[df_nojs["request_type"] == "xhr"]
                    .groupby("party_class")
                    .mean()["req_count"].to_list()
            ),
            ("[nojs]", "SD"): (
                df_nojs[df_nojs["request_type"] == "all"]
                    .groupby("party_class").std()["req_count"].to_list()
                + df_nojs[df_nojs["request_type"].isin([
                        "all", "nontracking", "tracking",
                    ])]
                    .groupby(["actual_url", "crawl_type", "request_type"])
                    .sum()
                    .groupby("request_type")
                    .std()["req_count"].to_list()
                + df_nojs[df_nojs["request_type"] == "image"]
                    .groupby("party_class")
                    .std()["req_count"].to_list()
                + df_nojs[df_nojs["request_type"] == "stylesheet"]
                    .groupby("party_class")
                    .std()["req_count"].to_list()
                + df_nojs[df_nojs["request_type"] == "font"]
                    .groupby("party_class")
                    .std()["req_count"].to_list()
                + df_nojs[df_nojs["request_type"] == "script"]
                    .groupby("party_class")
                    .std()["req_count"].to_list()
                + df_nojs[df_nojs["request_type"] == "xhr"]
                    .groupby("party_class")
                    .std()["req_count"].to_list()
            ),
        },
    )

    df_count = df_count.reindex([
        "All",
        "--- Non-tracking",
        "--- Tracking",
        "First party",
        "--- Image",
        "--- Stylesheet",
        "--- Font",
        "--- Script",
        "--- XHR",
        "Third party",
        "--- Image ",
        "--- Stylesheet ",
        "--- Font ",
        "--- Script ",
        "--- XHR ",
    ])

    df_count.index.names = ["\\makecell[l]{Request\\\\count}"]

    df_count["\\makecell[l]{Mean\\\\change (\\%)}"] \
        = 100 * df_count.xs("M", level=1, axis="columns").T.pct_change().T["[nojs]"]

    df_count.to_latex(
        output_path,
        escape=False,
        float_format="{:0.1f}".format,
    )


if __name__ == "__main__":
    parser = argparse.ArgumentParser()
    parser.add_argument("csv_file", type=pathlib.Path, metavar="INPUT",
                        help="input CSV file")
    parser.add_argument("-o", type=pathlib.Path, metavar="OUTPUT", required=True,
                        dest="output_path", help="output LaTeX table")
    args = parser.parse_args()

    plot(args.csv_file, args.output_path)
