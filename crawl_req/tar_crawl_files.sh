#!/usr/bin/env sh

set -o errexit
set -o nounset

ext_dir="${1:?Specify the extension directory}"

dest_tar="crawl_$(date +%s).tar.gz"

tar -czpvf "${dest_tar}" \
  run_crawl.sh \
  index.js package.json \
  "${ext_dir}/manifest.json" "${ext_dir}/src"

echo "Created ${dest_tar}"
