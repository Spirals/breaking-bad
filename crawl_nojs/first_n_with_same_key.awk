BEGIN {
  key = $1;
  c = 0;
}
{
  if ($1 != key) {
    key = $1;
    c = 0;
  }

  if (c < n) {
    print;
    c++;
  }
}
