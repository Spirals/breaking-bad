#!/usr/bin/env bash

set -o errexit
set -o nounset

crawl_data_dir="${1:?Specify the crawl data dir}"

get_domains_from_csv() {
  awk -F, '{ print $1 }' \
    | sed 's/^https\?:\/\///' \
    | grep -o '^[^/]\+'
}

get_domains_for_noscript_type() {
  noscript_type=$1
  grep ",${noscript_type}," < "${crawl_data_dir}/noscript_tags.csv" \
    | get_domains_from_csv \
    | sort \
    | uniq
}

n_domain=$(tail -n +2  "${crawl_data_dir}/noscript_tags.csv" \
  | get_domains_from_csv \
  | sort \
  | uniq \
  | wc -l)

n_domain_iframe=$(comm -2 -3 \
  <(get_domains_for_noscript_type 'tracking_iframe') \
  <(get_domains_for_noscript_type 'tracking_pixel') | wc -l)

n_domain_pixel=$(comm -1 -3 \
  <(get_domains_for_noscript_type 'tracking_iframe') \
  <(get_domains_for_noscript_type 'tracking_pixel') | wc -l)

n_domain_both=$(comm -1 -2 \
  <(get_domains_for_noscript_type 'tracking_iframe') \
  <(get_domains_for_noscript_type 'tracking_pixel') | wc -l)

n_domain_tracking=$((n_domain_pixel + n_domain_iframe))

domains_tracking_only=$(comm -2 -3 \
  <(tail -n +2 "${crawl_data_dir}/noscript_tags.csv" \
  | grep ',tracking_pixel,\|,tracking_iframe,' \
  | get_domains_from_csv \
  | sort \
  | uniq) \
  <(tail -n +2 "${crawl_data_dir}/noscript_tags.csv" \
  | grep -v ',tracking_pixel,\|,tracking_iframe,' \
  | grep -v 'empty_tag' \
  | grep -v 'nextjs_css' \
  | grep -v 'html_comment' \
  | get_domains_from_csv \
  | sort \
  | uniq))

echo "${domains_tracking_only}"

n_domain_tracking_only=$(echo "${domains_tracking_only}" | wc -l)

echo "Noscript tag domain count: ${n_domain}"
echo "Only iframe domain count: ${n_domain_iframe}"
echo "Only pixel domain count: ${n_domain_pixel}"
echo "Both tracking method domain count: ${n_domain_both}"

echo "Noscript tracking domain count: ${n_domain_tracking}"
echo "Noscript tracking only domain count: ${n_domain_tracking_only}"

echo "Noscript tracking domain share: $((100 * (n_domain_pixel + n_domain_iframe - n_domain_both) / n_domain)) %"
echo "Of which, tracking only domain share: $((100 * n_domain_tracking_only / n_domain_tracking)) %"
