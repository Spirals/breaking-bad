#!/usr/bin/env python3

import argparse
import pathlib

import pandas as pd
import seaborn as sns
import matplotlib.pyplot as plt

from mpl_toolkits.axes_grid1 import make_axes_locatable
from mpl_toolkits.axes_grid1.axes_divider import make_axes_area_auto_adjustable

import IPython


def plot(
    csv_path: pathlib.Path,
    output_path: pathlib.Path,
):
    sns.set_theme(style="darkgrid")

    df = pd.read_csv(csv_path)
    df = df[[
        "list_url",
        "actual_url_nojs",
        "actual_url_plain",
        "inspection_time_nojs",
        "inspection_time_plain",
        "highlight_time_nojs",
        "highlight_time_plain",
    ]]
    df = df.drop_duplicates(subset=["list_url"])

    def get_cell(total: int, col: str, col2=None):
        if col2:
            count = len(df[(~df[col].isna()) & (~df[col2].isna())])
        else:
            count = len(df[~df[col].isna()])

        return f"{count} (\\SI{{{100.0 * count/total:.0f}}}{{\\%}})"

    total = len(df[~df["list_url"].isna()])

    df_count = pd.DataFrame(
        {
            "[plain]": [
                str(total),
                get_cell(total, "actual_url_plain"),
                get_cell(total, "inspection_time_plain"),
                get_cell(total, "highlight_time_plain"),
            ],
            "[nojs]": [
                str(total),
                get_cell(total, "actual_url_nojs"),
                get_cell(total, "inspection_time_nojs"),
                get_cell(total, "highlight_time_nojs"),
            ],
            "both": [
                str(total),
                get_cell(total, "actual_url_plain", "actual_url_nojs"),
                get_cell(total, "inspection_time_plain", "actual_url_nojs"),
                get_cell(total, "highlight_time_plain", "highlight_time_nojs"),
            ],
        },
        index=[
            "Crawled",
            "Loaded (1)",
            "Inspected (2)",
            "HTML saved (3)",
        ],
    )

    df_count = df_count.rename_axis("Page")
    df_count = df_count.rename_axis("Crawl type", axis="columns")

    df_count.to_latex(output_path, escape=False)


if __name__ == "__main__":
    parser = argparse.ArgumentParser()
    parser.add_argument("csv_file", type=pathlib.Path, metavar="INPUT",
                        help="input CSV file")
    parser.add_argument("-o", type=pathlib.Path, metavar="OUTPUT", required=True,
                        dest="output_path", help="output LaTeX table")
    args = parser.parse_args()

    plot(args.csv_file, args.output_path)
