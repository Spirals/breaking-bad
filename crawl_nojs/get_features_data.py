#!/usr/bin/env python3

import argparse
from pathlib import Path
import json
import re
from urllib.parse import urlparse

FEATURES = [
    "PageBodies",
    "StyleRegions",
    "TrackingPixels",
    "InPageAds",
    "AnalyticsScripts",
    "Forms",
    "LargeImages",
    "Images",
    "LoneControls",
    "EmptyAnchorButtons",
    "GoToTopAnchorButtons",
    "ExternalCommentSections",
    "FontStylesheets",
    "GraphDivs",
    "LanguageSelectInputs",
    "TabButtons",
    "FullPageApps",
    "LoaderOverlays",
    "FadeinElements",
    "CustomElements",
    "MicrobloggingWidgets",
    "CAPTCHAs",
    "RequestLoadingIndicators",
    "DisclosureButtons",
    "EmptyDivsWithId",
    "MislinkedFragmentAnchors",
    "IFrames",
    "Pagination",
    "ObfuscatedClassNameBody",
    "NoscriptWarnings",
    "Paywalls",
    "LargeIFrames",
    "CanvasTags",
    "CookieBanners",
    "ProtectedEmails",
]


def url_from_filename(filename: str) -> str:
    page_url = filename.removeprefix("featureList_")
    page_url = re.split(r'_[^_]+_\d+.json$', page_url)[0]
    return page_url.replace('_', '/')


def is_landing_page(url_str):
    url = urlparse(url_str)

    if url.path == ""\
        or url.path == "/"\
        or url.path == "/index.htm"\
            or url.path == "/index.html":
        return True

    if re.match(r'^/[a-z]{2}-[A-Za-z]{2}/?$', url.path):
        return True

    m = re.match(r'^/([a-z]{2})/?$', url.path)
    # TODO: update the list if needed
    # This list excludes two-letter pair which are neither
    # an ISO 3166-1 alpha-2 code or an ISO 639-1 code
    return bool(m) and m.group(1) not in [
        "cs",
        "ef",
        "hd",
        "kb",
        "vs",
    ]


def escape_csv_string(string) -> str:
    return f'"{string}"' if string.find(',') > -1 else string


def write_csv_line(
    csv_file,
    page_url: str,
    feature: str,
    crawl_type: str,
    prop: str,
    element_count: int,
    landing_page: bool,
    category: str,
):
    escaped_url = escape_csv_string(page_url)
    csv_line = f"\
{escaped_url},\
{feature},\
{crawl_type},\
{prop},\
{element_count},\
{landing_page},\
{category}\n"
    csv_file.write(csv_line)


PROPS = [
    "visible.main.broken",
    "visible.main.working",
    "visible.all.broken",
    "visible.all.working",
]

VISIBLE_AND_HIDDEN_FEATURES = [
    "PageBodies",
    "StyleRegions",
]

ALWAYS_ALL_FEATURES = [
    "PageBodies",
    "StyleRegions",
    "LoaderOverlays",
]


def write_features_for_crawl_type(
    path_filepath: Path,
    csv_file,
    website_categories: dict[str, list[str]],
    crawl_type: str,
):
    with open(path_filepath) as path_file:
        for json_path_str in path_file:
            json_path = Path(json_path_str.removesuffix("\n"))

            page_url = url_from_filename(str(json_path.name))

            domain = urlparse(page_url).hostname
            try:
                categories = website_categories[domain]
                if categories == []:
                    categories = [""]
            except KeyError:
                print(f"No categories for {domain}")
                categories = [""]

            with open(json_path) as json_file:
                data = json.load(json_file)

                for prop in PROPS:
                    keys = prop.split(".")

                    for feature in FEATURES:
                        feature_name = "find" + feature

                        page_region = "all" if feature in ALWAYS_ALL_FEATURES else keys[1]
                        element_count\
                            = data[feature_name][keys[0]][page_region][keys[2]]

                        # Some features should also be counted both when hidden
                        # and visible
                        if feature in VISIBLE_AND_HIDDEN_FEATURES:
                            visibility = "visible" if keys[0] == "hidden" else "hidden"
                            element_count += data[feature_name][visibility][keys[1]][keys[2]]

                        landing_page = is_landing_page(page_url)

                        for category in categories:
                            write_csv_line(
                                csv_file,
                                page_url,
                                feature,
                                crawl_type,
                                prop,
                                element_count,
                                landing_page,
                                category,
                            )


def group_categories(categories: list[str]):
    CATEGORY_MAPPINGS = {
        "AdultThemes": "Adult",
        "Lingerie/Bikini": "Adult",
        "Nudity": "Adult",
        "Pornography": "Adult",
        "Sexuality": "Adult",
        "Tasteless": "Adult",
        "Auctions": "Ecommerce/Shopping…",
        "Classifieds": "Ecommerce/Shopping…",
        "Ecommerce/Shopping": "Ecommerce/Shopping…",
        "Weapons": None,  # Already included in other categories
        "Malware": None,
        "Proxy/Anonymizer": "Software/Technology",
        "ContentDeliveryNetworks": None,
    }

    for i, category in enumerate(categories):
        if category in CATEGORY_MAPPINGS:
            if CATEGORY_MAPPINGS[category]:
                categories[i] = CATEGORY_MAPPINGS[category]
        else:
            categories[i] = category

    return list(set(categories))

def write_features_data(
    plain: Path,
    nojs: Path,
    categories: Path,
    csv_path: Path,
):
    website_categories = {}
    with open(categories) as categories_file:
        for line in categories_file:
            fields = line.removesuffix('\n').split(' ')
            domain = fields[0]
            cat = fields[1].split(',') if fields[1] != '' else []
            website_categories[domain] = group_categories(cat)

    CSV_HEADERS = '\
page_url,\
feature,\
crawl_type,\
prop,\
element_count,\
is_landing_page,\
category\n'

    with open(csv_path, 'w') as csv_file:
        csv_file.write(CSV_HEADERS)

        write_features_for_crawl_type(
            plain,
            csv_file,
            website_categories,
            "plain",
        )
        write_features_for_crawl_type(
            nojs,
            csv_file,
            website_categories,
            "nojs",
        )


if __name__ == "__main__":
    parser = argparse.ArgumentParser()
    parser.add_argument("--plain", type=Path, required=True,
                        help="file listing the json plain files")
    parser.add_argument("--nojs", type=Path, required=True,
                        help="file listing the json nojs files")
    parser.add_argument("--categories", type=Path, required=True,
                        help="website categories")
    parser.add_argument("-o", type=Path, required=True,
                        metavar="OUTPUT", dest="csv_path",
                        help="output CSV file")
    args = parser.parse_args()

    write_features_data(
        args.plain,
        args.nojs,
        args.categories,
        args.csv_path,
    )
