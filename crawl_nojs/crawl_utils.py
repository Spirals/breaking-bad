import re
from urllib.parse import urlparse


def url_from_json_filename(filename: str) -> str:
    page_url = filename.removeprefix("featureList_")
    page_url = re.split(r'_[^_]+_\d+.json$', page_url)[0]
    return page_url.replace('_', '/')


def url_from_html_filename(filename: str) -> str:
    page_url = re.split(r'_[^_]+_\d+.html$', filename)[0]
    return page_url.replace('_', '/')


def is_landing_page(url_str):
    url = urlparse(url_str)

    if url.path == ""\
        or url.path == "/"\
        or url.path == "/index.htm"\
            or url.path == "/index.html":
        return True

    if re.match(r'^/[a-z]{2}-[A-Za-z]{2}/?$', url.path):
        return True

    m = re.match(r'^/([a-z]{2})/?$', url.path)
    # TODO: update the list if needed
    # This list excludes two-letter pair which are neither
    # an ISO 3166-1 alpha-2 code or an ISO 639-1 code
    return bool(m) and m.group(1) not in [
        "cs",
        "ef",
        "hd",
        "kb",
        "vs",
    ]


def escape_csv_string(string) -> str:
    should_escape = string.find(",") or string.find('"')
    string = string.replace('"', '\\"')

    return f'"{string}"' if should_escape > -1 else string
