#!/usr/bin/env python3

import argparse
import pathlib

import numpy as np
import pandas as pd
import seaborn as sns
import matplotlib.pyplot as plt
from matplotlib.ticker import MultipleLocator
from mpl_toolkits.axes_grid1 import make_axes_locatable
from mpl_toolkits.axes_grid1.axes_divider import make_axes_area_auto_adjustable
import matplotlib.transforms as transforms
from matplotlib.patches import Rectangle
from matplotlib.lines import Line2D

import IPython

FEATURES_NOT_TO_DIFF = [
    "InPageAds",
    "AnalyticsScripts",
    "AnalyticsIFrames",
    "PingAnchors",
    "CookieBanners",
    "Paywalls",
    "NoScriptFallbacks",
    "NoScriptWarnings",
    "CanvasTags",
    "ExternalCommentSections",
    "MicrobloggingWidgets",
    "CAPTCHAs",
    "RequestLoadingIndicators",
    "ObfuscatedClassNameBody",
    "LoaderOverlays",
    "FadeinElements",
    "CustomElements",
]

INTERACTIVE_FEATURES = [
    "Forms",
    "LoneControls",
    "EmptyAnchorButtons",
    "MislinkedFragmentAnchors",
    # "Carousels",
    "DisclosureButtons",
    # "Pagination",
    # "TabButtons",
    # "FullPageApps",
]

MAIN_FEATURES = INTERACTIVE_FEATURES + [
    "PageBodies",
    "StyleRegions",
    # "FullPageApps",
    "LoaderOverlays",
    "LargeImages",
]

HEATMAP_PLOT_FEATURES = [
    "Forms",
    "LoneControls",
    "EmptyAnchorButtons",
    "DisclosureButtons",
    "LargeImages",
    "Interactive features",
    "Main features",
    # "PageBodies",
    # "StyleRegions",
]

CDF_PLOT_FEATURES = [
    "MislinkedFragmentAnchors",
    "DisclosureButtons",
    # "TabButtons",
    # "LoaderOverlays",
    # "FullPageApps",
    "EmptyAnchorButtons",
    "LoneControls",
    "Forms",
    "Interactive features",
    "StyleRegions",
    "LargeImages",
    "PageBodies",
    "Main features",
]

BAR_PLOT_FLEATURES = CDF_PLOT_FEATURES

RENAME_FEATURES = {
    "PageBodies": "Page Text",
    "StyleRegions": "Stylesheets Loaded",
    "LoneControls": "Lone Controls",
    "EmptyAnchorButtons": "Empty\nAnchor Buttons",
    "DisclosureButtons": "Disclosure Buttons",
    "TabButtons": "Tab Buttons",
    "MislinkedFragmentAnchors": "Mislinked\nFragment Anchors",
    "FullPageApps": "Single-page Apps",
    "LoaderOverlays": "Loader Overlays",
    "LargeImages": "Large Images",
}


def get_cat_count_label_fn(pages_with_category_count, category_counts):
    def get_cat_count_label(cat_label):
        category = cat_label.get_text()
        cat_count = category_counts.at[category, "page_count"]
        return f"{category} ({cat_count})"

    return get_cat_count_label


def get_element_count_quant_str(df):
    df_quant = df.groupby(["category", "feature"])\
        .quantile([0.5, 0.75, 0.9, 0.95])

    df_quant_str = df_quant\
        .reset_index()[[
            "category",
            "feature",
            "element_count_diff_broken",
            "element_count_nojs_total",
        ]]

    df_quant_str["element_count_diff_broken"]\
        = df_quant_str["element_count_diff_broken"]\
        .apply("{:4.1f} ".format)

    df_quant_str["element_count_nojs_total"]\
        = df_quant_str["element_count_nojs_total"]\
        .apply("{:4.1f} ".format)

    df_quant_str["element_count_diff_broken_ratio_str"]\
        = df_quant_str["element_count_diff_broken"]\
        + df_quant_str["element_count_nojs_total"]

    df_quant_str = df_quant_str\
        .groupby(["category", "feature"], as_index=False)\
        .agg({
            "element_count_diff_broken_ratio_str":
                lambda c: "\n".join(c.astype(str)),
        })

    df_quant_str = df_quant_str[[
        "feature",
        "category",
        "element_count_diff_broken_ratio_str",
    ]]\
        .pivot(
            "feature",
            "category",
            "element_count_diff_broken_ratio_str",
        )

    return df_quant, df_quant_str


def get_dist_plot_fig(
    df_broken,
    pages_with_category_count,
    category_counts,
    large_categories,
):
    df_element_count_diff_broken_quant,\
        df_element_count_diff_broken_quant_str = get_element_count_quant_str(
            df_broken[[
                "category",
                "feature",
                "element_count_diff_broken",
                "element_count_nojs_total",
                "element_count_nojs_broken_ratio_perc",
            ]],
        )

    df_element_count_diff_broken_quant_str\
        = df_element_count_diff_broken_quant_str\
        .loc[HEATMAP_PLOT_FEATURES]\
        [large_categories]\
        .reindex(HEATMAP_PLOT_FEATURES)\
        .rename(index=RENAME_FEATURES)

    df_element_count_diff_broken_quant\
        = df_element_count_diff_broken_quant[
            df_element_count_diff_broken_quant
                .index.get_level_values("feature")
                .isin(HEATMAP_PLOT_FEATURES)
        ]

    df_element_count_diff_broken_quant\
        = df_element_count_diff_broken_quant\
        .reset_index()

    df_colors_090 = df_element_count_diff_broken_quant.copy()
    df_colors_090 = df_colors_090[df_colors_090["level_2"] == 0.90]
    df_colors_090 = df_colors_090[["feature", "category", "element_count_diff_broken"]]\
        .pivot("feature", "category", "element_count_diff_broken")\
        .reindex(HEATMAP_PLOT_FEATURES)\
        [large_categories]\
        .rename(index=RENAME_FEATURES)

    print(df_colors_090.axes)

    fig_dist, ax_dist = plt.subplots(1, 1, figsize=(16, 6))

    ax_dist = sns.heatmap(
        df_colors_090,
        vmin=0,
        vmax=10,
        xticklabels=True,
        # annot=df_element_count_diff_broken_quant_str,
        # fmt="",
        square=True,
        # cbar=False,
        cbar_kws={"shrink": 0.80},
        ax=ax_dist,
    )

    xlabels = list(map(
        get_cat_count_label_fn(pages_with_category_count, category_counts),
        ax_dist.get_xticklabels(),
    ))
    ax_dist.set_xticklabels(xlabels, rotation=30, ha="right")
    ax_dist.set_yticklabels(ax_dist.get_yticklabels(), va="center")
    ax_dist.set_ylabel("")
    ax_dist.set_xlabel("")

    make_axes_area_auto_adjustable(ax_dist)

    return fig_dist


def get_cdf_dist_plot(df_broken_interactive_cnt, region: str):
    df_ecdf_broken_ratio = df_broken_interactive_cnt[[
        "page_url",
        "feature",
        "element_count_nojs_broken_ratio_perc",
    ]].drop_duplicates()
    df_ecdf_broken_ratio["Page region"] = "main"

    df_ecdf_broken_ratio = df_ecdf_broken_ratio[
        df_ecdf_broken_ratio["feature"].isin(CDF_PLOT_FEATURES)
    ]

    df_ecdf_broken_ratio = df_ecdf_broken_ratio.replace({
        "feature": RENAME_FEATURES,
    })

    sns.set_theme(style="whitegrid")
    fig_perc_dist = sns.displot(
        df_ecdf_broken_ratio,
        x="element_count_nojs_broken_ratio_perc",
        hue="feature",
        kind="ecdf",
        # palette="husl",
    )

    fig_perc_dist.ax.yaxis.set_major_locator(MultipleLocator(0.1))
    fig_perc_dist.ax.yaxis.set_minor_locator(MultipleLocator(0.01))

    fig_perc_dist.ax.xaxis.set_major_locator(MultipleLocator(25))
    fig_perc_dist.ax.xaxis.set_minor_locator(MultipleLocator(5))

    fig_perc_dist.ax.grid(True, "minor", color="lightgray", linewidth=0.2)

    fig_perc_dist.ax.set_xlim([0, 100])
    fig_perc_dist.ax.set_ylim([0.6, 1])

    fig_perc_dist.ax.set_ylabel("CDF")

    fig_perc_dist.ax.set_xlabel(
        f"Normalized differential breakage of visible elements, {region} (%)"
    )
    return fig_perc_dist


def get_new_sum_feature(df, features_to_sum, new_feature_name):
    def sum_feature_counts(gr):
        df_to_sum = gr[gr["feature"].isin(features_to_sum)]

        sum_row = df_to_sum\
            .groupby(
                ["page_url", "category", "page_part"],
                dropna=False,
                as_index=False,
            )\
            .sum()

        total = df_to_sum["element_count_nojs_total"].sum()

        if total == 0:
            sum_row["element_count_nojs_broken_ratio_perc"] = 0
        else:
            sum_row["element_count_nojs_broken_ratio_perc"]\
                = 100 * df_to_sum["element_count_diff_broken"].sum()\
                / total

        sum_row["feature"] = new_feature_name
        # print(df_to_sum)
        # print(sum_row)
        return gr.append(sum_row, ignore_index=True)

    return df[[
        "page_url",
        "is_landing_page",
        "feature",
        "category",
        "page_part",
        "element_count_nojs_broken",
        "element_count_diff_broken",
        "element_count_nojs_total",
        "element_count_nojs_broken_ratio_perc",
    ]]\
        .groupby(["page_url", "category", "page_part"], dropna=False, as_index=False)\
        .apply(sum_feature_counts)


def get_category_counts(csv_path: pathlib.Path):
    df = pd.read_csv(csv_path)
    df = df.drop_duplicates()

    dfnojs = df[df["crawl_type"] == "nojs"]

    category_counts = dfnojs[["category", "page_url"]]\
        .dropna()\
        .drop_duplicates()
    category_counts = category_counts\
        .groupby("category")\
        .count()\
        .rename(columns={"page_url": "page_count"})
    pages_with_category_count = category_counts.sum()[0]

    return category_counts, pages_with_category_count


def get_dfnojsplain_brokenworking_interactive_count(csv_path: pathlib.Path):
    df = pd.read_csv(csv_path)
    print("Raw counts:")
    print(df.count(), "\n")
    df = df.drop_duplicates()
    print("Dropped duplicates counts:")
    print(df.count(), "\n")

    df_prop_split\
        = df["prop"].str.extract(r"([A-Za-z_]+)\.([A-Za-z_]+)\.([A-Za-z_]+)")
    df["feature_visibility"] = df_prop_split[0]
    df["page_part"] = df_prop_split[1]
    df["feature_status"] = df_prop_split[2]
    # df = df.drop(columns="prop")

    dfnojs = df[df["crawl_type"] == "nojs"]
    dfplain = df[df["crawl_type"] == "plain"]

    print("Counts for nojs:")
    print(dfnojs.drop_duplicates(subset="page_url").count(), "\n")
    print("Counts for plain:")
    print(dfplain.drop_duplicates(subset="page_url").count(), "\n")

    join_cols = ["page_url", "feature", "category"]

    dfnojsplain = dfnojs.merge(
        dfplain[join_cols + ["prop", "element_count"]],
        how="inner",
        on=join_cols + ["prop"],
        suffixes=('', '_plain'),
    )
    dfnojsplain = dfnojsplain.rename(
        {"element_count": "element_count_nojs"},
        axis="columns",
    )
    print("Counts for nojsplain:")
    print(dfnojsplain.drop_duplicates(subset="page_url").count(), "\n")
    print(dfnojsplain)

    dfnojsplain["element_count_diff"] = dfnojsplain["element_count_nojs"]

    dfnojsplain.loc[
        ~dfnojsplain["feature"].isin(FEATURES_NOT_TO_DIFF),
        "element_count_diff",
    ]\
        = dfnojsplain["element_count_nojs"]\
        - dfnojsplain["element_count_plain"]

    print("dfnojsplain")
    print(dfnojsplain)

    dfnojsplain_broken\
        = dfnojsplain[(dfnojsplain["prop"] == "visible.main.broken")
                      | (dfnojsplain["prop"] == "visible.all.broken")]
    dfnojsplain_working\
        = dfnojsplain[(dfnojsplain["prop"] == "visible.main.working")
                      | (dfnojsplain["prop"] == "visible.all.working")]

    dfnojsplain_brokenworking = dfnojsplain_broken.merge(
        dfnojsplain_working[join_cols + [
            "feature_visibility",
            "page_part",
            "element_count_nojs",
            "element_count_plain",
            "element_count_diff",
        ]],
        how="inner",
        on=join_cols + ["feature_visibility", "page_part"],
        suffixes=('', '_working'),
    )
    dfnojsplain_brokenworking = dfnojsplain_brokenworking.rename(
        {
            "element_count_nojs": "element_count_nojs_broken",
            "element_count_plain": "element_count_plain_broken",
            "element_count_diff": "element_count_diff_broken",
        },
        axis="columns",
    )
    dfnojsplain_brokenworking["element_count_nojs_total"]\
        = dfnojsplain_brokenworking["element_count_nojs_broken"]\
        + dfnojsplain_brokenworking["element_count_nojs_working"]

    dfnojsplain_brokenworking["element_count_nojs_broken_ratio_perc"]\
        = 100 * dfnojsplain_brokenworking["element_count_diff_broken"]\
        / dfnojsplain_brokenworking["element_count_nojs_total"]

    dfnojsplain_brokenworking["element_count_nojs_broken_ratio_perc"]\
        = dfnojsplain_brokenworking["element_count_nojs_broken_ratio_perc"]\
        .replace([np.nan, np.inf, -np.inf], value=0)

    dfnojsplain_brokenworking["element_count_plain_total"]\
        = dfnojsplain_brokenworking["element_count_plain_broken"]\
        + dfnojsplain_brokenworking["element_count_plain_working"]
    print(dfnojsplain_brokenworking.axes)
    print(dfnojsplain_brokenworking)

    dfnojsplain_brokenworking_interactive_count = get_new_sum_feature(
        dfnojsplain_brokenworking,
        INTERACTIVE_FEATURES,
        "Interactive features",
    )

    # dfnojsplain_brokenworking_interactive_count = get_new_sum_feature(
    #     dfnojsplain_brokenworking_interactive_count,
    #     MAIN_FEATURES,
    #     "Main features",
    # )

    def max_broken(gr):
        df_to_max = gr[gr["feature"].isin(MAIN_FEATURES)]
        # print(df_to_max)

        max_row = df_to_max.max()
        max_row["feature"] = "Main features"

        return gr.append(max_row, ignore_index=True)

    dfnojsplain_brokenworking_interactive_count =\
        dfnojsplain_brokenworking_interactive_count[[
            "page_url",
            "is_landing_page",
            "feature",
            "category",
            "page_part",
            "element_count_nojs_broken",
            "element_count_diff_broken",
            "element_count_nojs_total",
            "element_count_nojs_broken_ratio_perc",
        ]]\
            .groupby(["page_url", "category", "page_part"], dropna=False, as_index=False)\
            .apply(max_broken)

    return dfnojsplain_brokenworking_interactive_count


def add_ylabel_group(
    ax,
    xoffset: float,
    yoffset: float,
    width: float,
    height: float,
    label: str,
    bgcolor:str ="whitesmoke",
    linecolor: str ="darkgray",
):
    trans = transforms.blended_transform_factory(
        ax.transAxes,
        ax.transData,
    )

    make_axes_area_auto_adjustable(ax)

    rect = Rectangle(
        (xoffset, yoffset),
        width=width,
        height=height,
        transform=trans,
        clip_on=False,
        facecolor=bgcolor,
    )
    ax.add_patch(rect)

    min_line = Line2D(
        (xoffset, xoffset + width),
        (yoffset, yoffset),
        transform=trans,
        clip_on=False,
        color="gray",
    )
    ax.add_artist(min_line)

    max_line = Line2D(
        (xoffset, xoffset + width),
        (yoffset + height, yoffset + height),
        transform=trans,
        clip_on=False,
        color=linecolor,
    )
    ax.add_artist(max_line)

    ax.text(
        xoffset + width/2,
        yoffset + height/2,
        label,
        ha="center",
        va="center",
        transform=trans,
        clip_on=False,
        rotation="vertical",
    )


def get_bar_plot(df):
    df_page_broken_status = df[[
        "page_url",
        "feature",
        "page_part",
        "element_count_nojs_total",
        "element_count_diff_broken",
        "element_count_nojs_broken_ratio_perc",
    ]].drop_duplicates()
    # df_page_broken_status = df_page_broken_status[
    #     df_page_broken_status["feature"] == "Main features"
    # ]

    df_page_broken_status = df_page_broken_status[
        df_page_broken_status["page_part"] == "all"
    ].merge(
        df_page_broken_status[df_page_broken_status["page_part"] == "main"],
        on=["page_url", "feature"],
        suffixes=("_all", "_main"),
    ).drop(["page_part_all", "page_part_main"], axis="columns")

    # Whole page at least partly broken
    df_page_broken_status["broken_status"] = "Broken in main section"

    # Whole page completely working
    df_page_broken_status.loc[
        (df_page_broken_status["element_count_nojs_broken_ratio_perc_all"] <= 0)
        & (df_page_broken_status["element_count_nojs_broken_ratio_perc_main"] <= 0),
        "broken_status"
    ] = "Working in whole page"

    # Main page section completely working
    df_page_broken_status.loc[
        (df_page_broken_status["element_count_nojs_broken_ratio_perc_all"] > 0)
        & (df_page_broken_status["element_count_nojs_broken_ratio_perc_main"] <= 0),
        "broken_status"
    ] = "Working only in main section"

    df_page_broken_status.loc[
        df_page_broken_status["element_count_nojs_total_main"] == 0,
        "broken_status"
    ] = "No elements in main section"

    df_page_broken_status.loc[
        df_page_broken_status["element_count_nojs_total_all"] == 0,
        "broken_status"
    ] = "No elements in whole page"

    print(df_page_broken_status[
        df_page_broken_status["feature"].isin(BAR_PLOT_FLEATURES)
    ][["feature", "broken_status"]]
        .groupby("feature")["broken_status"]
        .value_counts())

    df_page_broken_status_main = 100 * df_page_broken_status[
        df_page_broken_status["feature"].isin(BAR_PLOT_FLEATURES)
    ][["feature", "broken_status"]]\
        .groupby("feature")["broken_status"]\
        .value_counts(normalize=True)\
        .unstack()\
        .replace(np.nan, value=0)

    df_page_broken_status_main = df_page_broken_status_main[[
        "No elements in whole page",
        "Working in whole page",
        "No elements in main section",
        "Working only in main section",
        "Broken in main section",
    ]]

    df_page_broken_status_main\
        = df_page_broken_status_main.reindex(BAR_PLOT_FLEATURES)

    df_page_broken_status_main = df_page_broken_status_main.rename(
        index=RENAME_FEATURES,
    )

    print(df_page_broken_status_main)

    plt.figure()

    ax_count = df_page_broken_status_main\
        .plot.barh(
            stacked=True,
            color=["darkseagreen", "seagreen", "navajowhite", "orange", "firebrick"],
            figsize=(12, 12),
        )

    ax_count.legend(bbox_to_anchor=(1, 1), loc="lower right")

    ax_count.xaxis.set_major_locator(MultipleLocator(10))
    ax_count.xaxis.set_minor_locator(MultipleLocator(5))
    ax_count.grid(True, "minor", color="lightgray", linewidth=0.2)

    ax_count.set_ylabel("")
    # ax_count.set_yticks([])
    ax_count.set_xlabel("Pages (%)")

    xoffset = -0.35
    width = -0.06

    add_ylabel_group(
        ax_count,
        xoffset,
        -0.5 + 6,
        width,
        3,
        "Page content",
    )

    add_ylabel_group(
        ax_count,
        xoffset,
        -0.5,
        width,
        5,
        "Interactive features",
    )

    return ax_count, df_page_broken_status


CACHE_CSV_FILENAME = "cache_dfnojsplain_brokenworking_interactive_count.csv"


def plot(
    csv_path: pathlib.Path,
    output_dir: pathlib.Path,
    output_ext: str,
    show_gui: bool,
):
    # sns.set_theme(style="ticks", font="monospace")
    sns.set_theme(style="ticks")

    try:
        dfnojsplain_brokenworking_interactive_count\
            = pd.read_csv(CACHE_CSV_FILENAME)
    except FileNotFoundError:
        dfnojsplain_brokenworking_interactive_count\
            = get_dfnojsplain_brokenworking_interactive_count(csv_path)
        dfnojsplain_brokenworking_interactive_count.to_csv(CACHE_CSV_FILENAME)

    df_main_broken = dfnojsplain_brokenworking_interactive_count[
        dfnojsplain_brokenworking_interactive_count["page_part"] == "main"
    ]
    df_all_broken = dfnojsplain_brokenworking_interactive_count[
        dfnojsplain_brokenworking_interactive_count["page_part"] == "all"
    ]

    category_counts, pages_with_category_count\
        = get_category_counts(csv_path)

    large_categories = list(reversed(
        category_counts[
            category_counts["page_count"] > 50]
            .sort_values(by="page_count")
            .index.to_list()
    ))

    print(category_counts)
    print("Pages with category count: ", pages_with_category_count)
    print("Category count: ", category_counts.count()[0])
    print(large_categories)

    fig_all_dist = get_dist_plot_fig(
        df_all_broken,
        pages_with_category_count,
        category_counts,
        large_categories,
    )
    fig_main_dist = get_dist_plot_fig(
        df_main_broken,
        pages_with_category_count,
        category_counts,
        large_categories,
    )

    fig_main_perc_dist = get_cdf_dist_plot(
        df_main_broken,
        "main section",
    )

    fig_all_perc_dist = get_cdf_dist_plot(
        df_all_broken,
        "whole page",
    )

    ax_count, df_page_broken_status = get_bar_plot(dfnojsplain_brokenworking_interactive_count)

    df_page_broken_status["element_count_diff_broken_section_diff"]\
        = 100 * (df_page_broken_status["element_count_diff_broken_all"]
                 - df_page_broken_status["element_count_diff_broken_main"])\
        / df_page_broken_status["element_count_diff_broken_all"]

    sns.set_theme(style="whitegrid")
    section_diff_features = CDF_PLOT_FEATURES
    section_diff_features.remove("PageBodies")
    section_diff_features.remove("StyleRegions")
    section_diff_features.remove("Main features")

    fig_section_diff = sns.displot(
        df_page_broken_status[
            (df_page_broken_status["element_count_nojs_broken_ratio_perc_all"] > 0)
            & (df_page_broken_status["feature"].isin(section_diff_features))
        ],
        x="element_count_diff_broken_section_diff",
        hue="feature",
        kind="ecdf",
        # palette="husl",
    )

    fig_section_diff.ax.yaxis.set_major_locator(MultipleLocator(0.1))
    fig_section_diff.ax.yaxis.set_minor_locator(MultipleLocator(0.05))

    # fig_section_diff.ax.xaxis.set_major_locator(MultipleLocator(100))
    # fig_section_diff.ax.xaxis.set_minor_locator(MultipleLocator(50))

    fig_section_diff.ax.grid(True, "minor", color="lightgray", linewidth=0.2)

    fig_section_diff.ax.set_xlim([-100, 200])

    fig_section_diff.ax.set_ylabel("CDF")

    if output_dir:
        output_extension = "png" if output_ext == "png" else "pdf"

        fig_main_dist.savefig(
            output_dir.joinpath(f"feature_main_dist.{output_extension}"),
            bbox_inches="tight",
        )

        fig_all_dist.savefig(output_dir.joinpath(
            f"feature_all_dist.{output_extension}"),
            bbox_inches="tight",
        )

        fig_main_perc_dist.savefig(
            output_dir.joinpath(f"feature_main_perc_dist.{output_extension}"),
            bbox_inches="tight",
        )

        fig_all_perc_dist.savefig(
            output_dir.joinpath(f"feature_all_perc_dist.{output_extension}"),
            bbox_inches="tight",
        )

        ax_count.get_figure().savefig(
            output_dir.joinpath(f"feature_count_broken_status.{output_extension}"),
            bbox_inches="tight",
        )

        fig_section_diff.savefig(
            output_dir.joinpath(f"feature_section_diff.{output_extension}"),
            bbox_inches="tight",
        )

    if show_gui is True:
        plt.show()


if __name__ == "__main__":
    parser = argparse.ArgumentParser()
    parser.add_argument("csv_file", type=pathlib.Path,  metavar="INPUT",
                        help="input CSV file")
    parser.add_argument("--output-dir", type=pathlib.Path, metavar="OUTPUT",
                        dest="output_dir", help="directory to store figures")
    parser.add_argument("--output-ext", dest="output_ext",
                        help="output format extension")
    parser.add_argument("--gui", action=argparse.BooleanOptionalAction,
                        default=True, help="toggle the GUI")
    args = parser.parse_args()

    plot(args.csv_file, args.output_dir, args.output_ext, args.gui)
