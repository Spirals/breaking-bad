#!/usr/bin/env python3

import argparse
import pathlib
import re

import pandas as pd
import seaborn as sns

import matplotlib.pyplot as plt
from mpl_toolkits.axes_grid1 import make_axes_locatable
from mpl_toolkits.axes_grid1.axes_divider import make_axes_area_auto_adjustable

from lxml import html
from lxml.cssselect import CSSSelector

import IPython

import crawl_utils

NOSCRIPT_DISPLAY_NAMES = {
    "image_fallback": "Image fallback",
    "tracking_iframe": "Tracking iframe",
    "empty_tag": "Empty tag",
    "tracking_pixel": "Tracking pixel",
    "image_lazy_fallback": "Image fallback",  # Same as image_fallback
    "other": "Other",
    "other_multiple_children": "Other",
    "noscript_warning": "Noscript warning",
    "style_only": "Style element",
    "async_fallback_stylesheet": "Async. stylesheet fallback",
    "anchor": "Anchor",
    "other_stylesheet": "Other stylesheet",
    "other_iframe": "Other iframe",
    "tracking_social_button_iframe": "Tracking social button iframe",
    "embedded_video_iframe": "Embedded video iframe",
    "video_tag": "Video tag",
    "ads_iframe": "Advertisement iframe",
    "noscript_custom_stylesheet": "Noscript-dedicated stylesheet",
    "meta": "Meta tag",
    "text_only": "Text only",
    "nextjs_css": "Next.js CSS nonce",
    "html_comment": "HTML comment",
    "ie_fallback": "Internet Explorer fallback",
}

def write_csv_line(
    csv_file,
    page_url: str,
    landing_page: bool,
    noscript_el_count: int,
    noscript_type: str,
    noscript_display_name: str,
    noscript_tag: str,
):
    escaped_url = crawl_utils.escape_csv_string(page_url)
    escaped_noscript_tag = crawl_utils.escape_csv_string(noscript_tag)
    csv_line = f"\
{escaped_url},\
{landing_page},\
{noscript_el_count},\
{noscript_type},\
{noscript_display_name},\
{escaped_noscript_tag}\n"
    csv_file.write(csv_line)


def has_noscript_warning(noscript_el):
    text_content = noscript_el.text_content()
    return re.search(r"javascript", text_content, re.IGNORECASE)\
        or re.search(r"JS", text_content)


# Do not use this to spot tracking pixel for sure, some websites request remote
# 1x1 placeholder images
def is_img_single_pixel(img_el):
    try:
        if  img_el.attrib["width"] == "1"\
            and img_el.attrib["height"] == "1":
            return True
    except KeyError:
        return False


def has_tracking_pixel(noscript_tag: str):
    return ("/matomo." in noscript_tag
            or "//piwik." in noscript_tag
            or "//www.facebook.com/tr" in noscript_tag
            or "//sb.scorecardresearch.com/p" in noscript_tag
            or "//b.scorecardresearch.com/p" in noscript_tag
            or "//googleads.g.doubleclick.net/pagead/" in noscript_tag
            or "//pixiedust.buzzfeed.com" in noscript_tag
            or "//mc.yandex.ru/watch/" in noscript_tag
            or "//analytics." in noscript_tag
            or "//tracking." in noscript_tag
            or "//pixel." in noscript_tag
            or "//stats." in noscript_tag
            or "//beacon." in noscript_tag
            or "/tracking/" in noscript_tag
            or "/pixel_" in noscript_tag
            or "/pixel." in noscript_tag
            or "/pixel?" in noscript_tag
            or "/atrk.gif" in noscript_tag
            or "/px.gif" in noscript_tag
            or "/noScript.gif" in noscript_tag
            or "/noscript.gif" in noscript_tag
            or "/s.gif" in noscript_tag
            or "/t.gif" in noscript_tag
            or "/n.gif" in noscript_tag
            or "/analysis-logger/" in noscript_tag
            or "/pvg.gif" in noscript_tag
            or "/piwik.php" in noscript_tag
            or "mail.ru/counter?" in noscript_tag
            or "imrworldwide.com" in noscript_tag
            or "ads.linkedin.com/collect/" in noscript_tag
            or "ct.pinterest.com" in noscript_tag
            or "statcounter.com" in noscript_tag
            or "counter.ukr.net" in noscript_tag
            or "ssitecat.eset.com" in noscript_tag
            or "somni.accenture.com" in noscript_tag
            or "webtrekk.net" in noscript_tag
            or "googletagmanager.com/ns.html" in noscript_tag
            or "/ma/m.cd?" in noscript_tag
            or "rtmark.net/img.gif" in noscript_tag
            or "effectivemeasure." in noscript_tag
            or "getclicky.com" in noscript_tag
            or "counter.rambler.ru" in noscript_tag
            or "bat.bing.com" in noscript_tag
            or "tracker.marinsm.com" in noscript_tag
            or "tracksrv.zdf.de" in noscript_tag
            or "wa.gmx.net" in noscript_tag
            or "wa.web.de" in noscript_tag
            or "wa.mail.com" in noscript_tag
            or "googleadservices.com/pagead/conversion/" in noscript_tag
            or "/hit.xiti" in noscript_tag
            or "xiti.com" in noscript_tag
            or "ad.doubleclick.net" in noscript_tag
            or "www.generaltracking.de" in noscript_tag
            or "awaps.yandex.ru" in noscript_tag
            or "yabs.yandex." in noscript_tag
            or "metrics.el-mundo.net" in noscript_tag
            or "/capns_log/nojs" in noscript_tag
            or "tns-counter.ru" in noscript_tag
            or "alb.reddit.com" in noscript_tag
            or "/collect/?pid=" in noscript_tag
            or "/counter.lt?" in noscript_tag
            or "trc.taboola.com" in noscript_tag
            or "/stats.php" in noscript_tag
            or "static.getclicky.com" in noscript_tag
            or "/njs.gif" in noscript_tag
            or "api.b2c.com" in noscript_tag
            or "/1.gif" in noscript_tag)


def has_tracking_iframe(noscript_tag: str):
    return ("www.googletagmanager.com/ns.html?id=" in noscript_tag
            or "fls.doubleclick.net" in noscript_tag
            or "www.google-analytics.com/gtm/ns" in noscript_tag)


def get_single_element_purpose(el, noscript_tag: str, html_tree, page_url: str):
    if el.tag == "iframe":
        if has_tracking_iframe(noscript_tag):
            return "tracking_iframe"

        if "www.facebook.com/plugins/like.php" in noscript_tag:
            return "tracking_social_button_iframe"

        if "syndication.optimizesrv.com/ads-iframe-display.php" in noscript_tag\
            or "syndication.exosrv.com/ads-iframe-display.php" in noscript_tag\
            or "syndication.realsrv.com/ads-iframe-display.php" in noscript_tag:
            return "ads_iframe"

        if "www.youtube.com/embed/" in noscript_tag\
            or "fast.wistia.net/embed/iframe" in noscript_tag:
            return "embedded_video_iframe"

        return "other_iframe"

    if el.tag in ["img", "picture", "source"]:
        if has_tracking_pixel(noscript_tag):
            return "tracking_pixel"

        try:
            loading_attr = el.attrib["loading"]
            if loading_attr == "lazy":
                return "image_lazy_fallback"
        except KeyError:
            pass

        return "image_fallback"

    if el.tag in ["span"]:
        if len(el.getchildren()) == 1:
            sub_el = el.getchildren()[0]

            if sub_el.tag == "img":
                return "image_fallback"

    if el.tag in ["div", "p"]:
        if len(el.getchildren()) == 1:
            sub_el = el.getchildren()[0]

            if sub_el.tag == "img":
                if has_tracking_pixel(noscript_tag):
                    return "tracking_pixel"

                return "image_fallback"

            if sub_el.tag == "iframe"\
                and has_tracking_iframe(noscript_tag):
                return "tracking_iframe"

            if sub_el.tag == "a":
                return "anchor"

    if el.tag in ["p", "h1", "h2", "h3", "h4", "h5", "h6"]\
        and len(el.getchildren()) == 0:
            return "text_only"

    if el.tag == "link":
        try:
            rel = el.attrib["rel"]
            href = el.attrib["href"]
        except KeyError:
            return "other_link"

        if rel != "stylesheet":
            return "other_link"

        if re.search(r"\bnoscript[.-_/]", href, re.IGNORECASE):
            return "noscript_custom_stylesheet"

        def stylesheet_requires_js(link_stylesheet_el):
            try:
                if link_stylesheet_el.attrib["href"] != href:
                    return False

                if (link_stylesheet_el.attrib["rel"] == "preload"
                    and link_stylesheet_el.attrib["onload"]):
                    return True

                return bool(link_stylesheet_el.attrib["media"] in ["none", "only x"]
                        or (link_stylesheet_el.attrib["media"] == "print"
                            and link_stylesheet_el.attrib["onload"]))
            except KeyError:
                return False

        link_stylesheet_els\
            = CSSSelector('link[rel="stylesheet"], link[rel="preload"][as="style"]')(html_tree)

        # Check whether the same stylesheet is also loaded when JavaScript is
        # enabled
        if any(map(stylesheet_requires_js, link_stylesheet_els)):
            return "async_fallback_stylesheet"

        return "other_stylesheet"

    if el.tag == "style":
        return "style_only"

    if el.tag == "video":
        return "video_tag"

    if el.tag == "a":
        return "anchor"

    if el.tag == "meta":
        return "meta"

    if isinstance(el, html.HtmlComment):
        return "html_comment"

    return "other"


def get_noscript_type(
    noscript_el,
    noscript_tag: str,
    html_tree,
    page_url: str,
):
    # https://github.com/vercel/next.js/blob/canary/packages/next/pages/_document.tsx#L585
    if "data-n-css" in noscript_el.attrib:
        return "nextjs_css"

    if has_noscript_warning(noscript_el):
        return "noscript_warning"

    if len(noscript_el.getchildren()) == 0:
        if re.fullmatch(r"^\s*$", noscript_el.text_content()):
            return "empty_tag"

        return "text_only"

    if len(noscript_el.getchildren()) == 2:
        first_child = noscript_el.getchildren()[0]
        second_child = noscript_el.getchildren()[1]

        if first_child.tag == "img"\
            and second_child.tag == "img":
            if has_tracking_pixel(
                    html.tostring(first_child).decode(),
                )\
            and has_tracking_pixel(
                    html.tostring(second_child).decode(),
                ):
                return "tracking_pixel"

            return "image_fallback"

        if first_child.tag == "a"\
            and second_child.tag == "a":
            return "anchor"

        if second_child.tag == "noscript":
            return get_single_element_purpose(
                first_child,
                noscript_tag,
                html_tree,
                page_url,
            )

    if len(noscript_el.getchildren()) == 3:
        if isinstance(noscript_el.getchildren()[0], html.HtmlComment)\
            and isinstance(noscript_el.getchildren()[2], html.HtmlComment):
                if re.search(" IE ", noscript_el.getchildren()[2].text):
                    return "ie_fallback"

    if len(noscript_el.getchildren()) > 1:
        purposes = list(set(map(
            lambda el: get_single_element_purpose(el, noscript_tag, html_tree, page_url),
            noscript_el.getchildren(),
        )))

        if len(purposes) == 1:
            return purposes[0]

        if sorted(purposes) == ["anchor", "text_only"]:
            return "anchor"

        return "other_multiple_children"

    single_child = noscript_el.getchildren()[0]
    return get_single_element_purpose(
        single_child,
        noscript_tag,
        html_tree,
        page_url,
    )


CSV_HEADERS = "\
page_url,\
is_landing_page,\
noscript_el_count,\
noscript_type,\
noscript_display_name,\
noscript_tag\n"


def classify_noscript_tags(
    html_list_filepath: pathlib.Path,
    csv_path: pathlib.Path,
    tex_path: pathlib.Path,
):
    with open(csv_path, "w") as csv_file:
        csv_file.write(CSV_HEADERS)

        with open(html_list_filepath) as html_list_file:
            for html_filepath_str in html_list_file:
                html_filepath\
                    = pathlib.Path(html_filepath_str.removesuffix("\n"))
                html_tree = html.parse(str(html_filepath))

                noscript_sel = CSSSelector("noscript")

                page_url = crawl_utils\
                    .url_from_html_filename(str(html_filepath.name))
                landing_page = crawl_utils.is_landing_page(page_url)

                noscript_els = noscript_sel(html_tree)

                for noscript_el in noscript_els:
                    noscript_tag = html.tostring(noscript_el)\
                        .decode()\
                        .replace("\n", "")\
                        .replace("\r", "")\
                        .replace("\t", "")\
                        .replace("  ", "")
                    noscript_type = get_noscript_type(
                        noscript_el,
                        noscript_tag,
                        html_tree,
                        page_url,
                    )

                    write_csv_line(
                        csv_file,
                        page_url,
                        landing_page,
                        len(noscript_els),
                        noscript_type,
                        NOSCRIPT_DISPLAY_NAMES[noscript_type],
                        noscript_tag,
                    )

    df = pd.read_csv(csv_path, escapechar="\\")

    page_count = df["page_url"]\
        .drop_duplicates()\
        .count()

    df_table = df["noscript_display_name"]\
        .value_counts()\
        .reset_index(name="\\makecell[l]{Total\\\\element\\\\count}")\
        .rename(columns={"index": "noscript_display_name"})

    col_name = "\\makecell[l]{Share of pages hav-\\\\ing \
\\htmltag{noscript}\\\\tags that feature\\\\at least one\\\\element of\\\\the \
type (\\%)}"

    df_table = (100 * df[["page_url", "noscript_display_name"]]
                .drop_duplicates()
                .groupby("noscript_display_name")
                .count()
                / page_count)["page_url"]\
        .reset_index()\
        .merge(
            df_table,
            on="noscript_display_name",
        )\
        .rename(columns={"page_url": col_name})\
        .sort_values(by=col_name, ascending=False)

    df_table\
        .rename(columns={"noscript_display_name": "\\htmltag{noscript} purpose"})\
        .to_latex(
            tex_path,
            index=False,
            escape=False,
            float_format="{:0.1f}".format,
        )

    df_corr = df[["page_url", "noscript_display_name"]]\
        .drop_duplicates()\
        .pivot(
            index="page_url",
            values="noscript_display_name",
            columns="noscript_display_name",
        )\
        .notna()\
        .corr()

    fig_corr, ax_corr = plt.subplots(1, 1, figsize=(13, 13))

    sns.heatmap(
        df_corr,
        # center=0,
        square=True,
        ax=ax_corr,
    )

    ax_corr.set_yticklabels(ax_corr.get_yticklabels(), ha="right", rotation=30)
    ax_corr.set_xticklabels(ax_corr.get_xticklabels(), ha="right", rotation=30)
    ax_corr.set_ylabel("")
    ax_corr.set_xlabel("")

    make_axes_area_auto_adjustable(ax_corr)

    fig_corr.savefig(
        "corr.pdf",
        bbox_inches="tight",
    )

    # IPython.embed()

if __name__ == "__main__":
    parser = argparse.ArgumentParser()
    parser.add_argument("html_list_filepath", type=pathlib.Path,
                        metavar="INPUT", help="input HTML file")
    parser.add_argument("--csv", type=pathlib.Path, required=True,
                        dest="csv_path", help="output CSV file")
    parser.add_argument("--tex", type=pathlib.Path, required=True,
                        metavar="OUTPUT", dest="tex_path",
                        help="output TeX file")
    args = parser.parse_args()

    classify_noscript_tags(
        args.html_list_filepath,
        args.csv_path,
        args.tex_path,
    )
