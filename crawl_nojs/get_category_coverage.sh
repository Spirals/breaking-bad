#!/usr/bin/env zsh

set -o errexit
set -o nounset

features="${1:?Specify a features file}"

echo "Category coverage: $((100. \
  * $(awk -F, '{ print $1, $7 }' "${features}" \
  | grep -v ' $' \
  | sed 's/^https\?:\/\///' \
  | grep -o '^[^/]\+' \
  | sort \
  | uniq \
  | wc -l) \
  / $(awk -F, '{ print $1, $7 }' "${features}" \
  | sed 's/^https\?:\/\///' \
  | grep -o '^[^/]\+' \
  | sort \
  | uniq \
  | wc -l))) %"
