#!/usr/bin/env python3

import argparse
import pathlib

import pandas as pd
import seaborn as sns
import matplotlib.pyplot as plt


def plot(csv_path: pathlib.Path, output_path: pathlib.Path, show_gui: bool):
    sns.set_theme(style="ticks", color_codes=True)

    data_frame = pd.read_csv(csv_path)

    df = data_frame.melt(id_vars=["list_url"],
                         value_vars=["load_time_plain", "load_time_nojs"],
                         var_name="js_enabled", value_name="load_time")
    df = df.dropna()
    df["load_time"] = df["load_time"] / 1000

    grid = sns.catplot(data=df,
                       x="js_enabled",
                       y="load_time",
                       kind="boxen",
                       aspect=0.7)

    grid.set(yscale="log", yticks=[0.1, 1, 10, 100])
    grid.ax.set_ylabel("Page load time (s)")
    grid.ax.set_xlabel("")
    grid.set_xticklabels(["JS enabled", "JS disabled"])
    grid.ax.grid(linestyle=":")

    if output_path:
        grid.savefig(output_path, dpi=300)

    if show_gui:
        plt.show()


if __name__ == "__main__":
    parser = argparse.ArgumentParser()
    parser.add_argument("csv_file", type=pathlib.Path,  metavar="INPUT",
                        help="input CSV file")
    parser.add_argument("-o", type=pathlib.Path, metavar="OUTPUT",
                        dest="output_path", help="output figure, can be pdf")
    parser.add_argument("--gui", action=argparse.BooleanOptionalAction,
                        default=True, help="toggle the GUI")
    args = parser.parse_args()

    plot(args.csv_file, args.output_path, args.gui)
