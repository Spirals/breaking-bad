#!/usr/bin/env sh

set -o errexit
set -o nounset

file="${1:?Specify a file}"
selector="${2:?Specify a selector}"

if [ "$(pup "${selector}" --number -f "${file}")" -gt 0 ]; then
  echo "1"
else
  echo "0"
fi
