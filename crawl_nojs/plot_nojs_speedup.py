#!/usr/bin/env python3

import argparse
import pathlib

import pandas as pd
import seaborn as sns
import matplotlib.pyplot as plt
from matplotlib.ticker import MultipleLocator

def plot(csv_path: pathlib.Path, output_path: pathlib.Path, show_gui: bool):
    sns.set_theme(style="ticks")

    data_frame = pd.read_csv(csv_path)

    print(f'Median load time [plain]: {data_frame["load_time_plain"].median()} ms.')
    print(f'Median load time [nojs]: {data_frame["load_time_nojs"].median()} ms.')

        = 100 * (data_frame["load_time_plain"] - data_frame["load_time_nojs"])\
        / data_frame["load_time_plain"]

    data_frame["nojs_dom_load_speedup_perc"]\
        = 100 * (data_frame["dom_load_time_plain"] - data_frame["dom_load_time_nojs"])\
        / data_frame["dom_load_time_plain"]
    data_frame = data_frame.melt(
        value_vars=["nojs_load_speedup_perc", "nojs_dom_load_speedup_perc"],
        var_name="event_type",
        value_name="speedup_perc",
    )

    grid = sns.catplot(data=data_frame,
                       x="speedup_perc",
                       y="event_type",
                       kind="box",
                       showfliers=False,
                       height=2,
                       aspect=3.3)

    grid.set_yticklabels(["DOMContentLoaded", "load"])
    grid.ax.set_ylabel("")
    grid.ax.set_xlabel("Page load speedup with JS disabled (%)")

    grid.ax.xaxis.set_major_locator(MultipleLocator(20))
    grid.ax.xaxis.set_minor_locator(MultipleLocator(10))
    grid.ax.grid(True, "minor", color="lightgray", linewidth=0.2)

    if output_path:
        grid.savefig(output_path, dpi=300)

    if show_gui:
        plt.show()


if __name__ == "__main__":
    parser = argparse.ArgumentParser()
    parser.add_argument("csv_file", type=pathlib.Path,  metavar="INPUT",
                        help="input CSV file")
    parser.add_argument("-o", type=pathlib.Path, metavar="OUTPUT",
                        dest="output_path", help="output figure, can be pdf")
    parser.add_argument("--gui", action=argparse.BooleanOptionalAction,
                        default=True, help="toggle the GUI")
    args = parser.parse_args()

    plot(args.csv_file, args.output_path, args.gui)
