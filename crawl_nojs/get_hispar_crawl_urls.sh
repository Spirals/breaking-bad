#!/usr/bin/env sh

set -o errexit
set -o nounset

file="${1:?Specify a Hispar list file}"
n="${2:?Specify the number of URLs to keep from the same Alexa URL}"

sort -n -k 1,1 -k 2,2 < "${file}" \
  | awk -v n="${n}" -f first_n_with_same_key.awk \
  | awk '{ print $3 }'
