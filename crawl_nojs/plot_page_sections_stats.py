#!/usr/bin/env python3

import argparse
import pathlib

import pandas as pd
import seaborn as sns
import matplotlib.pyplot as plt

from mpl_toolkits.axes_grid1 import make_axes_locatable
from mpl_toolkits.axes_grid1.axes_divider import make_axes_area_auto_adjustable

import IPython


def plot(
    csv_path: pathlib.Path,
    output_path: pathlib.Path,
):
    df = pd.read_csv(csv_path)
    print(df["count_total"])

    df["Ratio (\%)"] = 100 * df["count_found"] / df["count_total"]

    df = df.drop(columns=["count_total"])

    df["selector"] = df["selector"].str.replace("#", "\\#")
    df["selector"] = "\\texttt{" + df["selector"] + "}"

    df = df.rename(columns={
        "selector": "Selector",
        "count_found": "Page count found",
    })

    with pd.option_context('display.max_colwidth', None):
        df.to_latex(
            output_path,
            index=False,
            float_format="{:0.1f}".format,
            escape=False,
        )

    # IPython.embed()


if __name__ == "__main__":
    parser = argparse.ArgumentParser()
    parser.add_argument("csv_file", type=pathlib.Path,  metavar="INPUT",
                        help="input CSV file")
    parser.add_argument("-o", type=pathlib.Path, metavar="OUTPUT", required=True,
                        dest="output_path", help="output LaTeX table")
    args = parser.parse_args()

    plot(args.csv_file, args.output_path)
