#!/usr/bin/env sh

set -o errexit
set -o nounset

dest_tar="crawl_$(date +%s).tar.gz"

tar -czpvf "${dest_tar}" \
  run_crawl.sh \
  index.js package.json package-lock.json how-broken/manifest.json how-broken/src

echo "Created ${dest_tar}"
