#!/usr/bin/env sh

set -o errexit
set -o nounset

echo 'selector,count_found,count_total'

script_dir=$(dirname "$0")

for sel in 'main' \
  'main, #main, .main' \
  'main, header, footer' \
  'main, header, footer, aside, nav, section, article' \
  'main, #main, .main, header, #header, .header, footer, #footer, .footer'; do
  printf '"%s",' "${sel}"

  find . -type f -path '*/*/crawl_crawl_urls_*/nojs/*.html' -print0 \
    | xargs -P0 -0 -I{} "${script_dir}/html_has_selector.sh" {} "${sel}" \
    | sort \
    | uniq -c \
    | sort -nr -k 2,2 \
    | awk '{ print $1 }' \
    | tr '\n' ' ' \
    | awk '{ print $1 "," ($1+$2) }'
done
