#!/usr/bin/env sh

# set -o errexit
set -o nounset

containers="${1?:List of container names or ids}"

for container in ${containers}; do
  crawl_dir="$(./get_crawl_dir.sh "${container}")"

  find "${crawl_dir}" -name 'crawl_urls.txt' \
      2>&1 | grep -v 'Permission denied'

  find "${crawl_dir}" -path '*/crawl_crawl_urls_*/*.log' \
      2>&1 | grep -v 'Permission denied'
  find "${crawl_dir}" -path '*/crawl_crawl_urls_*/*.error' \
      2>&1 | grep -v 'Permission denied'

  find "${crawl_dir}" -path '*/crawl_crawl_urls_*/plain/featureList_*.json' \
      2>&1 | grep -v 'Permission denied'
  find "${crawl_dir}" -path '*/crawl_crawl_urls_*/plain/*.html' \
      2>&1 | grep -v 'Permission denied'

  find "${crawl_dir}" -path '*/crawl_crawl_urls_*/nojs/featureList_*.json' \
      2>&1 | grep -v 'Permission denied'
  find "${crawl_dir}" -path '*/crawl_crawl_urls_*/nojs/*.html' \
      2>&1 | grep -v 'Permission denied'

  find "${crawl_dir}" -path '*/crawl_crawl_urls_*/adblocker/featureList_*.json' \
      2>&1 | grep -v 'Permission denied'
  find "${crawl_dir}" -path '*/crawl_crawl_urls_*/adblocker/*.html' \
      2>&1 | grep -v 'Permission denied'
done
