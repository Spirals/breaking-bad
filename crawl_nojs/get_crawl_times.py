#!/usr/bin/env python3

import argparse
import pathlib
from datetime import datetime


def get_line_crawl_type(fields: list[str]) -> str:
    if fields[1] == "[plain]":
        return "plain"

    if fields[1] == "[adblocker]":
        return "adblocker"

    return "nojs"


def escape_csv_string(string) -> str:
    return f'"{string}"' if string.find(',') > -1 else string


def get_field_or(url_data, crawl_type: str, field: str):
    value = url_data[crawl_type][field]
    if not value:
        return ""

    value_str = str(value)
    return escape_csv_string(value_str)


def get_record_fields(url_data, current_list_url: str, crawl_type: str):
    return ",".join([
        get_field_or(url_data, crawl_type, "actual_url"),
        get_field_or(url_data, crawl_type, "dom_load_time"),
        get_field_or(url_data, crawl_type, "load_time"),
        get_field_or(url_data, crawl_type, "idle_time"),
        get_field_or(url_data, crawl_type, "inspection_time"),
        get_field_or(url_data, crawl_type, "highlight_time"),
    ])


def write_record(csv_file, url_data, current_list_url: str):
    csv_record = ",".join([
        escape_csv_string(current_list_url),
        get_record_fields(url_data, current_list_url, "plain"),
        get_record_fields(url_data, current_list_url, "nojs"),
        get_record_fields(url_data, current_list_url, "adblocker"),
    ])
    csv_file.write(csv_record + "\n")


def parse_js_isodatetime(iso_date):
    return datetime.fromisoformat(iso_date.removesuffix('Z'))


def get_new_url_data():
    return {
        "plain": {
            "url_list": "",
            "actual_url": "",
            "dom_load_time": None,
            "load_time": None,
            "idle_time": None,
            "inspection_time": None,
            "highlight_cnt": 0,
            "highlight_time": None,
            },
        "nojs": {
            "url_list": "",
            "actual_url": "",
            "dom_load_time": None,
            "load_time": None,
            "idle_time": None,
            "inspection_time": None,
            "highlight_cnt": 0,
            "highlight_time": None,
            },
        "adblocker": {
            "url_list": "",
            "actual_url": "",
            "dom_load_time": None,
            "load_time": None,
            "idle_time": None,
            "inspection_time": None,
            "highlight_cnt": 0,
            "highlight_time": None,
            },
        }


def write_data(log_file, csv_file):
    url_data = get_new_url_data()

    current_list_url = ""

    for line in log_file:
        fields = line.split(' ')
        crawl_type = get_line_crawl_type(fields)

        if fields[2] == "Starting":
            list_url = fields[3].removesuffix("\n")

            if current_list_url not in ("", list_url):
                write_record(csv_file, url_data, current_list_url)
                url_data = get_new_url_data()

            current_list_url = list_url
            url_data[crawl_type]["list_url"] = list_url

        if "Loaded DOM" in line:
            url_data[crawl_type]["actual_url"] = fields[4]
            url_data[crawl_type]["dom_load_time"]\
                = int(fields[-1].removesuffix(' ms.\n'))

        if "Loaded page" in line:
            url_data[crawl_type]["actual_url"] = fields[4]
            url_data[crawl_type]["load_time"]\
                = int(fields[-1].removesuffix(' ms.\n'))

        if "Waited for" in line:
            url_data[crawl_type]["idle_time"]\
                = 1000 * int(fields[-1].removesuffix(' s.\n'))

        if "Found feature list selector" in line:
            url_data[crawl_type]["inspection_time"]\
                = int(fields[-1].removesuffix(' ms.\n'))

        if "Found highlight done selector" in line:
            if url_data[crawl_type]["highlight_time"] is None:
                url_data[crawl_type]["highlight_time"] = 0

            url_data[crawl_type]["highlight_time"]\
                += int(fields[-1].removesuffix(' ms.\n'))

    # Write last record
    write_record(csv_file, url_data, current_list_url)


if __name__ == "__main__":
    parser = argparse.ArgumentParser()
    parser.add_argument("log_files", type=pathlib.Path, nargs="+",
                        metavar="INPUT", help="input log file")
    parser.add_argument("-o", type=pathlib.Path, required=True,
                        metavar="OUTPUT", dest="csv_path",
                        help="output CSV file")
    args = parser.parse_args()

    CSV_HEADERS = "\
list_url,\
actual_url_plain,\
dom_load_time_plain,\
load_time_plain,\
idle_time_plain,\
inspection_time_plain,\
highlight_time_plain,\
actual_url_nojs,\
dom_load_time_nojs,\
load_time_nojs,\
idle_time_nojs,\
inspection_time_nojs,\
highlight_time_nojs,\
actual_url_adblocker,\
dom_load_time_adblocker,\
load_time_adblocker,\
idle_time_adblocker,\
inspection_time_adblocker,\
highlight_time_adblocker\n"

    with open(args.csv_path, 'w') as csv_file:
        csv_file.write(CSV_HEADERS)

        for log_file in args.log_files:
            with open(log_file) as log_file:
                write_data(log_file, csv_file)
