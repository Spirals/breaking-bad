#!/usr/bin/env python3

import argparse
import pathlib

import pandas as pd
import seaborn as sns
import matplotlib.pyplot as plt

def plot(
    csv_path: pathlib.Path,
    output_path: pathlib.Path,
    show_gui: bool,
):
    sns.set_theme(style="ticks", color_codes=True)

    data_frame = pd.read_csv(csv_path)
    page_count = data_frame.shape[0]

    df = pd.DataFrame(
        {
            "Load time": [
                data_frame["load_time_plain"].sum(),
                data_frame["load_time_nojs"].sum(),
                data_frame["load_time_adblocker"].sum(),
            ],
            "Idle time": [
                data_frame["idle_time_plain"].sum(),
                data_frame["idle_time_nojs"].sum(),
                data_frame["idle_time_adblocker"].sum(),
            ],
            "Inspection time": [
                data_frame["inspection_time_plain"].sum(),
                data_frame["inspection_time_nojs"].sum(),
                data_frame["inspection_time_adblocker"].sum(),
            ],
            "Highlighting time": [
                data_frame["highlight_time_plain"].sum(),
                data_frame["highlight_time_nojs"].sum(),
                data_frame["highlight_time_adblocker"].sum(),
            ],
        },
        index=["plain", "nojs", "adblocker"],
    )
    df["Load time"] = df["Load time"] / 1000 / 3600
    df["Idle time"] = df["Idle time"] / 1000 / 3600
    df["Inspection time"] = df["Inspection time"] / 1000 / 3600
    df["Highlighting time"] = df["Highlighting time"] / 1000 / 3600

    ax = df.plot(
        kind="bar",
        ylabel="Time (hour)",
        # yticks=[0, 1, 2, 3, 4, 5, 6, 7],
        title=f"Crawl on {page_count} pages",
        grid=True,
        stacked=True,
        rot=0,
    )

    ax.set_xticklabels(["JS enabled", "JS disabled", "JS enabled with adblocker"])
    ax.grid(axis="x")

    if output_path:
        plt.gcf().savefig(output_path, dpi=300)

    if show_gui:
        plt.show()


if __name__ == "__main__":
    parser = argparse.ArgumentParser()
    parser.add_argument("csv_file", type=pathlib.Path,  metavar="INPUT",
                        help="input CSV file")
    parser.add_argument("-o", type=pathlib.Path, metavar="OUTPUT",
                        dest="output_path", help="output figure, can be pdf")
    parser.add_argument("--gui", action=argparse.BooleanOptionalAction,
                        default=True, help="toggle the GUI")
    args = parser.parse_args()

    plot(args.csv_file, args.output_path, args.gui)
