# Crawl setup

1. `@server$ mkdir crawl1`
2. `@desktop$ ./tar_crawl_files.sh`
3. `@desktop$ sftp`
    - `cd crawl1`
    - `put`
      - `crawl_….tar.gz`
      - `crawl_urls.txt`
      - `Dockerfile`
      - `web-ext_support_multiple_source_dirs.patch`
4. `@server$ docker build --build-arg crawltar=crawl_….tar.gz --build-arg otherexturl=https://github.com/gorhill/uBlock/releases/download/1.33.2/uBlock0_1.33.2.firefox.xpi -t "crawl:crawl" .`

For each container to run:

1. `@server$ docker create --name crawl0 $image_id`
2. `@server$ docker cp crawl_urls.txt "$(docker ps -lq)":/home/crawl/crawl_urls.txt`
3. `@server$ docker start "$(docker ps -lq)"`

To attach to a running container: `docker exec -it $container_name /bin/sh`

To access the logs (real-time or after the container has been stopped): `docker logs -f "$(docker ps -lq)"`

Extracting crawl output:

1. `@server$ docker kill $container_name`
2. `@server$ docker cp $container_name:/home/crawl/$crawl_dir .`
3. `@server$ tar -czpvf ${crawl_dir}_output.tar.gz $crawl_dir`
4. `@desktop$ sftp`
    - `get ${crawl_dir}_output.tar.gz`
