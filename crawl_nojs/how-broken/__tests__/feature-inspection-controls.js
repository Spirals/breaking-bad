"use strict";

const fs = require("fs");

const TEST_PAGES_BASE_DIR = "__tests__/";

beforeAll((done) => {
  fs.readFile(
    `${TEST_PAGES_BASE_DIR}test-controls-page.html`,
    "utf8",
    (err, data) => {
      if (err) {
        console.error(err);
      }

      document.write(data);
      done();
    },
  );
});

test("findLoneControls", async () => {
  require("../src/feature-data");
  require("../src/cache-store");
  require("../src/utils");
  require("../src/dom-utils");
  require("../src/feature-inspection-common");

  Object.defineProperty(window.HB.common, "hasEventHandlerType", {
    writable: true,
    value: jest.fn((element, eventType) => {
      if (eventType) {
        return element.classList.contains(`event-${eventType}`);
      }

      return false;
    }),
  });

  require("../src/feature-inspection-controls");

  expect(await window.HB.heuristics.findLoneControls({})).toHaveLength(18);
  expect(await window.HB.heuristics.findLoneControls({ useful: true }))
    .toHaveLength(17);
  expect(await window.HB.heuristics.findLoneControls({ essential: true }))
    .toHaveLength(2);
  expect(await window.HB.heuristics.findLoneControls({ broken: true }))
    .toHaveLength(5);
});

test("findGoToTopAnchorButtons", () => {
  require("../src/dom-utils");
  require("../src/feature-inspection-controls");

  expect(window.HB.heuristics.findGoToTopAnchorButtons({}))
    .toHaveLength(3);

  expect(window.HB.heuristics.findGoToTopAnchorButtons({ useful: true }))
    .toStrictEqual([]);
  expect(window.HB.heuristics.findGoToTopAnchorButtons({ essential: true }))
    .toStrictEqual([]);
});

test("findEmptyAnchorButtons", async () => {
  require("../src/utils");
  require("../src/dom-utils");
  require("../src/feature-inspection-common");

  Object.defineProperty(window.HB.common, "hasEventHandlerType", {
    writable: true,
    value: jest.fn((element, eventType) => {
      if (eventType) {
        return element.classList.contains(`event-${eventType}`);
      }

      return false;
    }),
  });

  require("../src/feature-inspection-controls");

  const EMPTY_ANCHOR_COUNT = 9;
  expect(await window.HB.heuristics.findEmptyAnchorButtons({}))
    .toHaveLength(EMPTY_ANCHOR_COUNT);
  expect(await window.HB.heuristics.findEmptyAnchorButtons({ useful: true }))
    .toHaveLength(EMPTY_ANCHOR_COUNT);
  expect(await window.HB.heuristics.findEmptyAnchorButtons({ essential: true }))
    .toHaveLength(2);

  expect(await window.HB.heuristics.findEmptyAnchorButtons({ broken: true }))
    .toHaveLength(7);
});

test("findLanguageSelectInputs", async () => {
  require("../src/utils");
  require("../src/dom-utils");
  require("../src/feature-inspection-common");

  Object.defineProperty(window.HB.common, "hasEventHandlerType", {
    writable: true,
    value: jest.fn((element, eventType) => {
      if (eventType) {
        return element.classList.contains(`event-${eventType}`);
      }

      return false;
    }),
  });

  require("../src/feature-inspection-forms");
  require("../src/feature-inspection-controls");

  expect(await window.HB.heuristics.findLanguageSelectInputs({}))
    .toHaveLength(3);
  expect(await window.HB.heuristics
    .findLanguageSelectInputs({ essential: true }))
    .toHaveLength(0);
  expect(await window.HB.heuristics.findLanguageSelectInputs({ broken: true }))
    .toHaveLength(1);
});

test("findPageDelegatedClickEvents", async () => {
  require("../src/utils");
  require("../src/dom-utils");
  require("../src/feature-inspection-common");

  Object.defineProperty(window.HB.common, "hasEventHandlerType", {
    writable: true,
    value: jest.fn((element, eventType) => {
      if (eventType) {
        return element.classList.contains(`event-${eventType}`);
      }

      return false;
    }),
  });

  require("../src/feature-inspection-controls");

  expect(await window.HB.heuristics.findPageDelegatedClickEvents({}))
    .toHaveLength(1);

  expect(await window.HB.heuristics
    .findPageDelegatedClickEvents({ broken: true }))
    .toHaveLength(0);
});

test("findMislinkedFragmentAnchors", async () => {
  require("../src/utils");
  require("../src/dom-utils");
  require("../src/feature-inspection-common");

  Object.defineProperty(window.HB.common, "hasEventHandlerType", {
    writable: true,
    value: jest.fn((element, eventType) => {
      if (eventType) {
        return element.classList.contains(`event-${eventType}`);
      }

      return false;
    }),
  });

  require("../src/feature-inspection-controls");

  expect(await window.HB.heuristics.findMislinkedFragmentAnchors({}))
    .toHaveLength(2);
  expect(await window.HB.heuristics
    .findMislinkedFragmentAnchors({ useful: true }))
    .toHaveLength(2);
  expect(await window.HB.heuristics
    .findMislinkedFragmentAnchors({ essential: true }))
    .toHaveLength(0);

  expect(await window.HB.heuristics
    .findMislinkedFragmentAnchors({ broken: true }))
    .toHaveLength(1);
});
