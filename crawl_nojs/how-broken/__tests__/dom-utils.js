"use strict";

const fs = require("fs");

const TEST_PAGES_BASE_DIR = "__tests__/";

beforeAll((done) => {
  fs.readFile(
    `${TEST_PAGES_BASE_DIR}test-dom-utils-page.html`,
    "utf8",
    (_, data) => {
      document.write(data);
      done();
    },
  );
});

test("isTextOnlyWhitespace", () => {
  require("../src/dom-utils");

  expect(window.HB.domUtils.isTextOnlyWhitespace("")).toBe(true);
  expect(window.HB.domUtils.isTextOnlyWhitespace(" ")).toBe(true);
  expect(window.HB.domUtils.isTextOnlyWhitespace("\t")).toBe(true);
  expect(window.HB.domUtils.isTextOnlyWhitespace("\n")).toBe(true);
  expect(window.HB.domUtils.isTextOnlyWhitespace("a")).toBe(false);
});

test("isElementTargetedByPseudoClassSelector", () => {
  require("../src/dom-utils");

  const buttonElement = document.querySelector("div form button");
  expect(window.HB.domUtils.isElementTargetedByPseudoClassSelector(
    buttonElement,
    "div form button:hover",
    "hover",
  )).toBe(true);

  expect(window.HB.domUtils.isElementTargetedByPseudoClassSelector(
    buttonElement,
    "span:hover, div form button:hover, img",
    "hover",
  )).toBe(true);

  expect(window.HB.domUtils.isElementTargetedByPseudoClassSelector(
    buttonElement,
    "span:hover, div:hover form button:hover, img",
    "hover",
  )).toBe(true);

  expect(window.HB.domUtils.isElementTargetedByPseudoClassSelector(
    buttonElement,
    ":hover",
    "hover",
  )).toBe(true);

  expect(window.HB.domUtils.isElementTargetedByPseudoClassSelector(
    buttonElement,
    "div form:hover button",
    "hover",
  )).toBe(false);

  expect(window.HB.domUtils.isElementTargetedByPseudoClassSelector(
    buttonElement,
    "div form button:hover",
    "visited",
  )).toBe(false);

  expect(window.HB.domUtils.isElementTargetedByPseudoClassSelector(
    buttonElement,
    "div form button",
    "hover",
  )).toBe(false);

  expect(window.HB.domUtils.isElementTargetedByPseudoClassSelector(
    buttonElement,
    "",
    "hover",
  )).toBe(false);

  const formElement = document.querySelector("div form");
  expect(window.HB.domUtils.isElementTargetedByPseudoClassSelector(
    formElement,
    "div form:hover button",
    "hover",
  )).toBe(false);
});

test("querySelectorAllExtra", () => {
  require("../src/dom-utils");
  const querySelectorAllExtra = window.HB.domUtils.querySelectorAllExtra;

  const testButtons = querySelectorAllExtra(
    document,
    '[class_="test"] button',
  );
  expect(testButtons).toHaveLength(1);
  expect(testButtons[0].innerHTML).toBe("Test");

  const testButtons2 = querySelectorAllExtra(
    document,
    '[class_="div-class"][class_="div-class2"] button',
  );
  expect(testButtons2).toHaveLength(1);
  expect(testButtons2[0].innerHTML).toBe("Test2");

  const testButtons3 = querySelectorAllExtra(
    document,
    '[class_="div-class"] div[class_="div-class2"] button',
  );
  expect(testButtons3).toHaveLength(1);
  expect(testButtons3[0].innerHTML).toBe("Test2");

  const testWordMatching = querySelectorAllExtra(
    document,
    '[class_="burger"]',
  );
  expect(testWordMatching).toHaveLength(8);

  // Empty string, not a valid selector
  expect(querySelectorAllExtra(document, "")).toStrictEqual([]);

  // Selector that matches nothing
  expect(querySelectorAllExtra(document, "aaaa")).toStrictEqual([]);

  // Selector that matches nothing
  expect(querySelectorAllExtra(document, "aaaa[aaaa]")).toStrictEqual([]);
});

test("matchesExtra", () => {
  require("../src/dom-utils");
  const matchesExtra = window.HB.domUtils.matchesExtra;

  expect(matchesExtra(
    document.querySelectorAll("button")[2],
    '[class_="div-class2"] button',
  )).toBe(true);

  expect(matchesExtra(
    document.querySelectorAll("button")[2],
    '[class_="div-class"][class_="div-class2"] button',
  )).toBe(true);

  // FIXME: this is a known limitation (for now, the custom word attribute
  // selector must be the last attribute selector on a selector), this
  // should return true
  expect(matchesExtra(
    document.querySelectorAll("button")[2],
    '[class_="div-class"][class*="div-class2"] button',
  )).toBe(false);

  expect(matchesExtra(
    document.querySelectorAll("button")[2],
    `[class_="div-class"] span,
    [class_="div-class2"] button`,
  )).toBe(true);

  expect(matchesExtra(
    document.querySelectorAll("button")[2],
    '[class_="div-class2"] *',
  )).toBe(true);

  // TODO: jsdom seems to not yet support the CSS4 :scope pseudo-selector
  // expect(matchesExtra(
  //   document.querySelectorAll("button")[2],
  //   '[class_="div-class2"] :scope',
  // )).toBe(true);

  expect(matchesExtra(
    document.querySelectorAll("button")[2],
    '[class_="test"] button',
  )).toBe(false);
});

test("selectorsMatches", () => {
  require("../src/dom-utils");

  expect(window.HB.domUtils.selectorsMatches({
    match: "div button",
    exclude: ".div-class2 button",
  })).toHaveLength(2);
});

test("selectorsMatchesExtra", () => {
  require("../src/dom-utils");

  expect(window.HB.domUtils.selectorsMatchesExtra({
    match: 'div[class_="div-class"] button',
    exclude: '[class_="div-class2"] button',
  })).toHaveLength(1);
});

test("keepOnlyOutmostElements", () => {
  require("../src/dom-utils");

  expect(window.HB.domUtils
    .keepOnlyOutmostElements(Array.from(document
      .querySelectorAll("div[class*=div-class]"))))
    .toHaveLength(3);
});

test("keepOnlyInmostElements", () => {
  require("../src/dom-utils");

  expect(window.HB.domUtils
    .keepOnlyInmostElements(Array.from(document
      .querySelectorAll("div[class*=div-class]"))))
    .toHaveLength(3);
});

test("getActiveDocumentCSSStyleRules", async () => {
  require("../src/cache-store");
  require("../src/utils");
  require("../src/dom-utils");

  // jsdom does not seem to implement the StyleSheet interface
  StyleSheet.prototype.media = {
    mediaText: "",
  };

  // jsdom does not implement window.matchMedia
  window.matchMedia = () => ({ matches: true });

  expect(await window.HB.domUtils.getActiveDocumentCSSStyleRules())
    .toHaveLength(12);
});

test("getElementMatchingStyleRules", async () => {
  require("../src/cache-store");
  require("../src/dom-utils");

  expect(await window.HB.domUtils
    .getElementMatchingStyleRules(document.querySelector("a")))
    .toHaveLength(1);

  expect(await window.HB.domUtils
    .getElementMatchingStyleRules(document.querySelector("a"), "hover"))
    .toHaveLength(1);
});

test("getParentPseudoClassStyleRules", async () => {
  require("../src/utils");
  require("../src/dom-utils");

  const cssRules = await window.HB.domUtils.getParentPseudoClassStyleRules(
    document.querySelector(".dropdown-toggle"),
    document.querySelector(".dropdown-menu"),
    "hover",
  );
  expect(cssRules).toHaveLength(1);
});

test("getBackgroundImageURLs", () => {
  require("../src/dom-utils");

  const testImg = document.querySelector("#test-img");
  const imgStyle = getComputedStyle(testImg);
  expect(window.HB.domUtils.getBackgroundImageURLs(imgStyle))
    .toStrictEqual([new URL(`${window.location.toString()}/test.png`)]);

  // The backgroundImage can be a gradient and not come from a URL
  const testDiv = document.querySelector("#test-div");
  const divStyle = getComputedStyle(testDiv);
  expect(window.HB.domUtils.getBackgroundImageURLs(divStyle))
    .toStrictEqual([]);

  expect(window.HB.domUtils
    .getBackgroundImageURLs(getComputedStyle(document.body)))
    .toStrictEqual([]);

  // TODO: add another test with a relative URL e.g. `test.png`, this is for now
  // prevented by https://github.com/jsdom/jsdom/issues/2916
});

test("hasPointerCursor", () => {
  require("../src/dom-utils");

  const pointerCursorElements = Array.from(document.querySelectorAll("div"))
    .filter((element) => {
      return window.HB.domUtils.hasPointerCursor(element);
    });

  expect(pointerCursorElements).toHaveLength(1);
});

test("isColorInitialValue", () => {
  require("../src/dom-utils");

  const isColorInitialValue = window.HB.domUtils.isColorInitialValue;
  expect(isColorInitialValue("rgba(0, 0, 0, 0)")).toBe(true);
  expect(isColorInitialValue("transparent")).toBe(true);

  expect(isColorInitialValue("blue")).toBe(false);
  expect(isColorInitialValue("rgb(255, 255, 255)")).toBe(false);
  expect(isColorInitialValue("#fff")).toBe(false);
  // This is not a bug for now, the function only checks if the color is the
  // initial value for the color property
  expect(isColorInitialValue("rgba(255, 255, 255, 0)")).toBe(false);
});
