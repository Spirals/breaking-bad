"use strict";

const fs = require("fs");

const TEST_PAGES_BASE_DIR = "__tests__/";

beforeAll((done) => {
  fs.readFile(
    `${TEST_PAGES_BASE_DIR}test-forms-page.html`,
    "utf8",
    (err, data) => {
      if (err) {
        console.error(err);
      }

      document.write(data);
      done();
    },
  );
});

test("findForms", async () => {
  require("../src/cache-store");
  require("../src/utils");
  require("../src/dom-utils");
  require("../src/feature-inspection-common");

  Object.defineProperty(window.HB.common, "hasEventHandlerType", {
    writable: true,
    value: jest.fn((element, eventType) => {
      if (eventType) {
        return element.classList.contains(`event-${eventType}`);
      }

      return false;
    }),
  });

  require("../src/feature-inspection-forms");

  const FORM_COUNT = 14;
  expect(await window.HB.heuristics.findForms({})).toHaveLength(FORM_COUNT);

  expect(await window.HB.heuristics.findForms({ useful: true }))
    .toHaveLength(FORM_COUNT);

  expect(await window.HB.heuristics.findForms({ essential: true }))
    .toHaveLength(1);

  expect(await window.HB.heuristics.findForms({ broken: true }))
    .toHaveLength(3);
});
