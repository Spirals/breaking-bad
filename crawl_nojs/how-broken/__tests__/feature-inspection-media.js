"use strict";

const fs = require("fs");

const TEST_PAGES_BASE_DIR = "__tests__/";

beforeAll((done) => {
  fs.readFile(
    `${TEST_PAGES_BASE_DIR}test-media-page.html`,
    "utf8",
    (err, data) => {
      if (err) {
        console.error(err);
      }

      document.write(data);
      done();
    },
  );
});

test("findVideoTags", () => {
  require("../src/cache-store");
  require("../src/dom-utils");
  require("../src/feature-inspection-common");
  require("../src/feature-inspection-media");

  expect(window.HB.heuristics.findVideoTags({})).toHaveLength(2);
  expect(window.HB.heuristics.findVideoTags({ essential: true }))
    .toHaveLength(1);
  expect(window.HB.heuristics.findVideoTags({ broken: true })).toHaveLength(2);
});

test("findAudioTags", () => {
  require("../src/dom-utils");
  require("../src/feature-inspection-media");

  expect(window.HB.heuristics.findAudioTags({})).toHaveLength(1);
  expect(window.HB.heuristics.findAudioTags({ essential: true }))
    .toHaveLength(0);
  expect(window.HB.heuristics.findAudioTags({ broken: true })).toHaveLength(1);
});

test("findCanvasTags", () => {
  require("../src/dom-utils");
  require("../src/feature-inspection-media");

  expect(window.HB.heuristics.findCanvasTags({})).toHaveLength(1);
  expect(window.HB.heuristics.findCanvasTags({ essential: true }))
    .toHaveLength(0);
  expect(window.HB.heuristics.findCanvasTags({ broken: true })).toHaveLength(1);
});
