"use strict";

const fs = require("fs");

const TEST_PAGES_BASE_DIR = "__tests__/";

beforeAll((done) => {
  fs.readFile(
    `${TEST_PAGES_BASE_DIR}test-ads-tracking-page.html`,
    "utf8",
    (err, data) => {
      if (err) {
        console.error(err);
      }

      document.write(data);
      done();
    },
  );
});

test("findInPageAds", () => {
  require("../src/dom-utils");
  require("../src/feature-inspection-ads-tracking");

  const ads = window.HB.heuristics.findInPageAds({});
  expect(ads).toHaveLength(2);
  expect(ads[1].querySelector("img").getAttribute("src")).toBe("ad2.jpg");

  expect(window.HB.heuristics.findInPageAds({ useful: true }))
    .toStrictEqual([]);
  expect(window.HB.heuristics.findInPageAds({ essential: true }))
    .toStrictEqual([]);
});

test("findAnalyticsScripts", () => {
  require("../src/dom-utils");
  require("../src/feature-inspection");
  require("../src/feature-inspection-ads-tracking");

  expect(window.HB.heuristics.findAnalyticsScripts({})).toHaveLength(2);
  expect(window.HB.heuristics.findAnalyticsScripts({ useful: true }))
    .toStrictEqual([]);
  expect(window.HB.heuristics.findAnalyticsScripts({ essential: true }))
    .toStrictEqual([]);
});

test("findAnalyticsIFrames", () => {
  require("../src/dom-utils");
  require("../src/feature-inspection");
  require("../src/feature-inspection-ads-tracking");

  expect(window.HB.heuristics.findAnalyticsIFrames({})).toHaveLength(2);
  expect(window.HB.heuristics.findAnalyticsIFrames({ useful: true }))
    .toStrictEqual([]);
  expect(window.HB.heuristics.findAnalyticsIFrames({ essential: true }))
    .toStrictEqual([]);
});

test("findPingAnchors", () => {
  require("../src/feature-inspection-ads-tracking");

  expect(window.HB.heuristics.findPingAnchors({})).toHaveLength(2);
});
