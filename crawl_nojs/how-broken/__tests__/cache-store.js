"use strict";

test("CacheStore", () => {
  require("../src/cache-store");

  const cacheStore = window.HB._cacheStore;
  cacheStore.set("key0", ["value0"]);
  cacheStore.set("key1", ["value1", "value2"]);

  expect(cacheStore.has("key0")).toBe(true);
  expect(cacheStore.has("key1")).toBe(true);
  expect(cacheStore.has("key100")).toBe(false);

  expect(cacheStore.get("key1")).toStrictEqual(["value1", "value2"]);

  expect(cacheStore.getOrSet("key1", () => ["value3"]))
    .toStrictEqual(["value1", "value2"]);

  expect(cacheStore.getOrSet("key2", () => ["value3"]))
    .toStrictEqual(["value3"]);

  expect(cacheStore.invalidate("key2")).toBe(true);
  expect(cacheStore.has("key2")).toBe(false);
  expect(cacheStore.has("key0")).toBe(true);
  expect(cacheStore.has("key1")).toBe(true);

  expect(cacheStore.invalidate("key100")).toBe(false);

  cacheStore.invalidateAll();
  expect(cacheStore.has("key0")).toBe(false);
  expect(cacheStore.has("key1")).toBe(false);
});
