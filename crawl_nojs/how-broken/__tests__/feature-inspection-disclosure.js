"use strict";

const fs = require("fs");

const TEST_PAGES_BASE_DIR = "__tests__/";

beforeAll((done) => {
  fs.readFile(
    `${TEST_PAGES_BASE_DIR}test-disclosure-page.html`,
    "utf8",
    (err, data) => {
      if (err) {
        console.error(err);
      }

      document.write(data);
      done();
    },
  );
});

test("findDisclosureButtons", async () => {
  require("../src/cache-store");
  require("../src/feature-data");
  require("../src/utils");
  require("../src/dom-utils");
  require("../src/feature-inspection-common");
  require("../src/feature-inspection");

  // jsdom does not seem to implement the StyleSheet interface
  StyleSheet.prototype.media = {
    mediaText: "",
  };

  // jsdom does not implement window.matchMedia
  window.matchMedia = () => ({ matches: true });

  Object.defineProperty(window.HB.common, "hasEventHandlerType", {
    writable: true,
    value: jest.fn((element, eventType) => {
      if (eventType) {
        return element.classList.contains(`event-${eventType}`);
      }

      return false;
    }),
  });

  require("../src/feature-inspection-disclosure");

  const disclosureButtons
    = await window.HB.heuristics.findDisclosureButtons({});
  expect(disclosureButtons).toHaveLength(9);
  expect(disclosureButtons[2].id).toBe("test2");

  const brokenDisclosureButton = await window.HB.heuristics
    .findDisclosureButtons({ broken: true });
  expect(brokenDisclosureButton).toHaveLength(4);
});
