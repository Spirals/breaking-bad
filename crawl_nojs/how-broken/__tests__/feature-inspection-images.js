"use strict";

const fs = require("fs");

const TEST_PAGES_BASE_DIR = "__tests__/";

beforeAll((done) => {
  fs.readFile(
    `${TEST_PAGES_BASE_DIR}test-images-page.html`,
    "utf8",
    (err, data) => {
      if (err) {
        console.error(err);
      }

      document.write(data);
      done();
    },
  );
});

test("findImages", () => {
  require("../src/feature-data");
  require("../src/dom-utils");
  require("../src/feature-inspection-images");

  expect(window.HB.heuristics.findImages({})).toHaveLength(17);
  // FIXME: jsdom does not implement currentSrc
  expect(window.HB.heuristics.findImages({ broken: true })).toHaveLength(0);
});

test("findBackgroundImageOnlyElements", () => {
  require("../src/feature-data");
  require("../src/dom-utils");
  require("../src/feature-inspection-images");

  expect(window.HB.heuristics.findBackgroundImageOnlyElements({}))
    .toHaveLength(1);
  expect(window.HB.heuristics
    .findBackgroundImageOnlyElements({ essential: true }))
    .toHaveLength(1);
  expect(window.HB.heuristics.findBackgroundImageOnlyElements({ broken: true }))
    .toHaveLength(1);
});

test("findSVGImages", () => {
  require("../src/feature-data");
  require("../src/dom-utils");
  require("../src/feature-inspection-images");

  const svgImages = window.HB.heuristics.findSVGImages({});
  expect(svgImages).toHaveLength(1);
});

test("findAvatarImages", () => {
  require("../src/feature-data");
  require("../src/cache-store");
  require("../src/dom-utils");
  require("../src/feature-inspection-common");
  require("../src/feature-inspection-images");

  expect(window.HB.heuristics.findAvatarImages({})).toHaveLength(3);
  expect(window.HB.heuristics.findAvatarImages({ essential: true }))
    .toHaveLength(3);
  expect(window.HB.heuristics.findAvatarImages({ broken: true }))
    .toHaveLength(0);
});

test("findLargeImages", () => {
  require("../src/feature-data");
  require("../src/dom-utils");
  require("../src/feature-inspection-images");

  const largeImages = window.HB.heuristics.findLargeImages({});
  expect(largeImages).toHaveLength(1);
});

test("findTrackingPixels", () => {
  require("../src/feature-data");
  require("../src/utils");
  require("../src/dom-utils");
  require("../src/feature-inspection-images");

  const trackingPixels = window.HB.heuristics.findTrackingPixels({});
  expect(trackingPixels).toHaveLength(7);

  expect(window.HB.heuristics.findTrackingPixels({ useful: true }))
    .toStrictEqual([]);
  expect(window.HB.heuristics.findTrackingPixels({ essential: true }))
    .toStrictEqual([]);
});
