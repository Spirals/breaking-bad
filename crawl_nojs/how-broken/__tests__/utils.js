"use strict";

test("getURLHostname", () => {
  require("../src/utils");

  expect(window.HB.utils
    .getURLHostname("https://example.com:80/test?test0=0&test1=1"))
    .toBe("example.com");

  expect(window.HB.utils
    .getURLHostname("//example.com:80/test?test0=0&test1=1"))
    .toBe("example.com");

  expect(window.HB.utils.getURLHostname("")).toBe("");
});

test("getURLProtocol", () => {
  require("../src/utils");

  expect(window.HB.utils
    .getURLProtocol("https://example.com:80/test?test0=0&test1=1"))
    .toBe("https:");

  // The location during unit testing is http://localhost/
  expect(window.HB.utils
    .getURLProtocol("//example.com:80/test?test0=0&test1=1"))
    .toBe("http:");

  expect(window.HB.utils
    .getURLProtocol("data:aaaa"))
    .toBe("data:");

  expect(window.HB.utils.getURLProtocol("")).toBe("");
});

test("getURLSearch", () => {
  require("../src/utils");

  expect(window.HB.utils
    .getURLSearch("https://example.com:80/test?test0=0&test1=1"))
    .toBe("?test0=0&test1=1");

  expect(window.HB.utils
    .getURLSearch("//example.com:80/test?test0=0&test1=1"))
    .toBe("?test0=0&test1=1");

  expect(window.HB.utils.getURLSearch("")).toBe("");
});

test("getURLSearchParams", () => {
  require("../src/utils");

  expect(window.HB.utils
    .getURLSearchParams("https://example.com:80/test?test0=0&test1=1")
    .get("test0"))
    .toBe("0");

  expect(window.HB.utils
    .getURLSearchParams("//example.com:80/test?test0=0&test1=1")
    .get("test1"))
    .toBe("1");

  expect(window.HB.utils.getURLSearchParams("")).toBe(null);
});

test("haveSameOrigins", () => {
  require("../src/utils");

  expect(window.HB.utils.haveSameOrigins(
    "https://example.com/test",
    "https://example.com",
  ))
    .toBe(true);

  expect(window.HB.utils.haveSameOrigins(
    "https://example.com",
    "https://www.example.com",
  ))
    .toBe(false);

  expect(window.HB.utils.haveSameOrigins(
    "https://example.com",
    "http://example.com",
  ))
    .toBe(false);

  expect(window.HB.utils.haveSameOrigins("", "")).toBe(false);
});

test("linkToSamePage", () => {
  require("../src/utils");

  expect(window.HB.utils.linkToSamePage(
    "https://example.com/test",
    "https://example.com/test#test",
  ))
    .toBe(true);

  expect(window.HB.utils.linkToSamePage(
    "https://example.com/test",
    "https://example.com/test?test=1",
  ))
    .toBe(false);

  expect(window.HB.utils.linkToSamePage(
    "https://example.com/test",
    "https://www.example.com/test",
  ))
    .toBe(false);

  expect(window.HB.utils.linkToSamePage(
    "https://example.com/test",
    "https://example.com",
  ))
    .toBe(false);

  expect(window.HB.utils.linkToSamePage("", "")).toBe(false);
});

test("linkToSubpath", () => {
  require("../src/utils");

  expect(window.HB.utils.linkToSubpath(
    "https://example.com/",
    "https://example.com/test",
  ))
    .toBe(true);

  expect(window.HB.utils.linkToSubpath(
    "https://example.com",
    "https://example.com/test",
  ))
    .toBe(true);

  expect(window.HB.utils.linkToSubpath(
    "https://example.com/test",
    "https://example.com/test",
  ))
    .toBe(true);

  expect(window.HB.utils.linkToSubpath(
    "https://example.com/test",
    "https://example.com/test/test",
  ))
    .toBe(true);

  expect(window.HB.utils.linkToSubpath(
    "https://example.com/test/",
    "https://example.com/test",
  ))
    .toBe(false);

  expect(window.HB.utils.linkToSubpath(
    "https://example.com/test",
    "https://example.com/test2",
  ))
    .toBe(false);

  expect(window.HB.utils.linkToSubpath(
    "https://example.com/test",
    "https://example.com/",
  ))
    .toBe(false);

  expect(window.HB.utils.linkToSubpath("", "")).toBe(false);
});

test("asyncFilter", async () => {
  require("../src/utils");

  const testArray = [1, 2, 3, 4, 5, 6, 7, 8, 9];

  expect(await window.HB.utils.asyncFilter(testArray, async (item) => {
    return await item % 2 === 0;
  })).toStrictEqual([2, 4, 6, 8]);
});

test("asyncFlatMap", async () => {
  require("../src/utils");

  const testArray = [1, 2, 3, 4];

  expect(await window.HB.utils.asyncFlatMap(testArray, async (item) => {
    return await [item, item % 2 === 0];
  })).toStrictEqual([1, false, 2, true, 3, false, 4, true]);
});

test("asyncSome", async () => {
  require("../src/utils");

  const spyFunction = jest.fn(() => {
    return "spy";
  });
  const spyObj = { toString: spyFunction };

  const testArray = ["b", "a", spyObj, "c"];

  expect(await window.HB.utils.asyncSome(testArray, async (item) => {
    return await item.toString().includes("a");
  })).toBe(true);
  // Test short-circuiting
  expect(spyFunction).not.toHaveBeenCalled();

  expect(await window.HB.utils.asyncSome(testArray, async (item) => {
    return await item.toString().includes("z");
  })).toBe(false);
  expect(spyFunction).toHaveBeenCalledTimes(1);
});

test("asyncEvery", async () => {
  require("../src/utils");

  const spyFunction = jest.fn(() => {
    return "spy";
  });
  const spyObj = { toString: spyFunction };

  const testArray = ["a", "b", spyObj, "c"];

  expect(await window.HB.utils.asyncEvery(testArray, async (item) => {
    return await item.toString().includes("a");
  })).toBe(false);
  // Test short-circuiting
  expect(spyFunction).not.toHaveBeenCalled();

  expect(await window.HB.utils.asyncEvery(testArray, async (item) => {
    return await item.toString().length < 4;
  })).toBe(true);
  expect(spyFunction).toHaveBeenCalledTimes(1);
});
