"use strict";

const fs = require("fs");

const TEST_PAGES_BASE_DIR = "__tests__/";

beforeEach((done) => {
  fs.readFile(
    `${TEST_PAGES_BASE_DIR}test-simple-page.html`,
    "utf8",
    (err, data) => {
      if (err) {
        console.error(err);
      }

      document.innerHTML = "";
      document.write(data);
      done();
    },
  );
});

test("findPageBodies", () => {
  require("../src/dom-utils");
  require("../src/feature-inspection");

  expect(window.HB.heuristics.findPageBodies({})).toHaveLength(1);
  expect(window.HB.heuristics.findPageBodies({ broken: true }))
    .toStrictEqual([]);

  // Make sure the page body is empty
  document.body.innerHTML = "";
  expect(window.HB.heuristics.findPageBodies({})).toHaveLength(1);
  expect(window.HB.heuristics.findPageBodies({ broken: true })).toHaveLength(1);

  // Insert some tags, with no visible content
  document.body.innerHTML = "<article><p></p></article>";
  expect(window.HB.heuristics.findPageBodies({})).toHaveLength(1);
  expect(window.HB.heuristics.findPageBodies({ broken: true })).toHaveLength(1);
});

test("findStyleRegions", () => {
  require("../src/feature-inspection");

  document.querySelector("p").style.fontFamily = "sans-serif";
  expect(window.HB.heuristics.findStyleRegions({})).toHaveLength(1);
  expect(window.HB.heuristics.findStyleRegions({ broken: true }))
    .toStrictEqual([]);
  document.querySelector("p").style.fontFamily = "";
  document.querySelector("p").style.fontSize = "20px";
  expect(window.HB.heuristics.findStyleRegions({})).toHaveLength(1);
  expect(window.HB.heuristics.findStyleRegions({ broken: true }))
    .toStrictEqual([]);

  document.innerHTML = "<p>Test</p>";
  document.querySelector("p").style.fontFamily = "";
  document.querySelector("p").style.fontSize = "";
  expect(window.HB.heuristics.findStyleRegions({})).toHaveLength(1);
  expect(window.HB.heuristics.findStyleRegions({ broken: true }))
    .toHaveLength(1);
});

test("findCookieBanners", () => {
  require("../src/dom-utils");
  require("../src/feature-inspection");

  // FIXME: this should work, but jsdom does not compute offsetHeight
  // expect(window.HB.heuristics.findCookieBanners({})).toHaveLength(3);
  expect(window.HB.heuristics.findCookieBanners({})).toHaveLength(4);
  expect(window.HB.heuristics.findCookieBanners({ useful: true }))
    .toHaveLength(4);
});

test("findPaywalls", () => {
  require("../src/dom-utils");
  require("../src/feature-inspection");

  expect(window.HB.heuristics.findPaywalls({})).toHaveLength(2);
  expect(window.HB.heuristics.findPaywalls({ useful: true })).toHaveLength(2);
});

test("findIFrames", () => {
  require("../src/cache-store");
  require("../src/dom-utils");
  require("../src/feature-inspection-common");
  require("../src/feature-inspection");

  expect(window.HB.heuristics.findIFrames({})).toHaveLength(7);
  expect(window.HB.heuristics.findIFrames({ useful: true })).toHaveLength(3);
  expect(window.HB.heuristics.findIFrames({ essential: true }))
    .toStrictEqual([]);
  expect(window.HB.heuristics.findIFrames({ broken: true })).toHaveLength(7);
});

test("findLargeIFrames", () => {
  require("../src/cache-store");
  require("../src/dom-utils");
  require("../src/feature-inspection-common");
  require("../src/feature-inspection");

  // FIXME: jsdom does not compute offsetWidth or offsetHeight
  // expect(window.HB.heuristics.findLargeIFrames({})).toHaveLength(2);
  // expect(window.HB.heuristics.findLargeIFrames({ useful: true })).toHaveLength(2);
  // expect(window.HB.heuristics.findLargeIFrames({ essential: true }))
  //   .toStrictEqual([]);
  // expect(window.HB.heuristics.findLargeIFrames({ broken: true })).toHaveLength(5);
});

test("findVideoIFrames", () => {
  require("../src/cache-store");
  require("../src/dom-utils");
  require("../src/feature-inspection-common");
  require("../src/feature-inspection");

  expect(window.HB.heuristics.findVideoIFrames({})).toHaveLength(2);
  expect(window.HB.heuristics.findVideoIFrames({ essential: true }))
    .toStrictEqual([]);
  expect(window.HB.heuristics.findVideoIFrames({ broken: true }))
    .toHaveLength(2);
});

test("findMicrobloggingWidgets", () => {
  require("../src/feature-inspection");

  expect(window.HB.heuristics.findMicrobloggingWidgets({})).toHaveLength(3);
});

test("findNoScriptFallbacks", () => {
  require("../src/feature-inspection");

  expect(window.HB.heuristics.findNoScriptFallbacks({})).toHaveLength(4);
});

test("findNoscriptWarnings", () => {
  require("../src/feature-inspection");

  expect(window.HB.heuristics.findNoscriptWarnings({})).toHaveLength(2);
});

test("findCarousels", async () => {
  require("../src/utils");
  require("../src/feature-inspection-common");

  Object.defineProperty(window.HB.common, "hasEventHandlerType", {
    writable: true,
    value: jest.fn((element, eventType) => {
      if (eventType) {
        return element.classList.contains(`event-${eventType}`);
      }

      return false;
    }),
  });

  require("../src/feature-inspection");

  expect(await window.HB.heuristics.findCarousels({})).toHaveLength(4);
  expect(await window.HB.heuristics.findCarousels({ broken: true }))
    .toHaveLength(2);
});

test("findTabButtons", async () => {
  require("../src/utils");
  require("../src/feature-inspection");

  expect(await window.HB.heuristics.findTabButtons({ broken: true }))
    .toHaveLength(0);
});

test("findExternalCommentSections", () => {
  require("../src/utils");
  require("../src/feature-inspection");

  expect(window.HB.heuristics.findExternalCommentSections({})).toHaveLength(2);
});

test("findCAPTCHAs", () => {
  require("../src/utils");
  require("../src/feature-inspection");

  expect(window.HB.heuristics.findCAPTCHAs({})).toHaveLength(3);
});

test("findLooksButtonLike", async () => {
  require("../src/cache-store");
  require("../src/utils");
  require("../src/dom-utils");
  require("../src/feature-inspection");

  // jsdom does not seem to implement the StyleSheet interface
  StyleSheet.prototype.media = {
    mediaText: "",
  };

  // jsdom does not implement window.matchMedia
  window.matchMedia = () => ({ matches: true });

  // FIXME: jsdom does not completely support the CSSStyleDeclaration interface
  // expect(window.HB.heuristics.findLooksButtonLike({})).toHaveLength(7);
  expect(await window.HB.heuristics.findLooksButtonLike({})).toHaveLength(5);
});

test("findProtectedEmails", async () => {
  require("../src/dom-utils");
  require("../src/feature-inspection");

  expect(await window.HB.heuristics.findProtectedEmails({})).toHaveLength(2);
});

test("findFullPageApps", () => {
  require("../src/feature-data");
  require("../src/feature-inspection");

  expect(window.HB.heuristics.findFullPageApps({})).toHaveLength(1);
});

test("findCustomElements", () => {
  require("../src/feature-data");
  require("../src/feature-inspection");

  expect(window.HB.heuristics.findCustomElements({})).toHaveLength(2);
});

test("findFeatureElements", async () => {
  require("../src/feature-data");
  require("../src/feature-inspection");

  expect(await window.HB.findFeatureElements("findIFrames", {}))
    .toHaveLength(7);
});

test("getFeatureList", async () => {
  require("../src/feature-data");
  require("../src/feature-inspection");

  // Suppress console output for this test
  console.log = jest.fn();

  const featureList = await window.HB.getFeatureList();
  expect(Object.keys(featureList)).toHaveLength(49);

  Object.values(featureList).forEach(({
    totalCount,
    brokenCount,
    essentialCount,
    usefulCount,
  }) => {
    expect(totalCount).not.toBeNaN();
    expect(brokenCount).not.toBeNaN();
    expect(essentialCount).not.toBeNaN();
    expect(usefulCount).not.toBeNaN();
  });
});
