"use strict";

const fs = require("fs");

const TEST_PAGES_BASE_DIR = "__tests__/";

beforeAll((done) => {
  fs.readFile(
    `${TEST_PAGES_BASE_DIR}test-common-page.html`,
    "utf8",
    (err, data) => {
      if (err) {
        console.error(err);
      }

      document.write(data);
      done();
    },
  );
});

beforeEach(() => {
  if (window.HB && window.HB._cacheStore) {
    window.HB._cacheStore.invalidateAll();
  }
});

test("isElementTargetedAtMobile", () => {
  require("../src/cache-store");
  require("../src/feature-inspection-common");

  const elementsTargetedAtMobile = Array.from(document.querySelectorAll("*"))
    .filter((element) => window.HB.common.isElementTargetedAtMobile(element));

  expect(elementsTargetedAtMobile).toHaveLength(3);
});

test("isElementNotUseful", () => {
  require("../src/cache-store");
  require("../src/dom-utils");
  require("../src/feature-inspection-common");

  const elementsNotUseful = Array.from(document.querySelectorAll("*"))
    .filter((element) => window.HB.common.isElementNotUseful(element));

  const AD_ELEMENT_COUNT = 3;
  expect(elementsNotUseful)
    .toHaveLength(AD_ELEMENT_COUNT);
});

test("isElementInHeader", () => {
  require("../src/cache-store");
  require("../src/dom-utils");
  require("../src/feature-inspection-common");

  const elementsInHeader = Array.from(document.querySelectorAll("*"))
    .filter((element) => window.HB.common.isElementInHeader(element));

  expect(elementsInHeader).toHaveLength(10);
});

test("isElementInFooter", () => {
  require("../src/cache-store");
  require("../src/dom-utils");
  require("../src/feature-inspection-common");

  const elementsInFooter = Array.from(document.querySelectorAll("*"))
    .filter((element) => window.HB.common.isElementInFooter(element));

  expect(elementsInFooter).toHaveLength(6);
});

test("isElementInSidebar", () => {
  require("../src/cache-store");
  require("../src/dom-utils");
  require("../src/feature-inspection-common");

  const elementsInSidebar = Array.from(document.querySelectorAll("*"))
    .filter((element) => window.HB.common.isElementInSidebar(element));

  expect(elementsInSidebar).toHaveLength(7);
});

test("isElementInRelatedSection", () => {
  require("../src/cache-store");
  require("../src/dom-utils");
  require("../src/feature-inspection-common");

  const elementsInRelatedSection = Array.from(document.querySelectorAll("*"))
    .filter((element) => window.HB.common.isElementInRelatedSection(element));

  expect(elementsInRelatedSection).toHaveLength(5);
});

test("isElementInMainSection", () => {
  require("../src/cache-store");
  require("../src/dom-utils");
  require("../src/feature-inspection-common");

  const elementsInMainSection = Array.from(document.querySelectorAll("*"))
    .filter((element) => window.HB.common.isElementInMainSection(element));

  expect(elementsInMainSection).toHaveLength(9);
});

test.skip("hasEventHandlerType", async () => {
  require("../src/feature-inspection-common");

  const TARGET_CLASS_NAME = "howBroken-event-8954811753249245191-click";

  Object.defineProperty(window.HB, "_getElementEventHandlers", {
    writable: true,
    value: jest.fn((element) => {
      if (element.id === "button-addeventlistenerclick-event") {
        return [{
          addEventListenerArgs: [
            "click",
            () => {},
            {
              capture: false,
              passive: false,
            },
            null,
          ],
          addEventListenerStackTrace: [""],
          targetClassName: TARGET_CLASS_NAME,
        }];
      }

      return [];
    }),
  });

  expect(await window.HB.common
    .hasEventHandlerType(document.querySelector("#button-no-event"), "click"))
    .toBe(false);

  const buttonOnClickEvent = document.querySelector("#button-onclick-event");
  expect(await window.HB.common
    .hasEventHandlerType(buttonOnClickEvent, "click"))
    .toBe(true);
  expect(await window.HB.common
    .hasEventHandlerType(buttonOnClickEvent, "input"))
    .toBe(false);

  const buttonAddEventListenerClickEvent
    = document.querySelector("#button-addeventlistenerclick-event");
  expect(await window.HB.common
    .hasEventHandlerType(buttonAddEventListenerClickEvent, "click"))
    .toBe(true);
  expect(await window.HB.common
    .hasEventHandlerType(buttonAddEventListenerClickEvent, "input"))
    .toBe(false);
});

test("hasCaret", () => {
  const elementsWithCaret = Array.from(document.querySelectorAll("button"))
    .filter((el) => window.HB.common.hasCaret(el));

  // nwsapi does not support getComputedStyle with two arguments
  // expect(elementsWithCaret).toHaveLength(3);
  expect(elementsWithCaret).toHaveLength(1);
});

test("isSpecificallyHidden", async () => {
  require("../src/utils");
  require("../src/dom-utils");

  // jsdom does not seem to implement the StyleSheet interface
  StyleSheet.prototype.media = {
    mediaText: "",
  };

  // jsdom does not implement window.matchMedia
  window.matchMedia = () => ({ matches: true });

  const specificallyHiddenElements = await window.HB.utils.asyncFilter(
    Array.from(document.querySelectorAll("*")),
    async (el) => await window.HB.common.isSpecificallyHidden(el),
  );

  expect(specificallyHiddenElements).toHaveLength(4);
});

test("isElementTabButtonLike", async () => {
  require("../src/utils");
  require("../src/dom-utils");

  const tabButtonLikeElements = await window.HB.utils.asyncFilter(
    Array.from(document.querySelectorAll("div")),
    async (element) => await window.HB.common.isElementTabButtonLike(element),
  );
  expect(tabButtonLikeElements).toHaveLength(3);
});
