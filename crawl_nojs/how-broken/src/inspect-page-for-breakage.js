"use strict";

console.log("document_idle");

let pageInspected = false;
const inspectPage = async () => {
  pageInspected = true;

  const featureList = await window.HB.getFeatureList();
  console.log(featureList);
  browser.runtime.sendMessage({ featureList });

  // Inject the featureList into the page in JSON, so that it can easily be read
  // with puppeteer
  const JSON_ID = "howbroken-feature-list";
  let jsonScriptElement = document.getElementById(JSON_ID);
  if (!jsonScriptElement) {
    jsonScriptElement = document.createElement("script");
    jsonScriptElement.id = JSON_ID;
    jsonScriptElement.type = "application/json";
    (document.head || document.documentElement).appendChild(jsonScriptElement);
  }
  jsonScriptElement.textContent = JSON.stringify(featureList);
};

let inspectPageOnLoad = false;
let pageLoaded = false;
window.addEventListener("load", () => {
  console.log("load");
  pageLoaded = true;

  if (inspectPageOnLoad && !pageInspected) {
    inspectPage();
  }
});

const enableAutoInspectionOnPage = () => {
  console.log("enableAutoInspectionOnPage");
  inspectPageOnLoad = true;

  if (pageLoaded && pageInspected) {
    return;
  }

  inspectPage();

  window.setTimeout(() => {
    console.log("Timeout");
    if (!pageInspected) {
      inspectPage();
    }
  }, 2000);
};

browser.storage.local.get("autoInspect").then((results) => {
  if (results.autoInspect) {
    enableAutoInspectionOnPage();
  }
}, (err) => {
  console.error(err);
});

const HIGHLIGHT_WORKING_FEATURE_CLASS = "highlight-working-feature";
const HIGHLIGHT_WORKING_FEATURE_INVISIBLE_CLASS
  = "highlight-working-feature-invisible";

const HIGHLIGHT_BROKEN_FEATURE_CLASS = "highlight-broken-feature";
const HIGHLIGHT_BROKEN_FEATURE_INVISIBLE_CLASS
  = "highlight-broken-feature-invisible";

const HIGHLIGHT_INFO_FEATURE_CLASS = "highlight-info-feature";
const HIGHLIGHT_INFO_FEATURE_INVISIBLE_CLASS
  = "highlight-info-feature-invisible";

const HIGHLIGHT_BROKEN_FEATURE_LABEL_CLASS = "highlight-label-broken-feature";
const HIGHLIGHT_WORKING_FEATURE_LABEL_CLASS = "highlight-label-working-feature";
const HIGHLIGHT_INFO_FEATURE_LABEL_CLASS = "highlight-label-info-feature";

const HIGHLIGHT_OVERLAY_CLASS = "highlight-overlay";

const HIGHLIGHTING_DONE_CLASS = "howbroken-highlighting-done";

const getFirstVisibleAncestor = (element) => {
  while (element && element !== document.body) {
    if (!window.HB.common.isElementHidden(element)) {
      return element;
    }

    element = element.parentElement;
  }

  return element;
};

const isElementPositionFixed = (element) => {
  while (element && element !== document.body) {
    const position = getComputedStyle(element).position;

    if (position === "absolute"
      || position === "sticky") {
      return false;
    }

    if (position === "fixed") {
      return true;
    }

    element = element.parentElement;
  }

  return false;
};

const addOverlayLabel = (
  element,
  options,
  isEssential,
  isUseful,
  infoOnly,
  featureId,
  highlightClass,
  statusClassName,
) => {
  let highlightLabelClass = options.broken
    ? HIGHLIGHT_BROKEN_FEATURE_LABEL_CLASS
    : HIGHLIGHT_WORKING_FEATURE_LABEL_CLASS;

  if (infoOnly) {
    highlightLabelClass = HIGHLIGHT_INFO_FEATURE_LABEL_CLASS;
  }

  const labelSign = isEssential ? "*" : isUseful ? "↑" : "↓";
  const labelText = `${labelSign}${featureId}`;

  const highlightLabel = document.createElement("span");
  highlightLabel.textContent = labelText;
  highlightLabel.classList.add(highlightLabelClass);

  const overlayDiv = document.createElement("div");
  overlayDiv.classList.add(
    HIGHLIGHT_OVERLAY_CLASS,
    highlightClass,
    `${statusClassName}-overlay`,
  );

  const boundingClientRect = element.getBoundingClientRect(element);
  const positionFixed = isElementPositionFixed(element);
  const overlayStyle = {
    position: positionFixed ? "fixed" : "absolute",
    width: `${boundingClientRect.width}px`,
    height: `${boundingClientRect.height}px`,
    top: `${(positionFixed ? 0 : window.scrollY) + boundingClientRect.top}px`,
    left: `${(positionFixed ? 0 : window.scrollX) + boundingClientRect.left}px`,
  };
  Object.assign(overlayDiv.style, overlayStyle);
  overlayDiv.appendChild(highlightLabel);
  document.body.appendChild(overlayDiv);
};

const getStatusClassName = (statusText, featureName, featureId) => {
  return `howbroken-${statusText}-${featureName}-${featureId}`;
};

const setHighlightFeatures = async (featureNames, highlighted, options) => {
  const statusText = options.broken ? "broken" : "working";

  if (!highlighted) {
    featureNames.forEach((featureName) => {
      const featureId = window.HB.FEATURE_DATA[featureName].id;
      const statusClassName = getStatusClassName(
        statusText,
        featureName,
        featureId,
      );

      // Remove the overlays
      Array.from(document
        .querySelectorAll(`.${HIGHLIGHT_OVERLAY_CLASS}.${statusClassName}-overlay`))
        .forEach((overlay) => overlay.remove());
    });

    return;
  }

  // Remove highlighting before adding it
  setHighlightFeatures(featureNames, !highlighted, options);

  const elements = await window.HB.utils.asyncFlatMap(
    featureNames,
    async (featureName) => (await window.HB.findFeatureElements(
      featureName,
      options,
    ))
      .map((element) => ({ featureName, element })),
  );

  options.essential = true;
  const essentialElements = await window.HB.utils.asyncFlatMap(
    featureNames,
    async (featureName) => (await window.HB.findFeatureElements(
      featureName,
      options,
    ))
      .map((element) => ({ featureName, element })),
  );

  options.essential = false;
  options.useful = true;
  const usefulElements = await window.HB.utils.asyncFlatMap(
    featureNames,
    async (featureName) => (await window.HB.findFeatureElements(
      featureName,
      options,
    ))
      .map((element) => ({ featureName, element })),
  );

  console.log(featureNames, elements, essentialElements, usefulElements);

  if (highlighted) {
    browser.runtime.sendMessage({ insertCSS: true });
  }

  (await elements).forEach(({ featureName, element }) => {
    const featureId = window.HB.FEATURE_DATA[featureName].id;
    const statusClassName = getStatusClassName(
      statusText,
      featureName,
      featureId,
    );

    const isEssential = essentialElements
      .map(({ element: el }) => el)
      .includes(element);
    const isUseful = usefulElements
      .map(({ element: el }) => el)
      .includes(element);

    element.classList.add(statusClassName);

    const infoOnly = window.HB.FEATURE_DATA[featureName].infoOnly;

    // If the element is currently invisible, highlight the first visible
    // ancestor as well, with a different style
    if (window.HB.common.isElementHidden(element)) {
      const visibleAncestor = getFirstVisibleAncestor(element);

      let invisibleHighlightClass = options.broken
        ? HIGHLIGHT_BROKEN_FEATURE_INVISIBLE_CLASS
        : HIGHLIGHT_WORKING_FEATURE_INVISIBLE_CLASS;

      if (infoOnly) {
        invisibleHighlightClass = HIGHLIGHT_INFO_FEATURE_INVISIBLE_CLASS;
      }

      addOverlayLabel(
        visibleAncestor,
        options,
        isEssential,
        isUseful,
        infoOnly,
        featureId,
        invisibleHighlightClass,
        statusClassName,
      );

      // Mark the ancestor as such, to make it easier to match the overlay
      // with this ancestor element
      visibleAncestor.classList.add(statusClassName + "-ancestor");

      return;
    }

    let highlightClass = options.broken
      ? HIGHLIGHT_BROKEN_FEATURE_CLASS
      : HIGHLIGHT_WORKING_FEATURE_CLASS;

    if (infoOnly) {
      highlightClass = HIGHLIGHT_INFO_FEATURE_CLASS;
    }

    addOverlayLabel(
      element,
      options,
      isEssential,
      isUseful,
      infoOnly,
      featureId,
      highlightClass,
      statusClassName,
    );
  });

  document.documentElement.classList.add(HIGHLIGHTING_DONE_CLASS);
};

const clearAllOverlays = () => {
  // Remove all overlays
  Array.from(document.getElementsByClassName(HIGHLIGHT_OVERLAY_CLASS))
    .forEach((element) => {
      console.log(element);
      element.remove();
    });

  // Must come last so that Puppeteer can detect it is done
  document.documentElement.classList.remove(HIGHLIGHTING_DONE_CLASS);
};

const onCommandReceived = (command) => {
  if (command.inspectNow) {
    inspectPage();
  }

  if (command.autoInspect) {
    browser.storage.local.set({
      autoInspect: Boolean(command.autoInspect.value),
    });

    if (command.autoInspect.value) {
      enableAutoInspectionOnPage();
    }
  }

  if (command.clearAllOverlays) {
    clearAllOverlays();
  }

  if (command.inspectPage) {
    inspectPage();
  }

  if (command.setHighlightFeatures) {
    setHighlightFeatures(
      command.setHighlightFeatures,
      command.highlighted,
      command.options,
    );
  }
};

browser.runtime.onMessage.addListener((message) => {
  console.log(message);

  onCommandReceived(message);
});

// Used for crawls, since the background page cannot be accessed in Firefox
window.addEventListener("howbrokenExtension", (e) => {
  onCommandReceived(e.detail);
});

console.log("hello world!");
