"use strict";

window.HB = window.HB || {};
window.HB.heuristics = window.HB.heuristics || {};

const findPageBodies = (options) => {
  // All page bodies are useful and essential

  if (options.broken) {
    return findPageBodies({
      useful: options.useful,
      essential: options.essential,
    })
      .filter((body) => {
        return window.HB.domUtils.isTextOnlyWhitespace(body.textContent)
          || window.HB.domUtils.isTextOnlyWhitespace(body.innerText);
      });
  }

  return [document.body];
};
window.HB.heuristics.findPageBodies = findPageBodies;

// TESTING
// https://www.sitepoint.com/understanding-css-content-property/
// https://www.bloomberg.com/opinion/articles/2020-08-18/inflation-shock-counts-less-than-covid-for-bonds-gold
// https://johnmorrisonline.com/new-native-lazy-/loading-attribute-for-html-img-and-iframe-tags/
// footer broken: https://wordpress.org/support/article/comments-in-wordpress/
const findStyleRegions = (options) => {
  // All style regions are useful and essential

  if (options.broken) {
    return findStyleRegions({
      useful: options.useful,
      essential: options.essential,
    }).filter(() => {
      const usesDefaultFontFamily = (element) => {
        const DEFAULT_FONT_FAMILY = "serif";
        const computedFontFamily = getComputedStyle(element).fontFamily;
        return !computedFontFamily
          || computedFontFamily === DEFAULT_FONT_FAMILY;
      };

      const usesDefaultFontSize = (element) => {
        const DEFAULT_FONT_SIZE = "16px";
        const computedFontSize = getComputedStyle(element).fontSize;
        return !computedFontSize
          || computedFontSize === DEFAULT_FONT_SIZE;
      };

      const TAG_NAMES_TO_INSPECT_FONT_FAMILY = [
        "body",
        "h1",
        "h2",
        "div",
        "p",
      ];

      // Style sheets usually set the font-family property for at least one of
      // those tags
      const isUsingDefaultFontFamily
        = TAG_NAMES_TO_INSPECT_FONT_FAMILY
          .filter((tagName) => document.querySelector(tagName))
          .every((tagName) => {
            const element = document.getElementsByTagName(tagName)[0];
            return usesDefaultFontFamily(element);
          });

      const TAG_NAMES_TO_INSPECT_FONT_SIZE = [
        "body",
        "div",
        "p",
      ];

      // Style sheets usually set the font-size property for at least one of
      // those tags
      const isUsingDefaultFontSize
        = TAG_NAMES_TO_INSPECT_FONT_SIZE
          .filter((tagName) => document.querySelector(tagName))
          .every((tagName) => {
            const element = document.getElementsByTagName(tagName)[0];
            return usesDefaultFontSize(element);
          });

      return isUsingDefaultFontFamily && isUsingDefaultFontSize;
    });
  }

  return [document.body];
};
window.HB.heuristics.findStyleRegions = findStyleRegions;

// TESTING
// https://www.hohner.de/en/service/accordion/faqs (with JS)
// https://www.reddit.com/r/linux/
const findCookieBanners = (options) => {
  // Cookie banners are considered useful and essential

  if (options.broken) {
    return findCookieBanners({
      useful: options.useful,
      essential: options.essential,
    });
  }

  const cookieBanners = window.HB.domUtils.keepOnlyOutmostElements(Array
    .from(document.querySelectorAll(`div[class*="cookie" i],
    div[aria-label*="cookie" i]`))
    .concat(Array.from(document.querySelectorAll('div[class*="notice" i]'))
      .filter((div) => /cookies/i.test(div.textContent))))
    .filter((div) => {
      // Check if this is indeed a banner, not a cookie wall
      return div.offsetHeight < window.innerHeight / 2;
    });

  return cookieBanners
    .concat(Array.from(document.scripts)
      .filter((script) => !/json/.test(script.type))
      .filter((script) => /cookie/i.test(script.textContent)
        && (/consent/i.test(script.textContent)
          || /banner/i.test(script.textContent))));
};
window.HB.heuristics.findCookieBanners = findCookieBanners;

const findPaywalls = (options) => {
  // Paywalls are considered useful and essential

  if (options.broken) {
    return findPaywalls({
      useful: options.useful,
      essential: options.essential,
    });
  }

  const paywalls = window.HB.domUtils
    .keepOnlyOutmostElements(Array.from(document
      .querySelectorAll('div[class*="paywall"], section[class*="paywall"]')));

  return paywalls;
};
window.HB.heuristics.findPaywalls = findPaywalls;

const isIFrameBroken = (iframe) => {
  const contentDocument = iframe.contentDocument;
  if (!contentDocument) {
    return true;
  }

  const contentBody = contentDocument.body;
  return !contentBody || contentBody.children.length === 0;
};

// TESTING
// https://www.cypress.io/
const findIFrames = (options) => {
  if (options.useful) {
    options.useful = false;
    options.essential = false;
    return findIFrames(options)
      .filter((iframe) => window.HB.common.isElementInMainSection(iframe));
  }

  if (options.essential) {
    // External content is not essential, it requires user approval
    return [];
  }

  if (options.broken) {
    return findIFrames({
      useful: options.useful,
      essential: options.essential,
    }).filter((iframe) => isIFrameBroken(iframe));
  }

  return Array.from(document.getElementsByTagName("iframe"));
};
window.HB.heuristics.findIFrames = findIFrames;

// TESTING
// https://www.cypress.io/
const LARGE_IFRAME_MIN_SIZE = 100;
const findLargeIFrames = (options) => {
  if (options.useful) {
    options.useful = false;
    options.essential = false;
    return findLargeIFrames(options)
      .filter((iframe) => window.HB.common.isElementInMainSection(iframe));
  }

  if (options.essential) {
    // External content is not essential, it requires user approval
    return [];
  }

  if (options.broken) {
    return findLargeIFrames({
      useful: options.useful,
      essential: options.essential,
    }).filter((iframe) => isIFrameBroken(iframe));
  }

  // https://www.w3.org/TR/CSS2/visudet.html#inline-replaced-width
  const DEFAULT_IFRAME_WIDTH = 300;
  const DEFAULT_IFRAME_HEIGHT = 150;

  return Array.from(document.getElementsByTagName("iframe"))
    .filter((iframe) => {
      const iframeWidth = iframe.offsetWidth;
      const iframeHeigth = iframe.offsetHeight;
      // Do not consider the IFrame large if it has default size
      return iframeWidth !== DEFAULT_IFRAME_WIDTH
        && iframeHeigth !== DEFAULT_IFRAME_HEIGHT
        && iframeWidth >= LARGE_IFRAME_MIN_SIZE
        && iframeHeigth >= LARGE_IFRAME_MIN_SIZE;
    });
};
window.HB.heuristics.findLargeIFrames = findLargeIFrames;

const findMapIFrame = (options) => {
  if (options.useful) {
    options.useful = false;
    options.essential = false;
    return findMapIFrame(options)
      .filter((iframe) => window.HB.common.isElementInMainSection(iframe));
  }

  if (options.essential) {
    // External content is not essential, it requires user approval
    return [];
  }

  if (options.broken) {
    return findMapIFrame({
      useful: options.useful,
      essential: options.essential,
    }).filter((iframe) => isIFrameBroken(iframe));
  }

  const EXTERNAL_MAP_URL_REGEXPS = [
    /^https?:\/\/maps\.google\.com\/maps\//,
    /^https?:\/\/www\.openstreetmap\.org\/export\/embed\.html/,
    /^https?:\/\/api\.mapbox\.com\//,
  ];

  return Array.from(document.getElementsByTagName("iframe"))
    .filter((iframe) => {
      return iframe.src && EXTERNAL_MAP_URL_REGEXPS.some((urlRegexp) => {
        return urlRegexp.test(iframe.src);
      });
    });
};
window.HB.heuristics.findMapIFrame = findMapIFrame;

// https://lea.verou.me/2020/07/introspecting-css-via-the-css-om-getting-supported-properties-shorthands-longhands/
const findVideoIFrames = (options) => {
  if (options.useful) {
    options.useful = false;
    options.essential = false;
    return findVideoIFrames(options)
      .filter((iframe) => {
        return window.HB.common.isElementInMainSection(iframe);
      });
  }

  if (options.essential) {
    // External content is not essential, it requires user approval
    return [];
  }

  if (options.broken) {
    return findVideoIFrames({
      useful: options.useful,
      essential: options.essential,
    }).filter((iframe) => isIFrameBroken(iframe));
  }

  return Array.from(document.querySelectorAll('iframe[src*="/embed/"]'));
};
window.HB.heuristics.findVideoIFrames = findVideoIFrames;

// TESTING
// https://semantic-ui.com/
// https://blog.grimm-co.com/2020/07/dji-privacy-analysis-validation.html
const findMicrobloggingWidgets = (options) => {
  if (options.useful) {
    options.useful = false;
    options.essential = true;
    return findMicrobloggingWidgets(options);
  }

  if (options.essential) {
    options.essential = false;
    return findMicrobloggingWidgets(options)
      .filter((widget) => {
        return window.HB.common.isElementInMainSection(widget);
      });
  }

  if (options.broken) {
    return findMicrobloggingWidgets({
      useful: options.useful,
      essential: options.essential,
    });
  }

  const MICROBLOGGING_SITES = [
    /twitter/,
    /mastodon/,
  ];

  return Array.from(document.querySelectorAll(`
    script[src*="widget"][src*="twitter"]
    `))
    .concat(Array.from(document.scripts)
      .filter((script) => !/json/.test(script))
      .filter((script) => {
        return /widget/.test(script.textContent)
          && MICROBLOGGING_SITES.some((site) => {
            return site.test(script.textContent);
          });
      }));
};
window.HB.heuristics.findMicrobloggingWidgets = findMicrobloggingWidgets;

const NOSCRIPT_SELECTOR = `
    noscript,
    .no-js,
    .js-only,
    .js-off,
    .noscript,
    .noscript-warning,
    .noJsHide,
    .ls-js-hidden
`;

// TESTING
// https://lea.verou.me/2020/09/the-failed-promise-of-web-components/
const findNoScriptFallbacks = (options) => {
  if (options.useful) {
    return [];
  }

  if (options.essential) {
    return [];
  }

  if (options.broken) {
    return findNoScriptFallbacks({
      useful: options.useful,
      essential: options.essential,
    });
  }

  return Array.from(document.querySelectorAll(NOSCRIPT_SELECTOR));
};
window.HB.heuristics.findNoScriptFallbacks = findNoScriptFallbacks;

const isCarouselBroken = async (carousel) => {
  const buttonNext = carousel.querySelector(`
    button[class*="-next"],
    a[class*="-next"]`);
  const buttonPrevious = carousel.querySelector(`
    button[class*="-prev"],
    button[class*="-previous"],
    a[class*="-prev"],
    a[class*="-previous"]`);

  const hasHiddenElement
    = Boolean(carousel.querySelector(`
      *[aria-hidden="true"],
      *[aria-selected="false"]`));

  // A carousel needs at least one button if at least one element is hidden
  if (hasHiddenElement && !buttonNext && !buttonPrevious) {
    return true;
  }

  const hasEventHandlerType = window.HB.common.hasEventHandlerType;

  if (
    !hasHiddenElement
    || buttonNext && await hasEventHandlerType(buttonNext, "click")
    || buttonPrevious && await hasEventHandlerType(buttonPrevious, "click")
  ) {
    return false;
  }

  return true;
};

// TESTING
// https://blog.checkpoint.com/2020/08/06/achilles-small-chip-big-peril/
// https://www.cypress.io/
// https://www.ebay.com/
// https://www.imdb.com/
const findCarousels = async (options) => {
  if (options.useful) {
    options.useful = false;
    options.essential = false;
    return await findCarousels(options);
  }

  if (options.essential) {
    options.essential = false;
    return (await findCarousels(options))
      .filter((carousel) => {
        return !window.HB.common.isElementNotUseful(carousel)
          && window.HB.common.isElementInMainSection(carousel);
      });
  }

  if (options.broken) {
    return window.HB.utils.asyncFilter(
      await findCarousels({
        useful: options.useful,
        essential: options.essential,
      }),
      async (carousel) => await isCarouselBroken(carousel),
    );
  }

  const carouselElements = window.HB.domUtils.querySelectorAllExtra(
    document,
    `
    div[class_="carousel"],
    div[class_="carrousel"],
    [aria-label_="carousel"],
    [aria-label_="carrousel"],
    [role_="carousel"],
    [role_="carrousel"],
    [aria-roledescription_="carousel"],
    [aria-roledescription_="carrousel"],
    div[class_="swiper"]`,
  );

  return window.HB.domUtils.keepOnlyOutmostElements(carouselElements);
};
window.HB.heuristics.findCarousels = findCarousels;

// TESTING
// https://www.npmjs.com/package/npm
// https://github.com/facebook/jest/
// https://hub.docker.com/_/node/
// https://docs.docker.com/storage/volumes/#use-a-read-only-volume
// https://guaranteedinstallmentloans.com/
const findTabButtons = async (options) => {
  if (options.useful) {
    options.useful = false;
    options.essential = false;
    return findTabButtons(options);
  }

  if (options.essential) {
    return [];
  }

  if (options.broken) {
    return (await findTabButtons({
      useful: options.useful,
      essential: options.essential,
    }))
      .filter((tabList) => !Array.from(tabList.querySelectorAll("a"))
        .every((anchor) => {
          const hrefAttr = anchor.getAttribute("href");
          return hrefAttr && !/\/\//.test(hrefAttr) && hrefAttr !== "#";
        }));
  }

  // Remove a potential hash and trailing slash
  const currentURL = window.location.href.replace(/\/?(?:#.*)?$/, "");

  return window.HB.utils.asyncFilter(
    Array.from(document.querySelectorAll("li")),
    async (li) => {
      const ul = li.parentElement;
      if (ul.tagName !== "UL") {
        return false;
      }

      if (ul.children.length < 2
        || getComputedStyle(ul).listStyleType !== "none") {
        return false;
      }

      // Check that the tab button links to a subpath
      const anchors = li.querySelectorAll("a");
      if (anchors.length !== 1) {
        return false;
      }

      if (!window.HB.utils.linkToSubpath(
        currentURL,
        anchors[0].href,
      )) {
        return false;
      }

      return await window.HB.utils.asyncSome(
        [li].concat(Array.from(li.querySelectorAll("a, div, span"))),
        (element) => window.HB.common.isElementTabButtonLike(element),
      );
    },
  );
};
window.HB.heuristics.findTabButtons = findTabButtons;

const findModals = (options) => {
  if (options.useful) {
    options.useful = false;
    options.essential = true;
    return findModals(options);
  }

  if (options.essential) {
    options.essential = false;
    return findModals(options);
  }

  return [];
};
window.HB.heuristics.findModals = findModals;

// TESTING
// https://work.lisk.in/2020/11/20/even-faster-bash-startup.html
// https://www.flailingmonkey.com/view-dom-events-in-firefox-developer-tools/
// https://lights0123.com/blog/2020/07/25/async-await-for-avr-with-rust/
const findExternalCommentSections = (options) => {
  if (options.useful) {
    options.useful = false;
    options.essential = false;
    return findExternalCommentSections(options);
  }

  if (options.essential) {
    // Since it is external, should ask the user before loading it, so this
    // should be considered useful, but not essential
    return [];
  }

  if (options.broken) {
    return findExternalCommentSections({
      useful: options.useful,
      essential: options.essential,
    })
      .filter((commentSection) => {
        return commentSection.offsetHeight === 0;
      });
  }

  let externalCommentSections = [];

  const disqusThread = document.getElementById("disqus_thread");
  if (disqusThread) {
    externalCommentSections.push(disqusThread);
  }

  // https://utteranc.es/
  // utteranc.es https://lights0123.com/blog/2020/07/25/async-await-for-avr-with-rust/
  let utterancesSections = [];
  utterancesSections = utterancesSections
    .concat(Array.from(document.getElementsByTagName("iframe"))
      .filter((iframe) => window.HB.utils
        .getURLHostname(iframe.src) === "utteranc.es"))
    .concat(Array.from(document
      .querySelectorAll('script[src="https://utteranc.es/client.js"]')));
  externalCommentSections = externalCommentSections.concat(utterancesSections);

  return externalCommentSections;
};
window.HB.heuristics.findExternalCommentSections = findExternalCommentSections;

const findGraphDivs = (options) => {
  if (options.useful) {
    options.useful = false;
    options.essential = false;
    return findGraphDivs(options);
  }

  if (options.essential) {
    // Since it is external, should ask the user before loading it, so this
    // should be considered useful, but not essential
    return [];
  }

  if (options.broken) {
    return findGraphDivs({
      useful: options.useful,
      essential: options.essential,
    })
      .filter((graphDiv) => {
        return graphDiv.offsetHeight === 0;
      });
  }

  return Array.from(document.querySelectorAll('div[id*="graph-"]'));
};
window.HB.heuristics.findGraphDivs = findGraphDivs;

// TESTING
// https://mailfence.com/
const findCAPTCHAs = (options) => {
  if (options.useful) {
    options.useful = false;
    options.essential = false;
    return findCAPTCHAs(options);
  }

  if (options.essential) {
    // Only non extenal CAPTCHAs should be considered essentials
    return [];
  }

  if (options.broken) {
    return findCAPTCHAs({
      useful: options.useful,
      essential: options.essential,
    });
  }

  return window.HB.domUtils
    .keepOnlyOutmostElements(window.HB.domUtils.querySelectorAllExtra(
      document,
      `
      script[src_="captcha"],
      div[id_="captcha"],
      div[class_="captcha"],
      img[alt_="captcha"]`,
    ));
};
window.HB.heuristics.findCAPTCHAs = findCAPTCHAs;

// TESTING
// https://github.com/jsdom/jsdom/issues/
// https://tracker.debian.org/pkg/firefox
// https://www.nextinpact.com/
const findPagination = (options) => {
  if (options.useful) {
    options.useful = false;
    options.essential = false;
    return findPagination(options);
  }

  if (options.essential) {
    return [];
  }

  if (options.broken) {
    return findPagination({
      useful: options.useful,
      essential: options.essential,
    })
      .filter((pagination) => {
        const hasLinkToOtherPage = Array.from(pagination.querySelectorAll("a"))
          .some((anchor) => {
            return anchor.href && !window.HB.utils.linkToSamePage(
              anchor.href,
              window.location.href,
            );
          });

        if (hasLinkToOtherPage) {
          return false;
        }

        return true;
      });
  }

  return window.HB.domUtils
    .keepOnlyOutmostElements(window.HB.domUtils.selectorsMatches({
      match: `div[class*="pagination" i], div[class*="paginate" i],
        div[aria-label*="pagination" i],
        ul[class*="pagination" i],
        ol[class*="pagination" i],
        div[class*="pager" i]`,
      exclude: "",
    }));
};
window.HB.heuristics.findPagination = findPagination;

// TESTING
// https://www.zoho.com/
// (::before pseudo-element)
const findLooksButtonLike = async () => {
  const candidateElements = Array.from(document
    .querySelectorAll("div, span, label, li, a"));

  const hasBorder = window.HB.common.hasBorder;

  const backgroundButtons = await window.HB.utils.asyncFilter(
    candidateElements,
    async (element) => {
      const isBackgroundButton = window.HB.domUtils.hasPointerCursor(element)
        && (window.HB.domUtils.hasDifferentBackgroundColorThanAncestor(element)
          || await window.HB.common.hasDifferentHoverColor(element));
      return isBackgroundButton;
    },
  );

  // console.log(backgroundButtons.flatMap((element) => [element, element.className]));

  const outlineButtons = candidateElements.filter((element) => {
    const elementStyle = getComputedStyle(element);
    return window.HB.domUtils.hasPointerCursor(element)
      && hasBorder(elementStyle, "all");
  });

  // console.log(outlineButtons.flatMap((element) => [element, element.className]));

  const tabButtons = await window.HB.utils.asyncFilter(
    candidateElements,
    (element) => {
      return window.HB.common.isElementTabButtonLike(element);
    },
  );

  // console.log(tabButtons.flatMap((element) => [element, element.className]));

  return []
    .concat(backgroundButtons)
    .concat(outlineButtons)
    .concat(tabButtons);
};
window.HB.heuristics.findLooksButtonLike = findLooksButtonLike;

// TESTING
// https://devexpress.github.io/testcafe/
// https://commitlint.js.org/ (app div)
// https://www.theguardian.com/international
// https://github.com/facebook/jest/
const findEmptyDivsWithId = () => {
  return window.HB.domUtils.selectorsMatches({
    match: 'div[id]:not([id=""]):empty',
    exclude: "",
  })
    .filter((div) => {
      const backgroundImage = getComputedStyle(div).backgroundImage;
      return !backgroundImage || backgroundImage === "none";
    });
};
window.HB.heuristics.findEmptyDivsWithId = findEmptyDivsWithId;

// TESTING
// https://wiki.lineageos.org/devices/
// https://video.ibm.com/
// https://www.apple.com/
// https://www.furet.com/
// https://www.bleepingcomputer.com
// This only reads the DOM, it cannot intercept fonts imported from CSS or
// loaded using JavaScript
const findFontStylesheets = () => {
  const FONT_HREF_REGEXEPS = [
    /^https?:\/\/fonts?\./,
    /\/fonts?\//,
    /\/fonts\?famil(?:y|ies)=/, // https://www.apple.com/
    /\/css\?famil(?:y|ies)=/, // https://www.furet.com/
    /\/font-?awesome\.css$/, // https://www.bleepingcomputer.com
    /_fonts/, // https://video.ibm.com/
  ];

  // CSP
  const fontStylesheetHrefs = Array
    .from(document.styleSheets)
    .map((stylesheet) => {
      try {
        return stylesheet.href;
      } catch {
        //
      }

      return "";
    });

  const fontStylesheets = fontStylesheetHrefs.map((href) => {
    const isFontStylesheet = FONT_HREF_REGEXEPS.some((fontHrefRegexp) => {
      return fontHrefRegexp.test(href);
    });

    if (isFontStylesheet) {
      console.log(href);
      // We have to return an element, so make one up
      return document.createElement("style");
    }

    return null;
  })
    .filter((style) => Boolean(style));

  return fontStylesheets;
};
window.HB.heuristics.findFontStylesheets = findFontStylesheets;

// TESTING
// https://framagit.org/framasoft/docs
// https://freefrontend.com/css-off-canvas-menus/ (Imunify360)
// https://powerpackelements.com/off-canvas-menu-elementor/
// https://webpack.js.org/
// https://deno.land/x
// https://www.imdb.com/
const findRequestLoadingIndicators = () => {
  return window.HB.domUtils
    .keepOnlyOutmostElements(window.HB.domUtils.selectorsMatchesExtra({
      match: `[class*="spinner"], span[aria-label*="loading"],
        i[class_="loading"],
        svg[arial-label*="loading"],
        [class_="spin"], div[class*="skeleton"],
        div[class*="loader"], div[class*="loading"],
        img[alt^="loading" i],
        img[src_="loader"],
        img[src_="loading"],
        svg[class_="loader"],
        div[class_="placeholder"]`,
      exclude: "",
    })
      .concat(Array.from(document.querySelectorAll("div"))
        .filter((div) => window.HB.domUtils
          .getBackgroundImageURLs(getComputedStyle(div))
          .some((url) => /\bloader\b/i.test(url.pathname)))));
};
window.HB.heuristics.findRequestLoadingIndicators
  = findRequestLoadingIndicators;

// TESTING
// https://www.cypress.io/
// https://www.npmjs.com/package/fontoxpath/v/3.1.0
const findProtectedEmails = (options) => {
  if (options.essential) {
    options.essential = false;
    return findProtectedEmails(options)
      .filter((email) => {
        return window.HB.common.isElementInMainSection(email);
      });
  }

  return window.HB.domUtils
    .keepOnlyInmostElements(Array.from(document.querySelectorAll("*"))
      .filter((el) => /\[email\sprotected\]/.test(el.textContent)));
};
window.HB.heuristics.findProtectedEmails = findProtectedEmails;

// TESTING
// https://www.cypress.io/
// https://pypi.org/
const findNoscriptWarnings = () => {
  return Array.from(document.querySelectorAll("noscript"))
    .filter((el) => /javascript/i.test(el.textContent));
};
window.HB.heuristics.findNoscriptWarnings = findNoscriptWarnings;

// TESTING
// https://medium.com
// https://twitter.com
// https://guaranteedinstallmentloans.com/
// https://www.sitepoint.com
const findObfuscatedClassNameBody = () => {
  const pageClassNames
    = Array.from(new Set(Array.from(document.querySelectorAll("*"))
      .map((element) => Array.from(element.classList)).flat()));

  const obfuscatedClassNames = pageClassNames
    .filter((className) => {
      // https://medium.com
      return /^[a-z]{1,2}$/i.test(className)
      // https://www.sitepoint.com
      // https://twitter.com/
        || !/l10n|i18n/i.test(className)
          && /^(?:[A-Za-z]-)?[A-Za-z_]+\d+[A-Za-z][A-Za-z0-9]*$/i
            .test(className);
    });

  console.log(obfuscatedClassNames, pageClassNames);

  if (obfuscatedClassNames.length > 50
    || obfuscatedClassNames.length > pageClassNames.length / 2) {
    return [document.body];
  }

  return [];
};
window.HB.heuristics.findObfuscatedClassNameBody
  = findObfuscatedClassNameBody;

// TESTING
// https://www.nerdydata.com/
// https://ivanceras.github.io/futuristic-ui/
const findFullPageApps = (options) => {
  const FULL_PAGE_DIV_ID = [
    "app",
    "main",
    "root",
  ];

  // All full page apps are useful and essential

  if (options.broken) {
    return findFullPageApps({
      useful: options.useful,
      essential: options.essential,
    })
      .filter((appDiv) => {
        return appDiv.matches(":scope:empty");
      });
  }

  return Array.from(document.querySelectorAll("div[id], section[id]"))
    .filter((div) => FULL_PAGE_DIV_ID.includes(div.id));
};
window.HB.heuristics.findFullPageApps = findFullPageApps;

// TESTING
// https://mlwhiz.com/
// https://quantumwarp.com/
const findLoaderOverlays = () => {
  return window.HB.domUtils
    .keepOnlyOutmostElements(window.HB.domUtils.selectorsMatchesExtra({
      match: 'body > #preloader, body > div[class_="preloader"]',
      exclude: "",
    }));
};
window.HB.heuristics.findLoaderOverlays = findLoaderOverlays;

const findFadeinElements = () => {
  return Array.from(document.querySelectorAll(".js-fadein, .js-fadein-hero"));
};
window.HB.heuristics.findFadeinElements = findFadeinElements;

const findMainSections = (options) => {
  if (options.broken) {
    return [];
  }

  return window.HB.domUtils
    .keepOnlyOutmostElements(Array.from(document.querySelectorAll("*"))
      .filter((el) => window.HB.common.isElementInMainSection(el)));
};
window.HB.heuristics.findMainSections = findMainSections;

const findSidebars = (options) => {
  if (options.broken) {
    return [];
  }

  return window.HB.domUtils
    .keepOnlyOutmostElements(Array.from(document.querySelectorAll("*"))
      .filter((el) => window.HB.common.isElementInSidebar(el)));
};
window.HB.heuristics.findSidebars = findSidebars;

const findCustomElements = () => {
  return Array.from(document.querySelectorAll("*"))
    .filter((el) => !window.HB.domUtils.isNativeHTMLElement(el));
};
window.HB.heuristics.findCustomElements = findCustomElements;

const findFeatureElements = async (featureName, options) => {
  // console.log(featureName, options);

  if (window.HB.FEATURE_DATA[featureName]) {
    const heuristicFunction = window.HB.heuristics[featureName];

    if (heuristicFunction) {
      if (options.working) {
        const brokenElements = new Set(await findFeatureElements(
          featureName,
          {
            broken: true,
            useful: options.useful,
            essential: options.essential,
          },
        ));

        const allFeatureElements = await findFeatureElements(
          featureName,
          { essential: options.essential, useful: options.useful },
        );

        return allFeatureElements.filter((element) => {
          return !brokenElements.has(element);
        });
      }

      return heuristicFunction(options);
    }
  }

  return [];
};
window.HB.findFeatureElements = findFeatureElements;

const getFeatureList = async () => {
  // FIXME: because of resistFingerprinting, the resolution of Date is limited
  // to 100 ms, rely on the console timestamps instead
  console.log("getFeatureList – start");
  window.HB._cacheStore.invalidateAll();

  // TODO: maybe use Promise.allSettled instead
  // (it is not yet supported on Firefox on Android)
  const features = await Promise.all(Object.keys(window.HB.FEATURE_DATA)
    .map(async (featureName) => {
      console.log(`${featureName} – 1`);
      const allElements = findFeatureElements(featureName, {});
      const brokenElements = findFeatureElements(
        featureName,
        { broken: true },
      );
      const essentialElements = findFeatureElements(
        featureName,
        { essential: true },
      );
      const usefulElements = findFeatureElements(
        featureName,
        { useful: true },
      );
      console.log(`${featureName} – 2`);

      const awaitedBrokenElements = await brokenElements;

      return {
        [featureName]: {
          id: window.HB.FEATURE_DATA[featureName].id,
          visible: {
            all: {
              broken: (await allElements)
                .filter((el) => awaitedBrokenElements.includes(el)
                  && !window.HB.common.isElementHidden(el)).length,
              working: (await allElements)
                .filter((el) => !awaitedBrokenElements.includes(el)
                  && !window.HB.common.isElementHidden(el)).length,
            },
            useful: {
              broken: (await usefulElements)
                .filter((el) => awaitedBrokenElements.includes(el)
                  && !window.HB.common.isElementHidden(el)).length,
              working: (await usefulElements)
                .filter((el) => !awaitedBrokenElements.includes(el)
                  && !window.HB.common.isElementHidden(el)).length,
            },
            essential: {
              broken: (await essentialElements)
                .filter((el) => awaitedBrokenElements.includes(el)
                  && !window.HB.common.isElementHidden(el)).length,
              working: (await essentialElements)
                .filter((el) => !awaitedBrokenElements.includes(el)
                  && !window.HB.common.isElementHidden(el)).length,
            },
            main: {
              broken: (await allElements)
                .filter((el) => awaitedBrokenElements.includes(el)
                  && !window.HB.common.isElementHidden(el))
                .filter((el) => window.HB.common.isElementInMainSection(el))
                .length,
              working: (await allElements)
                .filter((el) => !awaitedBrokenElements.includes(el)
                  && !window.HB.common.isElementHidden(el))
                .filter((el) => window.HB.common.isElementInMainSection(el))
                .length,
            },
          },
          hidden: {
            all: {
              broken: (await allElements)
                .filter((el) => awaitedBrokenElements.includes(el)
                  && window.HB.common.isElementHidden(el)).length,
              working: (await allElements)
                .filter((el) => !awaitedBrokenElements.includes(el)
                  && window.HB.common.isElementHidden(el)).length,
            },
            useful: {
              broken: (await usefulElements)
                .filter((el) => awaitedBrokenElements.includes(el)
                  && window.HB.common.isElementHidden(el)).length,
              working: (await usefulElements)
                .filter((el) => !awaitedBrokenElements.includes(el)
                  && window.HB.common.isElementHidden(el)).length,
            },
            essential: {
              broken: (await essentialElements)
                .filter((el) => awaitedBrokenElements.includes(el)
                  && window.HB.common.isElementHidden(el)).length,
              working: (await essentialElements)
                .filter((el) => !awaitedBrokenElements.includes(el)
                  && window.HB.common.isElementHidden(el)).length,
            },
            main: {
              broken: (await allElements)
                .filter((el) => awaitedBrokenElements.includes(el)
                  && window.HB.common.isElementHidden(el))
                .filter((el) => window.HB.common.isElementInMainSection(el))
                .length,
              working: (await allElements)
                .filter((el) => !awaitedBrokenElements.includes(el)
                  && window.HB.common.isElementHidden(el))
                .filter((el) => window.HB.common.isElementInMainSection(el))
                .length,
            },
          },
        },
      };
    }));

  const featureList = Object.assign({}, ...features);

  console.log("getFeatureList – stop");

  return featureList;
};
window.HB.getFeatureList = getFeatureList;

console.log("feature-inspection");
