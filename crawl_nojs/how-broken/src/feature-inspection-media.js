"use strict";

window.HB = window.HB || {};
window.HB.heuristics = window.HB.heuristics || {};

const findVideoTags = (options) => {
  // All video tags are considered useful

  if (options.essential) {
    options.essential = false;
    return findVideoTags(options)
      .filter((video) => {
        return window.HB.common.isElementInMainSection(video);
      });
  }

  if (options.broken) {
    return findVideoTags({
      useful: options.useful,
      essential: options.essential,
    }).filter((video) => video.readyState === 0);
  }

  return Array.from(document.getElementsByTagName("video"));
};
window.HB.heuristics.findVideoTags = findVideoTags;

const findAudioTags = (options) => {
  // All video tags are considered useful

  if (options.essential) {
    options.essential = false;
    return findAudioTags(options)
      .filter((audio) => {
        return window.HB.common.isElementInMainSection(audio);
      });
  }

  if (options.broken) {
    return findAudioTags({
      useful: options.useful,
      essential: options.essential,
    }).filter((audio) => audio.readyState === 0);
  }

  return Array.from(document.getElementsByTagName("audio"));
};
window.HB.heuristics.findAudioTags = findAudioTags;

const findCanvasTags = (options) => {
  // All video tags are considered useful

  if (options.essential) {
    options.essential = false;
    return findCanvasTags(options)
      .filter((canvas) => {
        return window.HB.common.isElementInMainSection(canvas);
      });
  }

  if (options.broken) {
    return findCanvasTags({
      useful: options.useful,
      essential: options.essential,
    });
  }

  return Array.from(document.getElementsByTagName("canvas"));
};
window.HB.heuristics.findCanvasTags = findCanvasTags;
