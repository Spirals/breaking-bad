"use strict";

const OPTIONS = {
  autoInspect: {
    id: "auto-inspect",
    default: false,
  },
};

Object.entries(OPTIONS).forEach(([name, option]) => {
  document.getElementById(option.id).addEventListener("change", (e) => {
    browser.storage.local.set({
      [name]: e.target.checked,
    });
  });
});

const restoreOptions = () => {
  const keys = Object.fromEntries(Object.entries(OPTIONS)
    .map(([name, option]) => [name, option.default]));

  browser.storage.local.get(keys)
    .then((optionsValues) => {
      console.log(optionsValues);
      Object.entries(optionsValues)
        .forEach(([name, value]) => {
          document.getElementById(OPTIONS[name].id).checked = value;
        });
    }, (err) => {
      console.error(err);
    });
};

document.addEventListener("DOMContentLoaded", restoreOptions);
