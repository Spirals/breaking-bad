"use strict";

window.HB = window.HB || {};
window.HB.utils = window.HB.utils || {};

const getURLHostname = (url) => {
  if (!url) {
    return "";
  }

  if (url.startsWith("//")) {
    url = window.location.protocol + url;
  }

  return new URL(url).hostname;
};
window.HB.utils.getURLHostname = getURLHostname;

const getURLProtocol = (url) => {
  if (!url) {
    return "";
  }

  if (url.startsWith("//")) {
    url = window.location.protocol + url;
  }

  return new URL(url).protocol;
};
window.HB.utils.getURLProtocol = getURLProtocol;

const getURLSearch = (url) => {
  if (!url) {
    return "";
  }

  if (url.startsWith("//")) {
    url = window.location.protocol + url;
  }

  return new URL(url).search;
};
window.HB.utils.getURLSearch = getURLSearch;

const getURLSearchParams = (url) => {
  if (!url) {
    return null;
  }

  if (url.startsWith("//")) {
    url = window.location.protocol + url;
  }

  return new URL(url).searchParams;
};
window.HB.utils.getURLSearchParams = getURLSearchParams;

const haveSameOrigins = (str1, str2) => {
  try {
    const url1 = new URL(str1);
    const url2 = new URL(str2);

    return url1.origin === url2.origin;
  } catch(e) {
    return false;
  }
};
window.HB.utils.haveSameOrigins = haveSameOrigins;

const linkToSamePage = (str1, str2) => {
  try {
    const url1 = new URL(str1);
    const url2 = new URL(str2);

    return url1.origin === url2.origin
      && url1.pathname === url2.pathname
      && url1.search === url2.search;
  } catch(e) {
    return false;
  }
};
window.HB.utils.linkToSamePage = linkToSamePage;

const linkToSubpath = (str1, str2) => {
  try {
    const url1 = new URL(str1);
    const url2 = new URL(str2);

    return url1.origin === url2.origin
      && url1.pathname === "/"
      ? true
      : new RegExp(`^${url1.pathname}(?:\\/.*)?$`).test(url2.pathname);
  } catch(e) {
    return false;
  }
};
window.HB.utils.linkToSubpath = linkToSubpath;

const asyncFilter = async (array, predicate) => {
  const fail = Symbol();

  return (await Promise.all(array.map(async (item) => {
    return await predicate(item) ? item : fail;
  })))
    .filter((item) => item !== fail);
};
window.HB.utils.asyncFilter = asyncFilter;

const asyncFlatMap = async (array, callback) => {
  return (await Promise.all(array.map(callback)))
    .flat();
};
window.HB.utils.asyncFlatMap = asyncFlatMap;

const asyncSome = async (array, predicate) => {
  for (const item of array) {
    // Run the predicates sequentially
    /* eslint-disable-next-line no-await-in-loop */
    if (await predicate(item)) {
      return true;
    }
  }

  return false;
};
window.HB.utils.asyncSome = asyncSome;

const asyncEvery = async (array, predicate) => {
  for (const item of array) {
    // Run the predicates sequentially
    /* eslint-disable-next-line no-await-in-loop */
    if (!await predicate(item)) {
      return false;
    }
  }

  return true;
};
window.HB.utils.asyncEvery = asyncEvery;
