"use strict";

const FEATURE_TABLE_ID = "feature-table";

const getCurrentTab = () => {
  return browser.tabs.query({
    active: true,
    windowId: browser.windows.WINDOW_ID_CURRENT,
  });
};

const setHighlightFeature = (featureName, highlighted, options) => {
  getCurrentTab().then((tabs) => {
    const tabId = tabs[0].id;

    browser.tabs.sendMessage(
      tabId,
      {
        setHighlightFeatures: [featureName],
        highlighted,
        options,
      },
    );
  });
};

const getFeatureTableRow = (featureName, feature) => {
  const brokenCount = feature.visible.all.broken
    + feature.hidden.all.broken;
  const workingCount = feature.visible.all.working
    + feature.hidden.all.working;
  const totalCount = feature.visible.all.broken
    + feature.visible.all.working
    + feature.hidden.all.broken
    + feature.hidden.all.working;
  const fullyWorking = brokenCount === 0;
  const someWorking = workingCount > 0;

  const featureTrNode = document.createElement("tr");

  const featureNameNode = document.createElement("td");
  const featureNameLabelNode = document.createElement("label");
  const displayName = window.HB.FEATURE_DATA[featureName].displayName;
  const featureId = window.HB.FEATURE_DATA[featureName].id;
  const featureNameText
    = document.createTextNode(`${displayName} (id: ${featureId})`);
  featureNameLabelNode.classList.add("feature-name-label");
  featureNameLabelNode.appendChild(featureNameText);
  featureNameNode.appendChild(featureNameLabelNode);

  featureTrNode.appendChild(featureNameNode);

  const featureBrokenNode = document.createElement("td");
  featureBrokenNode.classList.add("feature-broken");

  const featureBrokenCountNode = document.createElement("label");
  if (!fullyWorking) {
    featureBrokenCountNode.classList.add("feature-broken-count");
  }
  const hiddenBrokenCount = feature.hidden.all.broken;
  const featureBrokenCountText
    = document.createTextNode(`${feature.visible.all.broken}${hiddenBrokenCount
      ? `(+${hiddenBrokenCount})` : ""}/${totalCount}`);
  featureBrokenCountNode.appendChild(featureBrokenCountText);

  if (!fullyWorking && window.HB.FEATURE_DATA[featureName].highlightable) {
    const featureBrokenChkBoxNode = document.createElement("input");
    featureBrokenChkBoxNode.type = "checkbox";
    featureBrokenChkBoxNode.setAttribute("data-feature-name", featureName);
    featureBrokenChkBoxNode.classList.add("feature-broken-chkbox");

    featureBrokenChkBoxNode.addEventListener("change", (e) => {
      const brokenFeatureName = e.target.getAttribute("data-feature-name");

      setHighlightFeature(
        brokenFeatureName,
        e.target.checked,
        { broken: true },
      );
    });
    featureBrokenCountNode.appendChild(featureBrokenChkBoxNode);
  }

  featureBrokenNode.appendChild(featureBrokenCountNode);
  featureTrNode.appendChild(featureBrokenNode);

  const featureWorkingNode = document.createElement("td");
  featureWorkingNode.classList.add("feature-working");

  const featureWorkingCountNode = document.createElement("label");
  if (someWorking) {
    featureWorkingCountNode.classList.add("feature-working-count");
  }
  const visibleWorkingCount = feature.visible.all.working;
  const hiddenWorkingCount = feature.hidden.all.working;
  const featureWorkingCountText
    = document.createTextNode(`${visibleWorkingCount}${hiddenWorkingCount
      ? `(+${hiddenWorkingCount})` : ""}/${totalCount}`);
  featureWorkingCountNode.appendChild(featureWorkingCountText);

  if (someWorking && window.HB.FEATURE_DATA[featureName].highlightable) {
    const featureWorkingChkBoxNode = document.createElement("input");
    featureWorkingChkBoxNode.type = "checkbox";
    featureWorkingChkBoxNode.setAttribute("data-feature-name", featureName);
    featureWorkingChkBoxNode.classList.add("feature-working-chkbox");

    featureWorkingChkBoxNode.addEventListener("change", (e) => {
      const workingFeatureName = e.target.getAttribute("data-feature-name");

      setHighlightFeature(
        workingFeatureName,
        e.target.checked,
        { working: true },
      );
    });
    featureWorkingCountNode.appendChild(featureWorkingChkBoxNode);
  }

  featureWorkingNode.appendChild(featureWorkingCountNode);
  featureTrNode.appendChild(featureWorkingNode);

  const featureEssentialNode = document.createElement("td");
  featureEssentialNode.classList.add("feature-working");

  const featureEssentialCountNode = document.createElement("label");
  const visibleEssentialCount = feature.visible.essential.broken
    + feature.visible.essential.working;
  const hiddenEssentialCount = feature.hidden.essential.broken
    + feature.hidden.essential.working;
  const featureEssentialCountText
    = document.createTextNode(`${visibleEssentialCount}${hiddenEssentialCount
      ? `(+${hiddenEssentialCount})` : ""}/${totalCount}`);
  featureEssentialCountNode.appendChild(featureEssentialCountText);

  featureEssentialNode.appendChild(featureEssentialCountNode);
  featureTrNode.appendChild(featureEssentialNode);

  const featureUsefulNode = document.createElement("td");
  featureUsefulNode.classList.add("feature-working");

  const featureUsefulCountNode = document.createElement("label");
  const visibleUsefulCount = feature.visible.useful.broken
    + feature.visible.useful.working;
  const hiddenUsefulCount = feature.hidden.useful.broken
    + feature.hidden.useful.working;
  const featureUsefulCountText
    = document.createTextNode(`${visibleUsefulCount}${hiddenUsefulCount
      ? `(+${hiddenUsefulCount})` : ""}/${totalCount}`);
  featureUsefulCountNode.appendChild(featureUsefulCountText);

  featureUsefulNode.appendChild(featureUsefulCountNode);
  featureTrNode.appendChild(featureUsefulNode);

  return featureTrNode;
};

const getTableHeaderNode = (title) => {
  const tableHeaderNode = document.createElement("th");
  const tableHeaderText = document.createTextNode(title);
  tableHeaderNode.appendChild(tableHeaderText);
  return tableHeaderNode;
};

const getFeatureTableNode = (featureList) => {
  const featureTableNode = document.createElement("table");
  featureTableNode.id = FEATURE_TABLE_ID;

  const tableHeaderNode = document.createElement("tr");

  tableHeaderNode.appendChild(getTableHeaderNode("Detected feature"));
  tableHeaderNode.appendChild(getTableHeaderNode("Broken?"));
  tableHeaderNode.appendChild(getTableHeaderNode("Working?"));
  tableHeaderNode.appendChild(getTableHeaderNode("Essential?"));
  tableHeaderNode.appendChild(getTableHeaderNode("Useful?"));

  featureTableNode.appendChild(tableHeaderNode);

  for (const [featureName, feature] of Object.entries(featureList)) {
    const totalCount = feature.visible.all.broken
      + feature.visible.all.working
      + feature.hidden.all.broken
      + feature.hidden.all.working;
    if (totalCount > 0) {
      featureTableNode.appendChild(getFeatureTableRow(featureName, feature));
    }
  }

  return featureTableNode;
};

const updateFeatureTable = (featureList) => {
  const oldFeatureTableNode = document.getElementById(FEATURE_TABLE_ID);

  const newFeatureTableNode = getFeatureTableNode(featureList);
  oldFeatureTableNode.parentNode.replaceChild(
    newFeatureTableNode,
    oldFeatureTableNode,
  );
};

const onMessageReceived = (message) => {
  console.log(message);

  getCurrentTab().then((tabs) => {
    const tabId = tabs[0].id;

    if (message.featureList && message.tabId === tabId) {
      updateFeatureTable(message.featureList);
    }
  });
};

browser.runtime.onMessage.addListener(onMessageReceived);

browser.runtime.sendMessage({ popupOpened: true });
