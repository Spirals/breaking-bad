"use strict";

window.HB = window.HB || {};
window.HB.heuristics = window.HB.heuristics || {};

const couldBeDisclosableElement = (element, disclosureButton) => {
  if (!window.HB.domUtils.isNativeHTMLElement(element)) {
    if (/\bMENU\b/.test(element.tagName)) {
      return true;
    }

    return element.parentElement.children.length === 2
      && Array.from(element.parentElement.children)
        .includes(disclosureButton);
  }

  return element
    && (element.tagName === "UL"
      || element.matches(`
          div[class~="collapse"],
          div[class~="menu"],
          div[class~="list"],
          div[class~="content"]`))
    && !window.HB.domUtils.isTextOnlyWhitespace(element.textContent);
};

const getCollapsableDisclosureElement = async (disclosureButton) => {
  const nextSibling = disclosureButton.nextElementSibling;

  if (nextSibling
    && (nextSibling.tagName === "DIV"
      || couldBeDisclosableElement(nextSibling, disclosureButton))
    && await window.HB.common.isSpecificallyHidden(nextSibling)) {
    return nextSibling;
  }

  const grandparent = disclosureButton.parentElement.parentElement;

  const candidates = [
    nextSibling,
    nextSibling && nextSibling.nextElementSibling,
    nextSibling && nextSibling.nextElementSibling
      && nextSibling.nextElementSibling.nextElementSibling,
    disclosureButton.parentElement.nextElementSibling,
    grandparent && grandparent.parentElement.children.length <= 2
      && grandparent.nextElementSibling,
  ];

  const collapsableElement = candidates.find((el) => el
    && couldBeDisclosableElement(el, disclosureButton));

  if (!collapsableElement) {
    return null;
  }

  // https://getbootstrap.com/docs/5.0/components/dropdowns/
  if (collapsableElement.matches('[class~="dropdown-menu"]')) {
    return collapsableElement;
  }

  if (!window.HB.domUtils.isNativeHTMLElement(collapsableElement)) {
    return collapsableElement;
  }

  if (!window.HB.heuristics.findFadeinElements().includes(collapsableElement)
    && await window.HB.common.isSpecificallyHidden(collapsableElement)) {
    return collapsableElement;
  }

  return null;
};
window.HB.heuristics.getCollapsableDisclosureElement
  = getCollapsableDisclosureElement;

const isDisclosableElementCSSOnlyTriggered = async (
  disclosableElement,
  pseudoClassCSSRule,
) => {
  const HIDDEN_VISIBLE_STYLES = window.HB.common.HIDDEN_VISIBLE_STYLES;
  const testCond = window.HB.common.testCond;

  const doesNotMakeVisible = Object.keys(HIDDEN_VISIBLE_STYLES)
    .every((property) => {
      const pseudoClassProperty = pseudoClassCSSRule.style
        .getPropertyValue(property);
      if (!pseudoClassProperty) {
        return false;
      }

      return HIDDEN_VISIBLE_STYLES[property].visible.some((v) => testCond(
        pseudoClassProperty,
        v.test,
        v.value,
        HIDDEN_VISIBLE_STYLES[property].unit,
      ));
    });

  if (doesNotMakeVisible) {
    return false;
  }

  return await window.HB.common.isSpecificallyHidden(disclosableElement);
};

const hasDisclosureButtonProperEventHandlers = async (button) => {
  const hasEventHandlerType = window.HB.common.hasEventHandlerType;

  return await hasEventHandlerType(button, "click")
    || await hasEventHandlerType(button, "mouseover")
    || await hasEventHandlerType(button, "mouseenter")
    || await hasEventHandlerType(button, "focusin")
    || await hasEventHandlerType(button, "focus");
};

const canDisclosureButtonOpen = async (button) => {
  if (button.matches("details > summary")) {
    // Native element
    return true;
  }

  const collapsableDisclosureElement
    = await getCollapsableDisclosureElement(button);

  if (!collapsableDisclosureElement) {
    return false;
  }

  // Assume that if the disclosure button has an event handler, it is used to
  // toggle the collapsable element
  if (await hasDisclosureButtonProperEventHandlers(button)) {
    return true;
  }

  const parent = button.parentElement;
  const parentHasProperEventHandlers
    = await hasDisclosureButtonProperEventHandlers(parent);

  // Assume that if the disclosure button parent has an event handler and only
  // one child (this disclosure button), it is used to toggle the collapsable
  // element
  if (parentHasProperEventHandlers
    && parent.children.length === 1) {
    return true;
  }

  // Assume that if the disclosure button parent has an event handler and
  // exactly two children, the disclosure button and the collapsable element, it
  // is used to toggle the collapsable element
  if (parentHasProperEventHandlers
    && parent.children.length === 2
    && parent.contains(collapsableDisclosureElement)) {
    return true;
  }

  const grandparent = parent.parentElement;
  const grandparentHasProperEventHandlers
    = await hasDisclosureButtonProperEventHandlers(grandparent);

  // Assume that if the disclosure button grandparent has an event handler and
  // only one child, it is used to toggle the collapsable
  // element
  if (grandparentHasProperEventHandlers
    && grandparent.children.length === 1) {
    return true;
  }

  if (button.tagName === "LABEL") {
    const checkboxId = button.htmlFor;
    if (checkboxId) {
      const checkbox = document.getElementById(checkboxId);
      const checkboxCheckedCSSRules
        = await window.HB.domUtils
          .getParentPseudoClassStyleRules(
            checkbox,
            collapsableDisclosureElement,
            "checked",
          );

      const checkboxCheckedCSSOnlyTriggeredMenu
        = await window.HB.utils.asyncSome(
          checkboxCheckedCSSRules,
          (checkboxCheckedCSSRule) => {
            return isDisclosableElementCSSOnlyTriggered(
              collapsableDisclosureElement,
              checkboxCheckedCSSRule,
            );
          },
        );

      if (checkboxCheckedCSSOnlyTriggeredMenu) {
        return true;
      }
    }
  }

  const pseudoClassCSSRules = await window.HB.domUtils
    .getParentPseudoClassStyleRules(
      button,
      collapsableDisclosureElement,
      "hover",
    );

  const pseudoClassCSSOnlyTriggeredMenu = await window.HB.utils.asyncSome(
    pseudoClassCSSRules,
    (pseudoClassCSSRule) => {
      return isDisclosableElementCSSOnlyTriggered(
        collapsableDisclosureElement,
        pseudoClassCSSRule,
      );
    },
  );

  if (pseudoClassCSSOnlyTriggeredMenu) {
    return true;
  }

  return false;
};

// TESTING
// https://bugzilla.mozilla.org/show_bug.cgi?id=1393022
// https://polymer-library.polymer-project.org/3.0/docs/about_30
// https://w3c.github.io/aria-practices/examples/disclosure/disclosure-faq.html
// https://w3c.github.io/aria-practices/examples/disclosure/disclosure-navigation.html
// https://www.cambridge.org/gb/cambridgeenglish/catalog/dictionaries
// https://shop.fairphone.com/de/fairphone-3-plus
// https://www.hohner.de/en/service/accordion/faqs
// https://developer.mozilla.org/en-US/docs/Web/API/HTMLImageElement
// https://portswigger.net/research/web-cache-entanglement
// https://www.filamentgroup.com/lab/js-web-fonts.html
// https://gss.github.io/
// https://www.cnet.com/news/youtubes-new-nocookie-feature-continues-to-serve-cookies/ (CORS stylesheets)
// https://fingerprintjs.com/
// https://salsa.debian.org/qa/distro-tracker
// https://blog.qualys.com/vulnerabilities-research/2021/01/26/cve-2021-3156-heap-based-buffer-overflow-in-sudo-baron-samedit
// https://www.drupal.org/
// https://html.spec.whatwg.org/multipage/form-control-infrastructure.html
// https://www.bleepingcomputer.com/news/security/researcher-hacks-over-35-tech-firms-in-novel-supply-chain-attack/
// https://www.codeavail.com/blog/tips-on-how-to-tackle-a-machine-learning-project-as-a-beginner/
// https://getbootstrap.com/docs/4.5/getting-started/introduction/
// https://wix.com
// https://kde.org/
// https://git-scm.com/docs/git-reflog
// https:/imdb.com
// https://www.npmjs.com/package/npm
// https://ebay.com
// https://web.archive.org/web/20210202082744/https://www.lycee-pothier.com/
// https://www.crunchbase.com/organization/
// https://seaborn.pydata.org
// submenus, detect this properly; and maybe fix this as well)
const findDisclosureButtons = async (options) => {
  if (options.useful) {
    options.useful = false;
    options.essential = false;
    return (await findDisclosureButtons(options))
      .filter((button) => !window.HB.common.isElementNotUseful(button));
  }

  if (options.essential) {
    return [];
  }

  if (options.broken) {
    return window.HB.utils.asyncFilter(
      await findDisclosureButtons({
        useful: options.useful,
        essential: options.essential,
      }),
      async (button) => {
        return !await canDisclosureButtonOpen(button);
      },
    );
  }

  const disclosureButtons = (await window.HB.utils.asyncFilter(
    Array.from(document.querySelectorAll(`
      h2, h3, h4, h5,
      a,
      li,
      li > span:first-child,
      button,
      label,
      header,
      div,
      [class~="dropdown-toggle"]`)),
    async (element) => {
      if (element.parentElement === document.body) {
        return false;
      }

      // Disclosure buttons with no JavaScript, using a label and a hidden
      // checkbox
      if (element.tagName === "LABEL") {
        const checkboxId = element.htmlFor;
        if (checkboxId) {
          const checkbox = document.getElementById(checkboxId);
          if (checkbox && checkbox.type === "checkbox"
            && getComputedStyle(checkbox).display === "none") {
            return true;
          }
        }
      }

      // https://getbootstrap.com/docs/5.0/components/dropdowns/
      if (element.matches(".dropdown-toggle")) {
        return true;
      }

      if (element.tagName === "DIV"
        && !element.matches(".collapsible")
        && element.childNodes[0]
        && element.childNodes[0].nodeName === "#text") {
        return false;
      }

      // The disclosure button must have a cursor pointer, no need to check the
      // cursor for anchor and button elements
      if (element.tagName !== "A"
        && element.tagName !== "BUTTON"
        && !(element.tagName === "INPUT" && element.type === "button")
        && !window.HB.domUtils.hasPointerCursor(element)) {
        // return false;
      }

      // On some pages, collapsable disclosure elements are not part of the DOM
      // when the element is hidden, it is added by JavaScript, so skip checking
      // if a collapsableElement is found
      if (["DIV", "A"].some((tagName) => element.tagName === tagName)
        && window.HB.common.hasCaret(element)) {
        return true;
      }

      // If the anchor links to a totally different page, it cannot be a
      // disclosure button, however websites could have a fallback link to a
      // subpath
      // if (element.tagName === "A") {
      //   const hrefAttr = element.getAttribute("href");
      //   // Anchors whose href attribute is just a hash can actually link to a
      //   // different page if the baseURI of the page is changed, but some sites
      //   // are broken, e.g. https://www.hohner.de/en/service/accordion/faqs
      //   if (hrefAttr
      //     && !hrefAttr.startsWith("#")
      //     && !window.HB.utils.linkToSubpath(
      //       window.location.href,
      //       element.href,
      //     )) {
      //     return false;
      //   }
      // }

      const collapsableElement = await getCollapsableDisclosureElement(element);

      // If the disclosure button is inside a form, then the
      // collapsable element must be inside a form as well
      if (collapsableElement) {
        if (element.matches("form :scope")
          && !collapsableElement.matches("form :scope")) {
          return false;
        }

        return true;
      }

      return false;
    },
  ))
    .concat(Array.from(document.querySelectorAll("details > summary")));

  return window.HB.domUtils.keepOnlyOutmostElements(disclosureButtons);
};
window.HB.heuristics.findDisclosureButtons = findDisclosureButtons;
