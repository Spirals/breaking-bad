"use strict";

window.HB = window.HB || {};
window.HB.domUtils = window.HB.domUtils || {};

const hasWordIn = (string, word) => {
  if (!string) {
    return false;
  }

  // Do not use \b because we do not want do match the underscore
  if (new RegExp(`(?<![A-Za-z])${word}(?![A-Za-z])`, "i").test(string)) {
    return true;
  }

  const lowerCaseWord = word.toLowerCase();

  if (new RegExp(`^${lowerCaseWord}(?![a-z])`).test(string)) {
    return true;
  }

  const capWord = `${lowerCaseWord[0].toUpperCase()}${lowerCaseWord.slice(1)}`;
  if (new RegExp(`(?<![a-z])${capWord}(?![a-z])`)
    .test(string)) {
    return true;
  }

  const upperCaseWord = word.toUpperCase();

  if (new RegExp(`${upperCaseWord}`).test(string)) {
    return true;
  }

  return false;
};

const isTextOnlyWhitespace = (text) => {
  return /^\s*$/.test(text);
};
window.HB.domUtils.isTextOnlyWhitespace = isTextOnlyWhitespace;

const isNativeHTMLElement = (element) => {
  // Some native elements directly inherit from HTMLElement
  return [
    "HEADER",
    "FOOTER",
    "SECTION",
    "ARTICLE",
    "NAV",
    "ASIDE",
    "MARK",
    "FIGURE",
    "FIGCAPTION",
    "MAIN",
    "ADDRESS",
    "DD",
    "DT",
    "ABBR",
    "B",
    "BDI",
    "BDO",
    "CITE",
    "CODE",
    "DFN",
    "EM",
    "I",
    "KBD",
    "RB",
    "RP",
    "RT",
    "RTC",
    "RUBY",
    "S",
    "SAMP",
    "SMALL",
    "STRONG",
    "SUB",
    "SUP",
    "U",
    "VAR",
    "WBR",
    "NOSCRIPT",
    "COL-GROUP",
    "SUMMARY",
  ].includes(element.tagName)
    || document.createElement(element.tagName).constructor !== HTMLElement;
};
window.HB.domUtils.isNativeHTMLElement = isNativeHTMLElement;

const isElementTargetedByPseudoClassSelector = (
  element,
  selectorText,
  pseudoClass,
) => {
  const pseudoClassSelector = pseudoClass ? `:${pseudoClass}` : "";

  const parentPseudoClassSelectors
    = Array.from(selectorText.matchAll(new RegExp(
      `,?\\s*([^,]*${pseudoClassSelector}[^,]*)`,
      "g",
    )))
      .map((m) => m[1]);

  if (!parentPseudoClassSelectors.length) {
    return false;
  }

  return parentPseudoClassSelectors
    .some((selector) => {
      let selectorWithNoPseudoClass = selector.replace(
        new RegExp(pseudoClassSelector, "g"),
        "",
      );
      if (selectorWithNoPseudoClass === "") {
        selectorWithNoPseudoClass = "*";
      }

      try {
        // Check that the element matches if the pseudo-class is not present in
        // the selector
        const matchesWithNoPseudoClass
          = element.matches(selectorWithNoPseudoClass);
        if (!matchesWithNoPseudoClass) {
          return false;
        }

        // Check that the element matches if the selector is sliced after the
        // last occurrence of the pseudo-class selector
        const pseudoClassSelectorIndex
          = selector.lastIndexOf(pseudoClassSelector);
        if (pseudoClassSelectorIndex < 0) {
          return false;
        }

        let elementSelectorSliced
          = selector.slice(0, pseudoClassSelectorIndex).replace(
            new RegExp(pseudoClassSelector, "g"),
            "",
          );
        if (elementSelectorSliced === "") {
          elementSelectorSliced = "*";
        }

        return element.matches(elementSelectorSliced);
      } catch(e) {
        return false;
      }
    });
};
window.HB.domUtils.isElementTargetedByPseudoClassSelector
  = isElementTargetedByPseudoClassSelector;

const wordAttributeSelectorRegexp
  = /\s*([^[,]*)(?:((?:\[[A-Za-z-]+[^_]?=[^\]]+\])*)(?:\[([A-Za-z-]+)_=["']?([^"'\]]+)["']?\])*)+([^,]*)/g;
const querySelectorAllExtra = (element, selectors) => {
  // Implement a custom attribute selector: [attr_=value], to match elements
  // based on whether a word is found in the specified attribute

  const m = Array.from(selectors.matchAll(wordAttributeSelectorRegexp))
    // Filter out empty matches
    .filter((ma) => Boolean(ma[0]));

  if (!m.length) {
    return [];
  }

  return Array.from(new Set(m.flatMap((ma) => {
    const firstElementQuery = `${ma[1] ? ma[1] : "*"}${ma[2]}`;
    const firstElements = element.querySelectorAll(firstElementQuery);

    const attr = ma[3];
    const word = ma[4];

    // No custom [_=] attribute selector found
    if (!attr || !word) {
      return Array.from(firstElements);
    }

    const wordMatchingElements = Array.from(firstElements).filter((el) => {
      const attrValue = el.getAttribute(attr);
      if (!attrValue) {
        return false;
      }

      return hasWordIn(attrValue, word);
    });

    const selectorRest = ma[5];
    if (!selectorRest) {
      return wordMatchingElements;
    }

    return wordMatchingElements.flatMap((el) => {
      // :scope:scope is a valid selector, equivalent to :scope
      return querySelectorAllExtra(el, `:scope${selectorRest}`);
    });
  })));
};
window.HB.domUtils.querySelectorAllExtra = querySelectorAllExtra;

const matchesExtra = (element, selectorString) => {
  // Splitting the matching in two pieces as done here seems to be at least
  // 30 % faster that calling querySelectorAllExtra and checking if the returned
  // list contains the element

  return querySelectorAllExtra(document, selectorString)
    .includes(element);
};
window.HB.domUtils.matchesExtra = matchesExtra;

const selectorsMatches = (selectors) => {
  return Array.from(document.querySelectorAll(selectors.match))
    .filter((element) => !selectors.exclude
    || !element.matches(selectors.exclude));
};
window.HB.domUtils.selectorsMatches = selectorsMatches;

const selectorsMatchesExtra = (selectors) => {
  return querySelectorAllExtra(document, selectors.match)
    .filter((element) => !matchesExtra(element, selectors.exclude));
};
window.HB.domUtils.selectorsMatchesExtra = selectorsMatchesExtra;

const keepOnlyOutmostElements = (elements) => {
  return elements.filter((element) => {
    return !elements.some((otherElement) => otherElement !== element
      && otherElement.contains(element));
  });
};
window.HB.domUtils.keepOnlyOutmostElements = keepOnlyOutmostElements;

const keepOnlyInmostElements = (elements) => {
  return elements.filter((element) => {
    return !elements.some((otherElement) => otherElement !== element
      && element.contains(otherElement));
  });
};
window.HB.domUtils.keepOnlyInmostElements = keepOnlyInmostElements;

const getCSSRulesFromDownloadedStylesheet = async (styleSheetURL) => {
  let cssText = "";
  try {
    cssText = await (await fetch(
      styleSheetURL,
      { cache: "force-cache" },
    )).text();
  } catch(_) {
    // The fetch can fail because of the CSP or CORS
    console.log(styleSheetURL);
    return [];
  }

  // Write the stylesheet contents to a <style> elements so we read
  // the parsed stylesheet
  const styleElement = document.createElement("style");
  styleElement.textContent = cssText;
  document.body.appendChild(styleElement);

  try {
    return await new Promise((resolve, reject) => {
      // Wait for the style element to be ready
      setTimeout(() => {
        // Read the just-added stylesheet
        try {
          resolve(Array.from(styleElement.sheet.cssRules));
        } catch {
          reject(new Error());
        } finally {
          styleElement.remove();
        }
      }, 10);
    });
  } catch {
    console.log(styleSheetURL);
    return [];
  }
};

const getStylesheetCSSRules = async (styleSheet) => {
  // Skip font-only stylesheets
  if (/^https?:\/\/fonts?\./.test(styleSheet.href)) {
    return [];
  }

  try {
    return Array.from(styleSheet.cssRules);
  } catch(e) {
    // Access to styleSheets is not possible for CORS styleSheets
    // https://bugzilla.mozilla.org/show_bug.cgi?id=1393022
    // instead, fetch and inject the stylesheet ourself so it is not
    // cross-origin anymore
    if (e.name === "SecurityError") {
      console.log("CORS stylesheet", styleSheet.href);

      return await getCSSRulesFromDownloadedStylesheet(styleSheet.href);
    }

    return [];
  }
};

const extractCSSStyleRules = (cssRules) => {
  return cssRules.flatMap((cssRule) => {
    if (cssRule instanceof CSSStyleRule) {
      return [cssRule];
    }

    if (cssRule instanceof CSSMediaRule
      && cssRule.cssRules
      && window.matchMedia(cssRule.conditionText).matches) {
      return Array.from(cssRule.cssRules)
        .flatMap((innerCssRule) => {
          if (innerCssRule instanceof CSSStyleRule) {
            return [innerCssRule];
          }

          return [];
        });
    }

    return [];
  });
};

const getActiveDocumentCSSStyleRules = async () => {
  return await window.HB._cacheStore.getOrSet(
    "activeDocumentCSSStyleRules",
    () => {
      const styleSheets = Array.from(document.styleSheets);

      return window.HB.utils.asyncFlatMap(styleSheets, async (styleSheet) => {
        if (!window.matchMedia(styleSheet.media.mediaText).matches) {
          return [];
        }

        let cssRules = await getStylesheetCSSRules(styleSheet);

        // @import stylesheets are not directly part of document.styleSheets
        cssRules = await window.HB.utils.asyncFlatMap(
          cssRules,
          async (cssRule) => {
            if (cssRule instanceof CSSImportRule) {
              if (!window.matchMedia(cssRule.media.mediaText).matches) {
                return [];
              }

              if (/^https?:\/\/fonts?\./.test(cssRule.styleSheet.href)) {
                return [];
              }

              return [cssRule]
                .concat(await getStylesheetCSSRules(cssRule.styleSheet));
            }

            return [cssRule];
          },
        );

        return extractCSSStyleRules(cssRules);
      });
    },
  );
};
window.HB.domUtils.getActiveDocumentCSSStyleRules
  = getActiveDocumentCSSStyleRules;

const getElementMatchingStyleRules = async (element, pseudoClass) => {
  const cssRules = await getActiveDocumentCSSStyleRules();

  if (pseudoClass) {
    return cssRules.filter((cssRule) => {
      try {
        return isElementTargetedByPseudoClassSelector(
          element,
          cssRule.selectorText,
          pseudoClass,
        );
      } catch(e) {
        return false;
      }
    });
  }

  return cssRules.filter((cssRule) => {
    try {
      return element.matches(cssRule.selectorText);
    } catch(e) {
      return false;
    }
  });
};
window.HB.domUtils.getElementMatchingStyleRules = getElementMatchingStyleRules;

// Get CSS rules matching the button in
// button:hover .dropdown-menu
// using getParentPseudoClassStyleRules(theButton, theDropdownMenu, "hover")
const getParentPseudoClassStyleRules = async (
  targetedElement,
  matchingElement,
  pseudoClass,
) => {
  const pseudoClassSelector = `:${pseudoClass}`;

  const documentPseudoClassCSSRules = await window.HB._cacheStore
    .getOrSet(`activeDocumentCSSStyleRules_${pseudoClass}`, async () => {
      return (await window.HB.domUtils.getActiveDocumentCSSStyleRules())
        .filter((cssRule) => {
          const selectorText = cssRule.selectorText;
          if (!selectorText) {
            return false;
          }
          return new RegExp(`${pseudoClassSelector}\\b`).test(selectorText);
        });
    });

  const pseudoClassElementsStyleRules = documentPseudoClassCSSRules
    .map((cssRule) => {
      const selectorText = cssRule.selectorText;
      if (!selectorText) {
        return null;
      }

      const parentPseudoClassSelectors
        = Array.from(selectorText.matchAll(new RegExp(
          `,?\\s*([^,]*${pseudoClassSelector}[^,]*)`,
          "g",
        )))
          .map((m) => m[1]);

      if (!parentPseudoClassSelectors.length) {
        return null;
      }

      const matchesMatchingElement = parentPseudoClassSelectors
        .some((selector) => {
          let matchingElementQueryString = selector.replace(new RegExp(
            `:${pseudoClass}`,
            "g",
          ), "");
          if (matchingElementQueryString === "") {
            matchingElementQueryString = "*";
          }

          try {
            // Check that the matching element matches if the pseudo-class is
            // not present in the selector
            const matchingElementMatches
              = matchingElement.matches(matchingElementQueryString);
            if (!matchingElementMatches) {
              return false;
            }

            // Check that the target element is contained in an element that
            // matches if the selector is sliced after the last occurrence of
            // the pseudo-class selector
            const pseudoClassSelectorIndex
              = selector.lastIndexOf(pseudoClassSelector);

            if (pseudoClassSelectorIndex < 0) {
              return false;
            }

            let targetElementQueryString
              = selector.slice(0, pseudoClassSelectorIndex).replace(
                new RegExp(pseudoClassSelector, "g"),
                "",
              );
            if (targetElementQueryString === "") {
              targetElementQueryString = "*";
            }

            const pseudoClassElementContainsTargetElement
              = Array.from(document.querySelectorAll(targetElementQueryString))
                .some((el) => {
                  return el.contains(targetedElement);
                });
            return pseudoClassElementContainsTargetElement;
          } catch(e) {
            return false;
          }
        });

      if (!matchesMatchingElement) {
        return null;
      }

      return cssRule;
    }).filter((s) => Boolean(s));

  return pseudoClassElementsStyleRules;
};
window.HB.domUtils.getParentPseudoClassStyleRules
  = getParentPseudoClassStyleRules;

const getBackgroundImageURLs = (elementStyle) => {
  const backgroundImage = elementStyle.backgroundImage;

  if (!backgroundImage || backgroundImage === "none") {
    return [];
  }

  const urlStrings = /url\(["']?([^)]*)["']?\)/.exec(backgroundImage);

  if (!urlStrings) {
    return [];
  }

  return urlStrings.slice(1)
    .filter((urlString) => Boolean(urlString))
    // The computed value makes <url> values absolute
    .map((urlString) => urlString && new URL(urlString));
};
window.HB.domUtils.getBackgroundImageURLs = getBackgroundImageURLs;

const hasPointerCursor = (element) => {
  // Make sure the cursor value is not inherited
  return getComputedStyle(element).cursor === "pointer"
    && getComputedStyle(element.parentElement).cursor !== "pointer";
};
window.HB.domUtils.hasPointerCursor = hasPointerCursor;

const TRANSPARENT_RGBA_COLOR = "rgba(0, 0, 0, 0)";
window.HB.domUtils.TRANSPARENT_RGBA_COLOR = TRANSPARENT_RGBA_COLOR;
const isColorInitialValue = (colorString) => {
  return colorString === "transparent"
    || colorString === TRANSPARENT_RGBA_COLOR;
};
window.HB.domUtils.isColorInitialValue = isColorInitialValue;

const getAncestorBackgroundColor = (element) => {
  while (element && element !== document.body) {
    element = element.parentElement;

    const backgroundColor = getComputedStyle(element).backgroundColor;
    if (backgroundColor
      && !window.HB.domUtils.isColorInitialValue(backgroundColor)) {
      return backgroundColor;
    }
  }

  return window.HB.domUtils.TRANSPARENT_RGBA_COLOR;
};

const hasDifferentBackgroundColorThanAncestor = (element) => {
  const elementBackgroundColor = getComputedStyle(element).backgroundColor;
  if (!elementBackgroundColor
    || window.HB.domUtils.isColorInitialValue(elementBackgroundColor)) {
    return false;
  }

  const ancestorBackgroundColor = getAncestorBackgroundColor(element);
  return elementBackgroundColor !== ancestorBackgroundColor;
};
window.HB.domUtils.hasDifferentBackgroundColorThanAncestor
  = hasDifferentBackgroundColorThanAncestor;
