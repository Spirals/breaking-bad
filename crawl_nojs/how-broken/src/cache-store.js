"use strict";

window.HB = window.HB || {};

class CacheStore {
  constructor() {
    this.invalidateAll();
  }

  set(key, value) {
    return this._store[key] = value;
  }

  get(key) {
    return this._store[key];
  }

  getOrSet(key, fn) {
    const value = this.get(key);

    if (value) {
      return value;
    }

    return this.set(key, fn());
  }

  has(key) {
    return Boolean(this.get(key));
  }

  invalidate(key) {
    if (this.has(key)) {
      this._store[key] = null;
      return true;
    }

    return false;
  }

  invalidateAll() {
    this._store = {};
  }
}
window.HB._cacheStore = new CacheStore();
