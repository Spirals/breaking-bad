"use strict";

window.HB = window.HB || {};
window.HB.heuristics = window.HB.heuristics || {};

const isButtonLike = (element) => {
  return element.matches(`
    button,
    input[type="button"],
    input[type="image"],
    input[type="reset"],
    input[type="submit"]
  `);
};

const isSearchInput = (element) => {
  if (element.name === "q") {
    return true;
  }

  return element.matches(`
    input[type="search"],
    input[class_="search"],
    input[id_="search"],
    input[placeholder_="search"]`);
};

// TESTING
// https://www.mightynetworks.com/pricing
// https://www.microsoft.com
// https://devdocs.io/
const findLoneControls = async (options) => {
  if (options.useful) {
    options.useful = false;
    options.essential = false;
    return (await findLoneControls(options))
      .filter((control) => {
        return !window.HB.common.isElementNotUseful(control);
      });
  }

  if (options.essential) {
    options.useful = false;
    options.essential = false;
    return (await findLoneControls(options))
      .filter((control) => {
        return !window.HB.common.isElementNotUseful(control)
          && window.HB.common.isElementInMainSection(control);
      });
  }

  if (options.broken) {
    return window.HB.utils.asyncFilter(
      await findLoneControls({
        useful: options.useful,
        essential: options.essential,
      }),
      async (control) => {
        if (control.type === "hidden") {
          return false;
        }

        const hasEventHandlerType = window.HB.common.hasEventHandlerType;

        if (isButtonLike(control)) {
          const hasClickEventHandler
            = await hasEventHandlerType(control, "click");
          // The button is not broken if it has a click event handler attached
          if (hasClickEventHandler) {
            return false;
          }

          const parentElement = control.parentElement;
          // If the button has no event listener and its parent has at least
          // another element, we cannot be sure that an event handler from the
          // parent would handle the button event
          if (parentElement.children.length > 1) {
            return true;
          }

          // If the parent only has a single child (this control), we assume
          // that the event handler, if present, is there to handle the button
          // click event
          return !await hasEventHandlerType(parentElement, "click");
        }

        // By default the control is not considered broken, as it can simply be
        // read in JavaScript
        return false;
      },
    );
  }

  return window.HB.domUtils.selectorsMatches({
    match: "button, input, select, textarea",
    exclude: "form :scope",
  })
    // Exclude form-associated controls
    .filter((control) => !control.form);
};
window.HB.heuristics.findLoneControls = findLoneControls;

// https://html.spec.whatwg.org/multipage/browsing-the-web.html#the-indicated-part-of-the-document
const isTopIndicatingAnchor = (anchor, onlyExplicitTopHash) => {
  if (!window.HB.utils.linkToSamePage(anchor.href, window.location.href)) {
    return false;
  }

  try {
    const hash = new URL(anchor.href).hash;

    // https://html.spec.whatwg.org/multipage/browsing-the-web.html#the-indicated-part-of-the-document
    if (hash === "#top" || !onlyExplicitTopHash && hash === "") {
      return true;
    }
  } catch {
    //
  }

  return false;
};

const findGoToTopAnchorButtons = (options) => {
  if (options.useful) {
    return [];
  }

  if (options.essential) {
    return [];
  }

  if (options.broken) {
    return findGoToTopAnchorButtons({
      useful: options.useful,
      essential: options.essential,
    })
      .filter((anchor) => {
        if (isTopIndicatingAnchor(anchor)) {
          return false;
        }

        return true;
      });
  }

  // TODO: implement :has() and use this instead
  // const goToTopAnchorButtons
  //   = document.querySelectorAll('a[href="#top"], a:has(:is(i, span)[class*="-up"])');
  // https://html.spec.whatwg.org/multipage/browsing-the-web.html#the-indicated-part-of-the-document
  const goToTopAnchorButtons
      = Array.from(document.querySelectorAll("a"))
        .filter((anchor) => {
          return isTopIndicatingAnchor(anchor, true)
          // TODO: use :is() when it is supported by jsdom
            || anchor.querySelectorAll(`
              i[class*="-up"]:not([class*="-down"]),
              span[class*="-up"]:not([class*="-down"])`).length > 0;
        });
  return goToTopAnchorButtons;
};
window.HB.heuristics.findGoToTopAnchorButtons = findGoToTopAnchorButtons;

// TESTING
// https://salsa.debian.org/qa/distro-tracker (the 'Verified' button has a
// dynamically added focusin event handler)
// https://azeria-labs.com/arm-data-types-and-registers-part-2/ (mobile)
// https://www.vem-editor.org/
const findEmptyAnchorButtons = async (options) => {
  if (options.useful) {
    options.useful = false;
    options.essential = false;
    return (await findEmptyAnchorButtons(options))
      .filter((control) => {
        return !window.HB.common.isElementNotUseful(control);
      });
  }

  if (options.essential) {
    options.essential = false;
    return (await findEmptyAnchorButtons(options))
      .filter((anchor) => {
        return !window.HB.common.isElementNotUseful(anchor)
          && window.HB.common.isElementInMainSection(anchor);
      });
  }

  if (options.broken) {
    return window.HB.utils.asyncFilter(
      await findEmptyAnchorButtons({
        useful: options.useful,
        essential: options.essential,
      }),
      async (anchor) => {
        if (findGoToTopAnchorButtons({}).includes(anchor)) {
          return findGoToTopAnchorButtons({ broken: true }).includes(anchor);
        }

        const hasClickEventHandler
          = await window.HB.common.hasEventHandlerType(anchor, "click");
        const hasFocusinEventHandler
          = await window.HB.common.hasEventHandlerType(anchor, "focusin");
        return !hasClickEventHandler && !hasFocusinEventHandler;
      },
    );
  }

  // An anchor element with no href attribute but a name attribute is an
  // obsolete, HTML4 way of marking a destination for another anchor
  return window.HB.domUtils.selectorsMatches({
    match: `
      a:not([href]):not([name]):not([id]),
      a[href=""],
      a[href="#"],
      a[href="javascript:"],
      a[href="javascript:;"],
      a[href^="javascript:void(0)"],
      a[href^="javascript: void(0)"]`,
    exclude: "body > a:first-child",
  });
};
window.HB.heuristics.findEmptyAnchorButtons = findEmptyAnchorButtons;

// TESTING
// https://www.digitalocean.com/community/tutorials/how-to-use-sftp-to-securely-transfer-files-with-a-remote-server
const findLanguageSelectInputs = async (options) => {
  // All language select are useful

  if (options.essential) {
    return [];
  }

  if (options.broken) {
    return window.HB.utils.asyncFilter(
      await findLanguageSelectInputs({
        useful: options.useful,
        essential: options.essential,
      }),
      async (select) => {
        // If the select element is part of or associated to a form that can
        // submit, it is considered not broken
        if (select.form && window.HB.heuristics.canFormSubmit(select.form)) {
          return false;
        }

        const hasEventHandlerType = window.HB.common.hasEventHandlerType;

        return !await hasEventHandlerType(select, "change")
          && !await hasEventHandlerType(select, "input");
      },
    );
  }

  return Array.from(document.querySelectorAll(`
    select[id*="language"],
    select[class*="language"]`));
};
window.HB.heuristics.findLanguageSelectInputs = findLanguageSelectInputs;

// TESTING
// https://www.tenorshare.com/android-data/nexus-stuck-in-bootloop-how-to-fix.html
// https://semantic-ui.com/modules/rating.html
const findInteractiveRatings = (options) => {
  const ratingElements = window.HB.domUtils.selectorsMatchesExtra({
    match: 'div[class_="rating"]',
    exclude: "",
  });
  return ratingElements;
};
window.HB.heuristics.findInteractiveRatings = findInteractiveRatings;

const findPageDelegatedClickEvents = (options) => {
  if (options.broken) {
    return [];
  }

  return window.HB.utils.asyncFilter(
    Array.from(document.querySelectorAll("html, body")),
    async (el) => {
      const hasEventHandlerType = window.HB.common.hasEventHandlerType;
      return await hasEventHandlerType(el, "click");
    },
  );
};
window.HB.heuristics.findPageDelegatedClickEvents
  = findPageDelegatedClickEvents;

// TESTING
// https://drafts.csswg.org/selectors-4/#conform-partial
const findMislinkedFragmentAnchors = async (options) => {
  if (options.useful) {
    options.useful = false;
    options.essential = false;
    return (await findMislinkedFragmentAnchors(options))
      .filter((control) => {
        return !window.HB.common.isElementNotUseful(control);
      });
  }

  if (options.essential) {
    options.essential = false;
    return (await findMislinkedFragmentAnchors(options))
      .filter((anchor) => {
        return !window.HB.common.isElementNotUseful(anchor)
          && window.HB.common.isElementInMainSection(anchor);
      });
  }

  if (options.broken) {
    return window.HB.utils.asyncFilter(
      await findMislinkedFragmentAnchors({
        useful: options.useful,
        essential: options.essential,
      }),
      async (anchor) => {
        const hasClickEventHandler
          = await window.HB.common.hasEventHandlerType(anchor, "click");
        const hasFocusinEventHandler
          = await window.HB.common.hasEventHandlerType(anchor, "focusin");

        return !hasClickEventHandler && !hasFocusinEventHandler;
      },
    );
  }

  // An anchor element with no href attribute but a name attribute is an
  // obsolete, HTML4 way of marking a destination for another anchor
  return Array.from(document.querySelectorAll('a[href^="#"]:not([href="#"])'))
    .filter((anchor) => {
      try {
        const hash = decodeURI(new URL(anchor.href).hash);
        if (!hash) {
          return false;
        }

        // Remove the leading hash symbol
        const id = hash.slice(1);
        console.log(id);

        return id
          && id !== "top"
          && !document.getElementById(id)
        // Obsolete, HTML4 way of defining a target location
          && !document.querySelector(`a[name="${id}"]`);
      } catch {
        return false;
      }
    });
};
window.HB.heuristics.findMislinkedFragmentAnchors
  = findMislinkedFragmentAnchors;
