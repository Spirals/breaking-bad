"use strict";

const featureLists = [];

const computeBreakageLevel = (featureList) => {
  const breakageLevel = Math.max(...Object.entries(window.HB.FEATURE_DATA)
    .map(([featureName, featureData]) => {
      const featureDetected = featureList[featureName];

      if (featureDetected
        && featureDetected.visible.useful.broken > 0) {
        return featureData.breakageLevel;
      }

      return 0;
    }));

  return breakageLevel;
};

const updateBadge = (tabId, breakageLevel) => {
  console.log(tabId);
  browser.browserAction.setBadgeText({
    text: breakageLevel.toString() + "%",
    tabId,
  });

  const BADGE_BACKGROUND_COLOR_OK = [85, 170, 255, 255];
  const BADGE_BACKGROUND_COLOR_BROKEN = [255, 170, 0, 255];
  const BADGE_BACKGROUND_COLOR_VERY_BROKEN = [200, 0, 0, 255];

  const THRESHOLD_VERY_BROKEN = 75;
  const THRESHOLD_BROKEN = 5;

  let color = BADGE_BACKGROUND_COLOR_OK;

  if (breakageLevel >= THRESHOLD_VERY_BROKEN) {
    color = BADGE_BACKGROUND_COLOR_VERY_BROKEN;
  } else {
    if (breakageLevel >= THRESHOLD_BROKEN) {
      color = BADGE_BACKGROUND_COLOR_BROKEN;
    }
  }

  browser.browserAction.setBadgeBackgroundColor({
    color,
    tabId,
  });
};

const sendFeatureList = (tabId) => {
  const featureList = featureLists[tabId];
  browser.runtime.sendMessage({ tabId, featureList })
    .catch((err) => {
      console.error(err);
    });
};

const getCurrentTab = () => {
  return browser.tabs.query({
    active: true,
    windowId: browser.windows.WINDOW_ID_CURRENT,
  });
};

const onFeatureListReceived = (tabId, featureList) => {
  featureLists[tabId] = featureList;

  sendFeatureList(tabId);

  const breakageLevel = computeBreakageLevel(featureList);
  updateBadge(tabId, breakageLevel);
};

const onMessageReceived = (message, sender) => {
  console.log(message);

  if (message.insertCSS) {
    getCurrentTab().then((tabs) => {
      const tabId = tabs[0].id;

      browser.tabs.insertCSS(
        tabId,
        { file: "/src/highlight-page-feature.css" },
      );
    });
  }

  if (message.popupOpened) {
    getCurrentTab().then((tabs) => {
      const tabId = tabs[0].id;

      browser.tabs.sendMessage(
        tabId,
        { inspectPage: true },
      );

      sendFeatureList(tabId);
    });
    return;
  }

  const tabId = sender.tab.id;

  if (message.featureList) {
    onFeatureListReceived(tabId, message.featureList);
  }
};

browser.runtime.onMessage.addListener(onMessageReceived);
