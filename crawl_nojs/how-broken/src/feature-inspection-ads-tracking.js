"use strict";

window.HB = window.HB || {};
window.HB.heuristics = window.HB.heuristics || {};

// TESTING
// https://null-byte.wonderhowto.com/how-to/hackers-use-hidden-data-airline-boarding-passes-hack-flights-0180728/
// https://eslint.org/docs/rules/
const findInPageAds = (options) => {
  if (options.useful || options.essential) {
    return [];
  }

  if (options.broken) {
    return findInPageAds({
      useful: options.useful,
      essential: options.essential,
    });
  }

  const ads = window.HB.domUtils
    .keepOnlyOutmostElements(window.HB.domUtils
      .querySelectorAllExtra(
        document,
        `
        div[class_="ad"],
        div[class_="ads"]`,
      ));

  return ads;
};
window.HB.heuristics.findInPageAds = findInPageAds;

const ANALYTICS_REGEXS = [
  /analytics/i,
  /piwik/i,
  /matomo/i,
  /tagmanager/i,
  /tagcommander/i,
  /hs-script/i,
  /tracker/i,
];

// TESTING
// https://tracker.debian.org/pkg/firefox (should NOT match)
// https://arpitbhayani.me/blogs/slowsort
// https://www.openstreetmap.org
// https://www.w3.org/WAI/standards-guidelines/aria/
const findAnalyticsScripts = (options) => {
  if (options.useful || options.essential) {
    return [];
  }

  if (options.broken) {
    return findAnalyticsScripts({
      useful: options.useful,
      essential: options.essential,
    });
  }

  return Array.from(document.scripts)
    .filter((script) => script.type !== "application/json")
    .filter((script) => {
      return ANALYTICS_REGEXS
        .some((analyticsRegex) => {
          if (script.src) {
            return analyticsRegex.test(script.src);
          }

          return analyticsRegex.test(script.textContent);
        });
    });
};
window.HB.heuristics.findAnalyticsScripts = findAnalyticsScripts;

const findAnalyticsIFrames = (options) => {
  if (options.useful || options.essential) {
    return [];
  }

  if (options.broken) {
    return findAnalyticsIFrames({
      useful: options.useful,
      essential: options.essential,
    });
  }

  return Array.from(document.getElementsByTagName("iframe"))
    .filter((iframe) => {
      return ANALYTICS_REGEXS
        .some((analyticsRegex) => analyticsRegex.test(iframe.src));
    });
};
window.HB.heuristics.findAnalyticsIFrames = findAnalyticsIFrames;

const findPingAnchors = (options) => {
  if (options.useful || options.essential) {
    return [];
  }

  if (options.broken) {
    return findPingAnchors({
      useful: options.useful,
      essential: options.essential,
    });
  }

  return Array.from(document.querySelectorAll("a[ping]"));
};
window.HB.heuristics.findPingAnchors = findPingAnchors;
