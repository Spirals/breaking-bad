"use strict";

window.HB = window.HB || {};
window.HB.heuristics = window.HB.heuristics || {};

// https://hg.mozilla.org/mozilla-central/file
// https://developpaper.com/understanding-js-shallow-copy-and-deep-copy/
const isSearchForm = (form) => {
  const searchForms = window.HB._cacheStore.getOrSet(
    "searchForms",
    () => {
      return window.HB.domUtils.querySelectorAllExtra(
        document,
        `
      form[role="search"],
      form[id_="search"],
      form[action*="/search/"],
      [class_="search"] form
      `,
      );
    },
  );
  return searchForms.includes(form);
};

const isEmailSubscriptionForm = (form) => {
  const emailSubscriptionForms = window.HB._cacheStore.getOrSet(
    "emailSubscriptionForms",
    () => {
      return window.HB.domUtils.querySelectorAllExtra(
        document,
        `
      form[class_="newsletter"],
      form[class_="subscribe"],
      form[class_="email"],
      form[action*="/subscribe"],
      form[name*="/subscribe"]
      `,
      );
    },
  );
  return emailSubscriptionForms.includes(form);
};

const isCommentForm = (form) => {
  const commentForms = window.HB._cacheStore.getOrSet(
    "commentForms",
    () => {
      return window.HB.domUtils.querySelectorAllExtra(
        document,
        `
      form[id*="comment"],
      form[class_="comment"],
      form[action_="comment"],
      form[action_="comments"]
      `,
      );
    },
  );
  return commentForms.includes(form);
};

const getFormSubmitButtons = (form) => {
  return Array.from(form.elements)
    .filter((element) => {
      return element.tagName === "BUTTON" && element.type === "submit"
        || element.tagName === "INPUT" && element.type === "submit";
    });
};

// https://developer.mozilla.org/en-US/docs/Web/HTML/Element/output
const canFormSubmit = (form) => {
  // Using form.elements makes it easy to handle form-associated elements,
  // including form-associated custom elements
  // https://html.spec.whatwg.org/multipage/custom-elements.html#form-associated-custom-element

  // https://html.spec.whatwg.org/multipage/form-control-infrastructure.html#default-button
  const defaultButton = getFormSubmitButtons(form)[0];

  if (defaultButton) {
    if (defaultButton.disabled) {
      return false;
    }

    return true;
  }

  // Firefox list of single line text controls, this changes when widgets are
  // implemented for some of these
  // https://searchfox.org/mozilla-central/rev/7067bbd8194f4346ec59d77c33cd88f06763e090/dom/html/nsIFormControl.h#258
  const FIREFOX_SINGLE_LINE_TEXT_CONTROLS = [
    "datetime-local",
    "email",
    "month",
    "number",
    "password",
    "search",
    "text",
    "tel",
    "url",
    "week",
  ];

  const hasImplicitSubmissionTrigerringControl = Array.from(form.elements)
    .some((element) => {
      // https://searchfox.org/mozilla-central/rev/7067bbd8194f4346ec59d77c33cd88f06763e090/dom/html/HTMLInputElement.cpp#3872
      return element.tagName === "INPUT"
        && FIREFOX_SINGLE_LINE_TEXT_CONTROLS.includes(element.type);
    });

  if (!hasImplicitSubmissionTrigerringControl) {
    return false;
  }

  const implicitSubmissionBlockingFields = Array.from(form.elements)
    .filter((input) => {
      // The HTML specification lists the implicit submission blocking fields
      // https://html.spec.whatwg.org/multipage/form-control-infrastructure.html#implicit-submission
      // (textarea and select elements do not block implicit submission), but
      // Firefox actually uses a different list
      // https://searchfox.org/mozilla-central/rev/7067bbd8194f4346ec59d77c33cd88f06763e090/dom/html/HTMLFormElement.cpp#1797
      return input.tagName === "INPUT"
        && FIREFOX_SINGLE_LINE_TEXT_CONTROLS.includes(input.type);
    });

  return implicitSubmissionBlockingFields.length <= 1;
};
window.HB.heuristics.canFormSubmit = canFormSubmit;

// TESTING
// https://www.cypress.io/
// https://wiki.lineageos.org/devices/FP3
// https://salsa.debian.org/qa/distro-tracker
// https://github.com/jsdom/jsdom/
// https://bugzilla.mozilla.org/show_bug.cgi?id=1393022
// https://www.codeavail.com/blog/tips-on-how-to-tackle-a-machine-learning-project-as-a-beginner/
const findForms = async (options) => {
  // All forms are considered useful

  if (options.essential) {
    options.useful = false;
    options.essential = false;
    return (await findForms(options))
      .filter((form) => {
        // If the form has an autofocused elemement, it is considered essential
        if (Array.from(form.elements).some((element) => element.autofocus)) {
          return true;
        }

        return !window.HB.common.isElementNotUseful(form)
          && window.HB.common.isElementInMainSection(form)
          && !isEmailSubscriptionForm(form)
          && !isCommentForm(form);
      });
  }

  if (options.broken) {
    return window.HB.utils.asyncFilter(
      await findForms({
        useful: options.useful,
        essential: options.essential,
      }),
      async (form) => {
        const everyMeaningfulInputHaveName = Array.from(form.elements)
          .every((element) => {
            // The element may not have a name if it is hidden, this is usually
            // used for CSRF tokens
            // e.g. https://github.com/jsdom/jsdom/
            if (element.type === "hidden") {
              return true;
            }

            // Button-like inputs do not have to have a name attribute to make
            // sense, even if they are technically submittable (their value is
            // indeed transmitted, but the button may only be used to trigger
            // the form submission)
            // https://html.spec.whatwg.org/multipage/forms.html#category-submit
            // The image button type is not part of form.elements, see
            // https://html.spec.whatwg.org/multipage/forms.html#dom-form-elements
            if (element.tagName === "INPUT"
              && ["submit", "button", "reset"]
                .includes(element.type)) {
              return true;
            }

            // If these elements do not have a name attribute, they are broken
            // since they cannot be sent with the form
            if (["INPUT", "TEXTAREA", "SELECT"].includes(element.tagName)) {
              return Boolean(element.name);
            }

            return true;
          });

        // If every input that should have a name has a name and if this the
        // form can be submitted, the form is considered not broken
        if (everyMeaningfulInputHaveName && canFormSubmit(form)) {
          return false;
        }

        const hasEventHandlerType = window.HB.common.hasEventHandlerType;

        // If the form has a submit or a click event handler attached, assume
        // the event handler will handle the form properly
        if (await hasEventHandlerType(form, "submit")
          || await hasEventHandlerType(form, "click")) {
          return false;
        }

        const formButtons = Array.from(form.elements)
          .filter((element) => {
            return element.tagName === "BUTTON" && element.type !== "reset"
              || element.tagName === "INPUT"
                && ["button", "submit"].includes(element.type);
          });

        // If every button has a click event handler attached, assume the event
        // handlers will handle the form properly
        if (formButtons.length > 0 && await window.HB.utils.asyncEvery(
          formButtons,
          async (button) => {
            return await hasEventHandlerType(button, "click");
          },
        )) {
          return false;
        }

        // If the form has a single input, of type text or search, and if it has
        // an input event handler attached, assume it will handle the input
        // commit
        const firstElement = form.elements[0];
        if (form.elements.length === 1
          && firstElement
          && firstElement.tagName === "INPUT"
          && ["text", "search"].includes(firstElement.type)
          && await hasEventHandlerType(firstElement, "input")) {
          return false;
        }

        // Otherwise, the form is considered broken
        return true;
      },
    );
  }

  return Array.from(document.getElementsByTagName("form"));
};
window.HB.heuristics.findForms = findForms;
