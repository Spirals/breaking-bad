"use strict";

console.log("spy on event listeners – start");

(function () {
  const secretEventName = new Uint8Array(8);
  window.crypto.getRandomValues(secretEventName);

  const CLASS_PREFIX = "howBroken";
  const EVENT_PREFIX = "-event-";

  const eventHandlerList = {};

  window.HB = window.HB || {};
  window.HB._getElementEventHandlers = (element) => {
    const classAttr = element.getAttribute("class");

    // If no event handler has been detected on this element
    if (!classAttr) {
      return [];
    }

    const classesList = classAttr.split(" ");
    const customIdClasses = classesList
      .filter((className) => {
        return className.startsWith(`${CLASS_PREFIX}${EVENT_PREFIX}`);
      });

    // If no event handler has been detected on this element
    if (customIdClasses.length === 0) {
      return [];
    }

    const eventHandlers = Object.entries(eventHandlerList)
      .filter(([customIdClass]) => customIdClasses.includes(customIdClass))
      .map(([, eventHandler]) => eventHandler);
    return eventHandlers;
  };

  const eventName
    = `${CLASS_PREFIX}EventListenerAdded${JSON.stringify(secretEventName)}`;
  window.addEventListener(eventName, (event) => {
    const eventHandler = JSON.parse(event.detail);

    eventHandlerList[eventHandler.targetClassName] = eventHandler;
  });

  const HTMLElementsToInstrument = [
    HTMLHtmlElement,
    HTMLBodyElement,
    HTMLAnchorElement,
    HTMLButtonElement,
    HTMLInputElement,
    HTMLSelectElement,
    HTMLFormElement,
    HTMLDivElement,
    HTMLHeadingElement,
    HTMLLIElement,
    // HTMLElement,
  ];

  // https://learn.jquery.com/events/event-extensions/#events-overview-and-general-advice
  // https://kernelnewbies.org/Linux_5.8
  HTMLElementsToInstrument.forEach((htmlElement) => {
    const _addEventListener = htmlElement.prototype.addEventListener;

    // wantsUntrusted is Gecko/Mozilla only
    function e(type, listener, options, wantsUntrusted) {
      // console.log(this, type, listener, options, wantsUntrusted);

      const getErrorObject = function () {
        try {
          throw Error("");
        } catch(err) {
          return err;
        }
      };

      const error = getErrorObject();
      const stackTrace = error.stack.split("\n").slice(2);
      // console.log(stackTrace);

      const uniqueId = new Uint8Array(8);
      window.crypto.getRandomValues(uniqueId);

      // Add a unique and unguessable class name to the element as an
      // identifier, so that the event handler data can later be matched to the
      // element when needed
      const targetClassName
        = `${CLASS_PREFIX}${EVENT_PREFIX}${uniqueId.join("")}-${type}`;
      this.classList.add(targetClassName);

      const event
        = new CustomEvent(eventName, {
          detail: JSON.stringify({
            targetClassName,
            addEventListenerArgs: [
              type,
              {
                listenerName: listener.name,
                listenerToString: listener.toString(),
              },
              options,
              wantsUntrusted,
            ],
            addEventListenerStackTrace: stackTrace,
          }),
        });
      window.dispatchEvent(event);

      _addEventListener.call(this, type, listener, options, wantsUntrusted);
    }

    // TODO: exportFunction only works in Firefox
    /* global
      exportFunction
     */
    exportFunction(e, htmlElement.prototype, {
      defineAs: "addEventListener",
    });
  });
})();

console.log("spy on event listeners – end");
