"use strict";

// This script is injected in the page

(function () {
  const getJQueryEvents = (selectedElement) => {
    if (!selectedElement) {
      return null;
    }

    const jQuery = window.jQuery || window.$;

    if (!jQuery) {
      return null;
    }

    // jQuery 1.8+ (2011+)
    // see https://blog.jquery.com/2011/11/08/building-a-slimmer-jquery/
    let events = jQuery._data && jQuery._data(selectedElement).events;
    if (events) {
      return events;
    }

    // Older jQuery
    events = jQuery(selectedElement).data
      && jQuery(selectedElement).data("events");
    if (events) {
      return events;
    }

    return null;
  };

  const selectedElements = { document: document, body: document.body };
  const jQueryEvents = Object.assign(
    {},
    ...Object.entries(selectedElements)
      .map(([elementName, selectedElement]) => {
        return { [elementName]: getJQueryEvents(selectedElement) };
      }),
  );
  const libEvents = { jQueryEvents };

  const event = new CustomEvent("howBrokenLibEvents", {
    detail: JSON.stringify(libEvents),
  });
  window.dispatchEvent(event);
})();
