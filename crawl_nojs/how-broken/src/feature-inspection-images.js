"use strict";

window.HB = window.HB || {};
window.HB.heuristics = window.HB.heuristics || {};

const isSVGImage = (image) => {
  const src = image.currentSrc || image.src;
  if (!src) {
    return false;
  }

  return new URL(src).pathname.endsWith(".svg");
};

const isImageBroken = (image) => {
  // If the image is lazy-loaded, we cannot yet determine if the image is broken
  // The lazy-loaded image can have a 1x1 placeholder image
  if (image.currentSrc === ""
    || image.naturalWidth === 1 && image.naturalHeight === 1) {
    return false;
  }

  // Because SVG images usually have no intrinsic dimensions, it is hard to
  // determine whether they are broken without network instrumentation; consider
  // them not broken for now
  if (isSVGImage(image)) {
    return false;
  }

  if (!image.complete) {
    return true;
  }

  // Some sites would load a very low resolution at first, which is
  // considered broken, but some other sites stretch the images a bit,
  // which is not considered broken, this is a tradeoff between these two
  // conditions
  if (image.naturalWidth === 0
    || image.naturalWidth < image.width / 2
    || image.naturalHeight < image.height / 2) {
    return true;
  }

  return false;
};

const findImages = (options) => {
  // All images are considered useful

  if (options.essential) {
    options.essential = false;
    return findImages(options)
      .filter((image) => {
        return !window.HB.common.isElementNotUseful(image)
          && window.HB.common.isElementInMainSection(image);
      });
  }

  if (options.broken) {
    return findImages({
      useful: options.useful,
      essential: options.essential,
    }).filter((image) => isImageBroken(image));
  }

  return Array.from(document.getElementsByTagName("img"));
};
window.HB.heuristics.findImages = findImages;

// TESTING
// https://www.zoho.com/
// https://www.mightynetworks.com/
const findBackgroundImageOnlyElements = (options) => {
  // All images are considered useful

  if (options.essential) {
    options.essential = false;
    return findBackgroundImageOnlyElements(options);
  }

  if (options.broken) {
    return findBackgroundImageOnlyElements({
      useful: options.useful,
      essential: options.essential,
    });
  }

  return Array.from(document.getElementsByTagName("span"))
    .filter((span) => window.HB.domUtils.isTextOnlyWhitespace(span.textContent))
    .filter((span) => {
      const backgroundImage = getComputedStyle(span).backgroundImage;
      return backgroundImage && backgroundImage !== "none";
    });
};
window.HB.heuristics.findBackgroundImageOnlyElements
  = findBackgroundImageOnlyElements;

const findSVGImages = (options) => {
  if (options.useful || options.essential) {
    // Nothing to do: filtering is already done in findImages
  }

  if (options.broken) {
    return findSVGImages({
      useful: options.useful,
      essential: options.essential,
    }).filter((image) => isImageBroken(image));
  }

  return findImages({
    useful: options.useful,
    essential: options.essential,
  }).filter((image) => isSVGImage(image));
};
window.HB.heuristics.findSVGImages = findSVGImages;

const findAvatarImages = (options) => {
  // All avatar images are considered useful

  if (options.essential) {
    options.essential = false;
    return findAvatarImages(options)
      .filter((image) => {
        return !window.HB.common.isElementNotUseful(image)
          && window.HB.common.isElementInMainSection(image);
      });
  }

  if (options.broken) {
    return findAvatarImages({
      useful: options.useful,
      essential: options.essential,
    }).filter((image) => isImageBroken(image));
  }

  return window.HB.domUtils.selectorsMatchesExtra({
    match: `
    img[class_="avatar"],
    img[class_="gravatar"],
    [class_="avatar"] img`,
    exclude: "",
  });
};
window.HB.heuristics.findAvatarImages = findAvatarImages;

const LARGE_IMAGE_MIN_SIZE = 100;
const findLargeImages = (options) => {
  if (options.useful || options.essential) {
    // Nothing to do: filtering is already done in findImages
  }

  if (options.broken) {
    return findLargeImages({
      useful: options.useful,
      essential: options.essential,
    }).filter((image) => isImageBroken(image));
  }

  return findImages({
    useful: options.useful,
    essential: options.essential,
  }).filter((image) => (image.width >= LARGE_IMAGE_MIN_SIZE
    || image.naturalWidth >= LARGE_IMAGE_MIN_SIZE)
    && (image.height >= LARGE_IMAGE_MIN_SIZE
    || image.naturalHeight >= LARGE_IMAGE_MIN_SIZE));
};
window.HB.heuristics.findLargeImages = findLargeImages;

const isTrackingPixel = (image) => {
  const PIXEL_TRACKING_SUBDOMAINS = [
    "analytics",
    "stats",
    "beacon",
    "piwik",
    "matomo",
    "tim", // Tracking IMage
  ];

  const PIXEL_TRACKING_PATHS = [
    "analytics",
    "stats",
    "beacon",
    "tracker",
    "count",
    "piwik",
    "matomo",
  ];

  const src = image.currentSrc || image.src;

  if (!src) {
    return false;
  }

  const isRemote = window.HB.utils.getURLProtocol(src) !== "data:";

  if (!isRemote) {
    return false;
  }

  const altAttr = image.getAttribute("alt");

  // Tracking do not have an alt attribute longer than one character
  if (altAttr && altAttr.length > 1) {
    return false;
  }

  if ((image.width === 1 || image.naturalWidth === 1)
      && (image.height === 1 || image.naturalHeight === 1)) {
    return true;
  }

  // Check it the image comes from a subdomain known to be used for tracking
  if (PIXEL_TRACKING_SUBDOMAINS.some((subdomain) => {
    const hostname = window.HB.utils.getURLHostname(src);
    return hostname.startsWith(subdomain)
      || new RegExp(`.${subdomain}.`).test(hostname);
  })) {
    return true;
  }

  // Check it the image comes from a URL path known to be used for tracking
  if (PIXEL_TRACKING_PATHS.some((path) => {
    return new RegExp(`\\/${path}\\/`).test(src);
  })) {
    return true;
  }

  // Only tracking pixels would have a gdpr search param
  if (window.HB.utils.getURLSearchParams(src).get("gdpr")) {
    return true;
  }

  const hasTag = (tagName) => {
    return Boolean(document.getElementsByTagName(tagName).length);
  };

  const COMMON_SEMANTIC_TAG_NAMES = [
    "header",
    "footer",
    "article",
    "section",
    "figure",
  ];

  const hasSemanticTags
    = COMMON_SEMANTIC_TAG_NAMES.some((tagName) => hasTag(tagName));

  // If the page has semantic tags, an image that is at most the
  // grandchild of the body tag is very likely a tracking pixel
  // TODO: rewrite this when jsdom supports :is()
  if (hasSemanticTags
    && image.matches(`
      body > img,
      body > div > img,
      body > span > img,
      body > * > div > img,
      body > * > span > img`)) {
    return true;
  }

  return false;
};

// TESTING
// https://www.theguardian.com/international
// https://www.thesun.co.uk/
const findTrackingPixels = (options) => {
  if (options.useful || options.essential) {
    return [];
  }

  if (options.broken) {
    return findTrackingPixels({
      useful: options.useful,
      essential: options.essential,
    }).filter((image) => isImageBroken(image));
  }

  const trackingPixels = findImages({
    useful: options.useful,
    essential: options.essential,
  }).filter((image) => {
    return isTrackingPixel(image);
  });

  console.log(trackingPixels);

  return trackingPixels;
};
window.HB.heuristics.findTrackingPixels = findTrackingPixels;
