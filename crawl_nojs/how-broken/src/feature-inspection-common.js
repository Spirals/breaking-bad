"use strict";

window.HB = window.HB || {};
window.HB.common = window.HB.common || {};

// https://arxiv.org/abs/2009.01026
const isElementTargetedAtMobile = (element) => {
  const elementsTargetedAtMobile = window.HB._cacheStore.getOrSet(
    "elementsTargetedAtMobile",
    () => {
      return Array.from(document.querySelectorAll(`
        [id*="mobile"],
        :not([class*="hidden-mobile"])[class*="mobile"],
        :not([class*="hidden-mobile"])[class*="mobile"] *
        `));
    },
  );
  return elementsTargetedAtMobile.includes(element);
};
window.HB.common.isElementTargetedAtMobile = isElementTargetedAtMobile;

const isElementNotUseful = (element) => {
  const elementsNotUseful = window.HB._cacheStore.getOrSet(
    "elementsNotUseful",
    () => {
      return window.HB.domUtils.querySelectorAllExtra(
        document,
        '[class_="ad"], [class_="ad"] *',
      );
    },
  );
  return elementsNotUseful.includes(element);
};
window.HB.common.isElementNotUseful = isElementNotUseful;

const isElementInHeader = (element) => {
  const elementsInHeader = window.HB._cacheStore.getOrSet(
    "elementsInHeader",
    () => {
      return window.HB.domUtils.querySelectorAllExtra(
        document,
        // Search the exact 'header' class, otherwise it would also match
        // classes like column-header
        `
        header,
        [class~="header"],
        [id="header"]`,
      )
        .filter((el) => el.matches("header, div, nav, section"));
    },
  );
  return elementsInHeader
    .some((el) => el.contains(element));
};
window.HB.common.isElementInHeader = isElementInHeader;

const isElementInFooter = (element) => {
  const elementsInFooter = window.HB._cacheStore.getOrSet(
    "elementsInFooter",
    () => {
      return window.HB.domUtils.querySelectorAllExtra(
        document,
        `
        footer,
        [class~="footer"],
        [id="footer"]`,
      )
        .filter((el) => el.matches("footer, div, section"));
    },
  );
  return elementsInFooter
    .some((el) => el.contains(element));
};
window.HB.common.isElementInFooter = isElementInFooter;

const isElementInSidebar = (element) => {
  const elementsInSidebar = window.HB._cacheStore.getOrSet(
    "elementsInSidebar",
    () => {
      return Array.from(document.querySelectorAll(`
        [class~="sidebar"],
        [class~="side"],
        [id~="sidebar"],
        [aria-label~="sidebar" i],
        [role~="sidebar"],
        [aria-roledescription~="sidebar" i]`))
        .filter((el) => el.matches("div, nav, aside, section"));
    },
  );
  return elementsInSidebar
    .some((el) => el.contains(element));
};
window.HB.common.isElementInSidebar = isElementInSidebar;

const isElementInRelatedSection = (element) => {
  const elementsInRelatedSection = window.HB._cacheStore.getOrSet(
    "elementsInRelatedSection",
    () => {
      return window.HB.domUtils.querySelectorAllExtra(
        document,
        `
        [id_="related"]`,
      );
    },
  );
  return elementsInRelatedSection
    .some((el) => el.contains(element));
};
window.HB.common.isElementInRelatedSection = isElementInRelatedSection;

// https://blog.mozilla.org/security/2021/01/07/encrypted-client-hello-the-future-of-esni-in-firefox/
const isElementInMainSection = (element) => {
  if (isElementInHeader(element)
    || isElementInFooter(element)
    || isElementInSidebar(element)
    || isElementInRelatedSection(element)) {
    return false;
  }

  const elementsInPageMain = Array.from(document.querySelectorAll(`
        main,
        [id="main"],
        div[role="main"]`));

  if (elementsInPageMain.length > 0) {
    return elementsInPageMain
      .some((el) => el.contains(element));
  }

  // TODO: use :is() when querySelectorAllExtra
  // supports commas within selectors
  const elementsInContent = window.HB._cacheStore
    .getOrSet("elementsInContentPost", () => {
      return window.HB.domUtils.querySelectorAllExtra(
        document,
        `
        figure,
        article,
        div[class~="splash"],
        [id="content"],
        [class_="post"],
        div[class="document"]`,
      );
    });

  if (elementsInContent.length > 0) {
    return elementsInContent
      .some((el) => el.contains(element));
  }

  return true;
};
window.HB.common.isElementInMainSection = isElementInMainSection;

const getjQueryEventListsOnPage = () => {
  return window.HB._cacheStore.getOrSet(
    "jQueryEventListOnDocument",
    () => {
      const scriptElement = document.createElement("script");
      scriptElement.src = browser.runtime.getURL("src/spy-lib-events.js");
      (document.head || document.documentElement).appendChild(scriptElement);
      scriptElement.addEventListener("load", () => scriptElement.remove());

      // const prefix = "howBroken";
      // const eventName
      //   = `${prefix}jQueryEvents${JSON.stringify(secretEventName)}`;
      const eventName = "howBrokenLibEvents";
      return new Promise((resolve) => {
        window.addEventListener(eventName, (event) => {
          const libEvents = JSON.parse(event.detail);
          const jQueryEvents = libEvents && libEvents.jQueryEvents;
          resolve(jQueryEvents);
        });

        const GET_LIB_EVENTS_TIMEOUT_MS = 50;
        setTimeout(() => {
          resolve(null);
        }, GET_LIB_EVENTS_TIMEOUT_MS);
      });
    },
  );
};

const hasjQueryEventHandlerType = async (element, eventType) => {
  // Search events delegated to a parent of the element (only the document for
  // now)
  // https://learn.jquery.com/events/event-delegation/

  const events = await getjQueryEventListsOnPage(element);

  return events && Object.values(events).some((eventList) => {
    return eventList && eventList[eventType]
      && eventList[eventType].some((eventData) => {
        try {
          return element.matches(eventData.selector);
        } catch(e) {
          // Invalid selector
          return false;
        }
      });
  });
};

const hasEventHandlerType = async (element, eventType) => {
  const eventHandlers = window.HB._getElementEventHandlers(element);
  // console.log(element, eventHandlers);
  // console.log(element.tagName, element.type);

  return Boolean(element[`on${eventType}`])
    || eventHandlers && eventHandlers.some((eventHandler) => {
      return eventHandler.addEventListenerArgs
        && eventHandler.addEventListenerArgs[0] === eventType;
    })
    || await hasjQueryEventHandlerType(element, eventType);
};
window.HB.common.hasEventHandlerType = hasEventHandlerType;

const isSVGCaret = (svgElement) => {
  const polygons = Array.from(svgElement.querySelectorAll("polygon"));
  const paths = Array.from(svgElement.querySelectorAll("path"));

  if (polygons.length === 0) {
    if (paths.length === 0) {
      return false;
    }

    if (paths.length > 1) {
      return false;
    }

    const dAttr = paths[0].getAttribute("d");
    if (!dAttr) {
      return false;
    }

    // The caret must be drawn in a single line
    if ((dAttr.match(/[Mm]/g) || []).length > 1) {
      return false;
    }

    // Has too many commands
    if (dAttr.split(" ").length > 10) {
      return false;
    }

    const lineCount = (dAttr.match(/[lhv]/gi) || []).length;
    return lineCount >= 1 && lineCount < 7;
  }

  if (paths.length === 0 && polygons.length > 1) {
    return false;
  }

  const pointCount = polygons[0].points.length;
  return pointCount > 2 && pointCount < 7;
};

const hasCaret = (element) => {
  const DOWN_POINTING_CHARACTERS = [
    "⯆",
    "▼",
    "🞃",
    "⏷",
    "⮟",
    "",
    "⌄",
    "\ue90b", // Icomoon downwards caret
  ];

  const afterStyle = getComputedStyle(element, "::after");

  // The content property is a caret character
  if (DOWN_POINTING_CHARACTERS.some((character) => {
    return afterStyle.content === `"${character}"`;
  })) {
    return true;
  }

  // const svgChildren = Array.from(element.querySelectorAll(`
  //   :scope > svg,
  //   :scope > div > svg,
  //   :scope > div > div > svg,
  //   :scope > div > div > div > svg
  //   `));
  const svgChildren = Array.from(element.querySelectorAll("svg"));

  // A child is an SVG element, looking like a caret
  if (svgChildren.some((svgElement) => {
    return svgElement.matches(':not([class*="left" i]):not([class*="right" i]):not([class*="up" i])')
      && isSVGCaret(svgElement);
  })) {
    return true;
  }

  // TODO: use this when NWSAPI/jsdom supports the :is() pseudo-class
  // `
  // :is([class*="caret"],
  //    [class*="arrow"]):not([class*="left"]):not([class*="right"]):not([class*="up"])
  // `
  const HAS_DOWNWARD_CARET = `
    [class*="caret"]:not([class*="left"]):not([class*="right"]):not([class*="up"]),
    [class*="arrow"][class*="up"]
  `;

  if (Array.from(element.children)
    .some((child) => {
      return child.matches(HAS_DOWNWARD_CARET);
    })) {
    return true;
  }

  // A background image has an URL indicating a dropdown menu
  if (window.HB.domUtils.getBackgroundImageURLs(afterStyle).some((url) => {
    const filename = url.pathname;
    return /\bmenu\b/.test(filename)
      || /\barrow\b/.test(filename);
  })) {
    console.log(element);
    return true;
  }

  const TRANSPARENT_RGBA_COLOR = window.HB.domUtils.TRANSPARENT_RGBA_COLOR;

  // Dropdown caret drawn with CSS borders
  if (afterStyle.borderLeftColor === TRANSPARENT_RGBA_COLOR
    && afterStyle.borderRightColor === TRANSPARENT_RGBA_COLOR
    && afterStyle.borderLeftWidth !== "0px"
    && afterStyle.borderLeftWidth === afterStyle.borderRightWidth) {
    return true;
  }

  return false;
};
window.HB.common.hasCaret = hasCaret;

const hasBorder = (elementStyle, position) => {
  const DEFAULT_BORDER_WIDTH = "0px";

  if (Boolean(elementStyle.boxShadow) && elementStyle.boxShadow !== "none") {
    return true;
  }

  switch (position) {
    case "top":
      return Boolean(elementStyle.borderTopWidth)
        && elementStyle.borderTopWidth !== DEFAULT_BORDER_WIDTH
        && elementStyle.borderTopColor !== elementStyle.backgroundColor;
    case "bottom":
      return Boolean(elementStyle.borderBottomWidth)
        && elementStyle.borderBottomWidth !== DEFAULT_BORDER_WIDTH
        && elementStyle.borderBottomColor !== elementStyle.backgroundColor;
    case "left":
      return Boolean(elementStyle.borderLeftWidth)
        && elementStyle.borderLeftWidth !== DEFAULT_BORDER_WIDTH
        && elementStyle.borderLeftColor !== elementStyle.backgroundColor;
    case "right":
      return Boolean(elementStyle.borderRightWidth)
        && elementStyle.borderRightWidth !== DEFAULT_BORDER_WIDTH
        && elementStyle.borderRightColor !== elementStyle.backgroundColor;
    default:
      return hasBorder(elementStyle, "top")
        && hasBorder(elementStyle, "bottom")
        && hasBorder(elementStyle, "right")
        && hasBorder(elementStyle, "left");
  }
};
window.HB.common.hasBorder = hasBorder;

const hasDifferentHoverColor = async (element) => {
  const hoverCSSRules
    = await window.HB.domUtils.getElementMatchingStyleRules(element, "hover");

  const isColorInitialValue = window.HB.domUtils.isColorInitialValue;

  return hoverCSSRules
    .some((cssRule) => {
      const style = cssRule.style;
      const hoverBackgroundColor = style.backgroundColor;
      const differentHoverColor = Boolean(hoverBackgroundColor)
        && !isColorInitialValue(hoverBackgroundColor)
        || Boolean(style.borderBottomColor)
        && !isColorInitialValue(style.borderBottomColor)
        && Boolean(style.borderRightColor)
        && !isColorInitialValue(style.borderRightColor)
        && Boolean(style.borderLeftColor)
        && !isColorInitialValue(style.borderLeftColor)
        && Boolean(style.borderTopColor)
        && !isColorInitialValue(style.borderTopColor);

      return differentHoverColor;
    });
};
window.HB.common.hasDifferentHoverColor = hasDifferentHoverColor;

const isElementTabButtonLike = async (element) => {
  const tabButtonLikeElements = await window.HB._cacheStore.getOrSet(
    "tabButtonLikeElements",
    () => {
      return window.HB.utils.asyncFilter(
        Array.from(document.querySelectorAll("div, span, label, li, a")),
        async (el) => {
          if (!window.HB.domUtils.hasPointerCursor(el)) {
            return false;
          }

          const isBackgroundButton = window.HB.domUtils
            .hasDifferentBackgroundColorThanAncestor(element);

          if (isBackgroundButton) {
            return true;
          }

          const elementStyle = getComputedStyle(el);
          const sameVerticalBorders
            = elementStyle.borderRightWidth === elementStyle.borderLeftWidth;
          const sameHorizontalBorders
            = elementStyle.borderTopWidth === elementStyle.borderBottomWidth;

          const hasBottomBorder = hasBorder(elementStyle, "bottom");
          const hasTopBorder = hasBorder(elementStyle, "top");

          if (sameVerticalBorders
            && (hasTopBorder && !hasBottomBorder
              || !hasTopBorder && hasBottomBorder)) {
            return true;
          }

          const hasLeftBorder = hasBorder(elementStyle, "left");
          const hasRightBorder = hasBorder(elementStyle, "right");
          return sameHorizontalBorders
            && (hasLeftBorder && !hasRightBorder
              || !hasLeftBorder && hasRightBorder);
        },
      );
    },
  );
  return tabButtonLikeElements.includes(element);
};
window.HB.common.isElementTabButtonLike = isElementTabButtonLike;

const isElementHidden = (element) => {
  if (element.hidden) {
    return true;
  }

  const computedStyle = getComputedStyle(element);
  return computedStyle.display === "none"
    || computedStyle.visibility === "hidden"
    || element.offsetWidth === 0
    || element.offsetHeight === 0;
};
window.HB.common.isElementHidden = isElementHidden;

const HIDDEN_VISIBLE_STYLES = {
  "display": {
    hidden: [{
      test: "===",
      value: "none",
    }],
    visible: [{
      test: "!==",
      value: "none",
    }],
  },
  "visibility": {
    hidden: [{
      test: "!==",
      value: "visible",
    }],
    visible: [{
      test: "===",
      value: "visible",
    }],
  },
  "opacity": {
    hidden: [{
      test: "===",
      value: "0",
    }],
    visible: [{
      test: "===",
      value: "1",
    }],
  },
  "left": {
    unit: "px",
    hidden: [{
      test: "<=",
      value: "-1000",
    },
    {
      test: ">=",
      value: "9000",
    }],
    visible: [{
      test: "===",
      value: "0",
    }],
  },
  "max-height": {
    unit: "px",
    hidden: [{
      test: "===",
      value: "0",
    }],
    visible: [{
      test: ">",
      value: "0",
    }],
  },
  "height": {
    unit: "px",
    hidden: [{
      test: "===",
      value: "0",
    }],
    visible: [{
      test: ">",
      value: "0",
    }],
  },
  "margin-left": {
    unit: "%",
    hidden: [{
      test: "===",
      value: "-100",
    }],
    visible: [{
      test: "===",
      value: "0",
    }],
  },
};
window.HB.common.HIDDEN_VISIBLE_STYLES = HIDDEN_VISIBLE_STYLES;

const testCond = (a, stringCond, b, unit) => {
  if (unit) {
    a = a.replace(unit, "");
  }

  if (stringCond === "===") {
    return a === b;
  }

  if (stringCond === "!==") {
    return a !== b;
  }

  if (stringCond === "<=") {
    return parseInt(a) <= parseInt(b);
  }

  if (stringCond === ">=") {
    return parseInt(a) >= parseInt(b);
  }

  return false;
};
window.HB.common.testCond = testCond;

const isSpecificallyHidden = async (element) => {
  if (element.style.display === "none"
    || element.style.height === "0px") {
    return true;
  }

  const cssRules
    = await window.HB.domUtils.getElementMatchingStyleRules(element);

  return cssRules.some((cssRule) => {
    return Object.keys(HIDDEN_VISIBLE_STYLES).some((propName) => {
      const propValue = cssRule.style.getPropertyValue(propName);
      if (!propValue) {
        return false;
      }

      return HIDDEN_VISIBLE_STYLES[propName].hidden.some((h) => {
        const hides = testCond(
          propValue,
          h.test,
          h.value,
          HIDDEN_VISIBLE_STYLES[propName].unit,
        );
        return hides;
      });
    });
  });
};
window.HB.common.isSpecificallyHidden = isSpecificallyHidden;
