| Control        | Shape            | Input Type |
| :------------: | :--------------: | :---------:|
| hidden         | none             | n/a        |
| <button>       | button           | once       |
| button         | button           | once       |
| image          | button           | once       |
| reset          | button           | once       |
| submit         | button           | once       |
| checkbox       | restricted field | once       |
| radio          | restricted field | once       |
| range          | restricted field | once       |
| <select>       | restricted field | once       |
| file           | field            | once       |
| color          | field            | rep/once   |
| date           | field            | rep/once   |
| datetime-local | field            | rep/once   |
| month          | field            | rep/once   |
| time           | field            | rep/once   |
| week           | field            | rep/once   |
| datetime       | field            | rep/once   |
| email          | field            | rep        |
| number         | field            | rep        |
| password       | field            | rep        |
| search         | field            | rep        |
| tel            | field            | rep        |
| text           | field            | rep        |
| <textarea>     | field            | rep        |
| url            | field            | rep        |

rep/once depends on whether the user-agent provides a picker UI
